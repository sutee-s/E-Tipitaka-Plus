//
//  Bookmark.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "Bookmark.h"
#import "NSString+PaseHTML.h"

@implementation Bookmark

@synthesize note = _note;
@synthesize created = _created;
@synthesize plainNote = _plainNote;
@synthesize snippet = _snippet;

- (NSDictionary *)dictionaryValue
{
    return @{@"rowid":[NSNumber numberWithInteger:self.rowId],
             @"note":self.note ? self.note : @"",
             @"created":[NSNumber numberWithDouble:self.created.timeIntervalSince1970],
             @"important":[NSNumber numberWithBool:self.starred],
             @"rank": [NSNumber numberWithInteger:self.rank],
             @"code": [NSNumber numberWithInt:self.code],
             @"volume": [NSNumber numberWithInteger:self.volume],
             @"page": [NSNumber numberWithInteger:self.page]};
}

- (NSString *)plainNote
{
  if (!_plainNote) {
    _plainNote = [self.note parseHTML];
  }
  return _plainNote;
}

- (NSString *)snippet
{
  if (!_snippet) {
    _snippet = self.plainNote.length > 50 ? [self.plainNote substringToIndex:50] : self.plainNote;
  }
  return _snippet;
}

@end
