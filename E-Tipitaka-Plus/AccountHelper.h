//
//  AccountHelper.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountHelper : NSObject

+ (NSString *)currentUsername;
+ (NSString *)currentToken;
+ (void)deleteCurrentAccount;
+ (void)updateCurrentAccount:(NSString *)username withToken:(NSString *)token;

@end
