//
//  PageInfo.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 11/12/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "PageInfo.h"

@implementation PageInfo

@synthesize code = _code;
@synthesize volume = _volume;
@synthesize page = _page;
@synthesize keywords = _keywords;

- (id)initWithCode:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page
{
    self = [super init];
    if (self) {
        self.code = code;
        self.volume = volume;
        self.page = page;
    }
    return self;
}

@end
