//
//  UserData.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "UserData.h"

@implementation UserData

@synthesize username = _username;
@synthesize platform = _platform;
@synthesize path = _path;
@synthesize gid = _gid;
@synthesize adding = _adding;
@synthesize deleting = _deleting;
@synthesize rowId = _rowId;

- (instancetype)initWithUsername:(NSString *)username platform:(NSString *)platform path:(NSString *)path gid:(NSNumber *)gid
{
    if (self=[super init]) {
        self.username = username;
        self.platform = platform;
        self.path = path;
        self.gid = gid;
    }
    return self;
}

@end
