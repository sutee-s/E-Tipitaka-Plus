//
//  DownloadDatabaseHelper.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 10/12/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadDatabaseHelper.h"
#import "UserDatabaseHelper.h"

#import <JGProgressHUD/JGProgressHUD.h>
#import <SSZipArchive/SSZipArchive.h>
#import <AFNetworking/AFNetworking.h>

#include <sys/param.h>
#include <sys/mount.h>


@interface DownloadDatabaseHelper : NSObject

@property (nonatomic ,assign) BOOL isFirstTime;
@property (nonatomic, weak) UIViewController *viewController;

+ (instancetype)sharedInstance;
+ (float) diskSpaceLeft;
- (void)downloadDatabases;
- (BFTask *)updateDatabaseWithCode:(NSString *)stringCode;
- (void) forceDismissHUD;

@end
