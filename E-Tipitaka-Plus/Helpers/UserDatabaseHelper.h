//
//  UserDatabaseHelper.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDatabase.h>
#import <Bolts/Bolts.h>
#import "History.h"
#import "Bookmark.h"
#import "Highlight.h"
#import "Lexicon.h"
#import "Tag.h"
#import "HighlightColor.h"
#import "HighlightNote.h"
#import "HighlightDict.h"
#import "ReadingState.h"
#import "OtherUser.h"
#import "WriteNote.h"
#import "SearchAllHistory.h"

@interface UserDatabaseHelper : NSObject

+ (void)saveSearchResults:(NSArray *)results ofKeywords:(NSString *)keywords withCode:(LanguageCode)code;
+ (void)saveSearchResults:(NSArray *)results ofKeywords:(NSString *)keywords ofType:(SearchType)type withCode:(LanguageCode)code totalSearch:(BOOL)totalSearch;
+ (void)updateSearchAllHistoryAtColumn:(NSString *)column setValue:(id)value byRowId:(NSInteger)rowId;

+ (void)saveSearchAllHistory:(NSString *)keywords withDetail:(NSString *)detail;
+ (NSArray *)querySearchAllHistories:(NSString *)keywords;
+ (NSArray *)querySearchAllHistories;
+ (void)deleteSearchAllHistoryByRowId:(NSInteger)rowId;
+ (SearchAllHistory *)getSearchAllHistoryByRowId:(NSInteger)rowId;
+ (void)updateSearchAllHistoryNote:(NSInteger)rowId note:(NSString *)note state:(NSUInteger)state;
+ (NSArray *)querySearchAllHistoriesInRowIds:(NSArray *)rowIds;

+ (NSArray *)queryHistoriesInRowIds:(NSArray *)rowIds withCode:(LanguageCode)code;
+ (NSArray *)queryHistories:(NSString *)keywords withCode:(LanguageCode)code;
+ (NSArray *)queryHistories:(NSString *)keywords withCode:(LanguageCode)code ofUser:(OtherUser *)user;
+ (NSArray *)queryHistories:(NSString *)keywords;
+ (NSArray *)queryHistories:(NSString *)keywords ofUser:(OtherUser *)user;
+ (NSArray *)queryHistories;

+ (NSArray *)historyItemsById:(NSInteger)rowId;
+ (NSDictionary *)historyDataByRowId:(NSInteger)rowId;

+ (History *)getHistory:(NSString *)keywords withCode:(LanguageCode)code;
+ (History *)getHistoryByRowId:(NSInteger)rowId;
+ (History *)getHistory:(NSString *)keywords ofType:(SearchType)type withCode:(LanguageCode)code;

+ (NSArray *)getHistoryReadItemsByRowId:(NSInteger)rowId;
+ (NSArray *)getHistorySkimmedItemsByRowId:(NSInteger)rowId;
+ (NSArray *)getHistoryMarkedItemsByRowId:(NSInteger)rowId;
+ (NSMutableDictionary *)getHistoryNoteItemsByRowId:(NSInteger)rowId;

+ (void)updateHistorySkimmedItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page;
+ (void)updateHistoryReadItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page;
+ (void)updateHistoryMarkedItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page;
+ (void)updateHistoryNote:(NSInteger)rowId note:(NSString *)note state:(NSUInteger)state;
+ (void)updateHistoryNoteItems:(NSInteger)index note:(NSString *)note state:(NSUInteger)state rowId:(NSInteger)rowId;
+ (void)updateHistoryAtColumn:(NSString *)column setValue:(id)value byRowId:(NSInteger)rowId;


+ (void)deleteHistoryMarkedItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page;
+ (void)deleteHistoryByRowId:(NSInteger)rowId;

+ (NSArray *)queryBookmarks:(NSString *)note withCode:(LanguageCode)code;
+ (NSArray *)queryBookmarks:(NSString *)note withCode:(LanguageCode)code ofUser:(OtherUser *)user;
+ (NSArray *)queryBookmarksInRowIds:(NSArray *)rowIds withCode:(LanguageCode)code;
+ (NSArray *)queryBookmarks;
+ (void)saveBookmark:(NSString *)text volume:(NSInteger)volume page:(NSInteger)page starred:(BOOL)starred withCode:(LanguageCode)code;
+ (Bookmark *)getBookmarkByVolume:(NSInteger)volume page:(NSInteger)page withCode:(LanguageCode)code;
+ (void)updateBookmarkAtColumn:(NSString *)column setValue:(id)value byRowId:(NSInteger)rowId;
+ (void)deleteBookmarkByRowId:(NSInteger)rowId;
+ (void)importUserDatabase:(NSURL *)url;
+ (void)addHistory:(History *)history items:(NSArray *)items skimmedItems:(NSArray *)skimmedItems readItems:(NSArray *)readItems markedItems:(NSArray *)markedItems created:(NSDate *)created;
+ (void)clearHistories;
+ (void)exportUserDatabase:(NSURL *)url;

+ (void)saveHighlight:(Highlight *)highlight;
+ (void)updateHighlight:(Highlight *)highlight;
+ (NSInteger)checkExistingHighlight:(Highlight *)highlight;
+ (void)deleteHighlight:(NSInteger)rowId;
+ (NSArray *)queryHighlightsInVolume:(NSInteger)volume page:(NSInteger)page code:(LanguageCode)code;
+ (BOOL)checkOverlapHighlightsInVolume:(NSInteger)volume page:(NSInteger)page code:(LanguageCode)code range:(NSRange)range;
+ (Highlight *)getHighlightByRowId:(NSInteger)rowId;
+ (NSArray *)queryHighlightsByKeywordOfSelection:(NSString *)keyword code:(LanguageCode)code;
+ (NSArray *)queryHighlightsByKeywordOfNote:(NSString *)keyword code:(LanguageCode)code;
+ (NSArray *)queryHighlightsByKeyword:(NSString *)keyword code:(LanguageCode)code;
+ (NSArray *)queryHighlightsOfCode:(LanguageCode)code;
+ (NSMutableArray *)queryHighlights:(NSArray *)rowIds withCode:(LanguageCode)code;
+ (NSArray *)queryHighlights;

+ (HighlightColor *)getHighlightColorByRowId:(NSInteger)rowId;
+ (NSArray *)queryHighlightColor;
+ (BOOL)deleteHighlightColorByRowId:(NSInteger)rowId;
+ (void)addHighlightColor:(HighlightColor *)color;
+ (void)updateHighlightColor:(HighlightColor *)color;

+ (NSArray *)queryHighlightNoteOfHighlight:(Highlight *)highlight;
+ (BOOL)checkOverlapHighlightNotesInRange:(NSRange)range ofHighlight:(Highlight *)highlight;
+ (void)addHighlightNote:(HighlightNote *)highlightNote;
+ (BOOL)deleteHighlightNote:(HighlightNote *)highlightNote;
+ (HighlightNote *)getHighlightNoteByRowId:(NSInteger)rowId;
+ (NSString *)createHtmlHighlightNotes:(Highlight *)highlight withBlock:(NSString *(^)(NSString *, NSInteger))block;

+ (void)saveLexicon:(Lexicon *)lexicon type:(DictionaryType)type;
+ (BOOL)deleteLexicon:(Lexicon *)lexicon type:(DictionaryType)type;
+ (NSArray *)querySavedLexicon:(DictionaryType)type;
+ (NSArray *)querySavedLexicon;

+ (void)updateHistoryDatabase;
+ (void)updateHighlightDatabase;
+ (void)updateReadingStateDatabase;
+ (void)updateNoteDatabase;
+ (void)updateTagDatabase;
+ (void)updateHighlightColorDatabase;

+ (LanguageCode)languageCodeByCodeString:(NSString *)code;
+ (BOOL)isTipitaka:(LanguageCode)code;

+ (void)saveUserData:(NSString *)username path:(NSString *)path platform:(NSString *)platform gid:(NSNumber *)gid;
+ (NSMutableArray *)getUserDataItems:(NSString *)username platform:(NSString *)platform;
+ (NSMutableArray *)getDeletingUserDataItems:(NSString *)username;
+ (void)updateUserDataAtColumn:(NSString *)column setValue:(id)value byGid:(NSInteger)gid;
+ (BOOL)deleteUserDataByGid:(NSInteger)gid;

+ (NSArray *)getTagItems:(NSString *)column ofTag:(NSString *)name withCode:(LanguageCode)code;
+ (void)updateTagItems:(NSString *)column items:(NSArray *)items ofTag:(NSString *)name withCode:(LanguageCode)code;
+ (void)addTagItems:(NSString *)column rowId:(NSInteger)rowId ofTag:(NSString *)name withCode:(LanguageCode)code;
+ (void)updateTagPriority:(NSNumber *)priority ofTag:(NSString *)name withCode:(LanguageCode)code;
+ (BOOL)deleteTag:(NSString *)name withCode:(LanguageCode)code;
+ (void)addTag:(NSString *)name withCode:(LanguageCode)code;
+ (void)updateTagName:(NSString *)name ofTag:(Tag *)tag;
+ (Tag *)getTag:(NSString *)name withCode:(LanguageCode)code;
+ (NSMutableArray *)queryTags:(LanguageCode)code;
+ (NSArray *)queryTagNames:(LanguageCode)code;
+ (NSMutableArray *)queryTagNames;
+ (NSMutableArray *)queryTags;
+ (NSArray *)queryTagNamesFrom:(NSString *)column byRowId:(NSInteger)rowId withCode:(LanguageCode)code;

+ (NSArray *)getSuggestWords:(NSString *)word code:(NSString *)stringCode;

+ (NSInteger)saveWriteNote:(NSString *)text starred:(BOOL)starred created:(NSDate *)created main:(BOOL)main;
+ (void)updateWirteNote:(NSString *)text starred:(BOOL)starred rowId:(NSInteger)rowId;
+ (void)updateWirteNote:(BOOL)starred rowId:(NSInteger)rowId;
+ (void)deleteWriteNote:(NSInteger)rowId;
+ (WriteNote *)getWriteNote:(NSInteger)rowId;
+ (WriteNote *)getMainWriteNote;
+ (NSArray *)queryNotes:(NSString *)note;

+ (void)initReadingStateDatabase;
+ (NSArray *)queryReadingStates;
+ (NSArray *)queryReadingStatesWithCode:(LanguageCode)code;
+ (NSArray *)queryReadingStatesWithCode:(LanguageCode)code andVolume:(NSInteger)volume;
+ (NSArray *)queryReadingStates:(ReadingStateType)state code:(LanguageCode)code volume:(NSInteger)volume;
+ (ReadingState *)getReadingState:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page;
+ (NSInteger)countReadingStates:(ReadingStateType)state code:(LanguageCode)code volume:(NSInteger)volume;
+ (NSInteger)countReadingStates:(ReadingStateType)state code:(LanguageCode)code;
+ (NSInteger)countReadingStatesWithCode:(LanguageCode)code andVolume:(NSInteger)volume;
+ (NSInteger)countReadingStates:(ReadingStateType)state code:(LanguageCode)code fromVolume:(NSInteger)start toVolume:(NSInteger)end;
+ (ReadingState *)addReadingState:(ReadingState *)readingState;

+ (NSInteger)countOthers;
+ (NSArray *)queryOthers;
+ (void)addOtherUser:(OtherUser *)user;
+ (OtherUser *)getOtherUserByUsername:(NSString *)username;
+ (void)deleteOther:(OtherUser *)user;

+ (void)saveHighlightDict:(HighlightDict *)highlight;
+ (void)deleteHighlightDict:(NSInteger)rowId;
+ (NSArray *)queryHighlightDicts:(DictionaryType)type rowId:(NSUInteger)rowId;
+ (HighlightDict *)getHighlightDictByRowId:(NSInteger)rowId;
+ (BOOL)checkOverlapHighlightDicts:(DictionaryType)type wid:(NSInteger)wid range:(NSRange)range;

+ (FMDatabase *)bookmarkDatabase;
+ (FMDatabase *)userDataDatabase;
+ (FMDatabase *)historyDatabase;
+ (FMDatabase *)suggestDatabase;
+ (FMDatabase *)readingStateDatabase;
+ (FMDatabase *)othersDatabase;

+ (void)createSavedLexiconDatabaseTable;
+ (void)createUserDataDatabaseTable;
+ (void)createHighlightDatabaseTable;
+ (void)createHighlightNoteDatabaseTable;
+ (void)createHighlightColorDatabaseTable;
+ (void)createHighlightDictDatabaseTable;
+ (void)createTagDatabaseTable;
+ (void)createReadingStateDatabaseTable;
+ (void)createOthersDatabaseTable;
+ (void)createNoteDatabaseTable;

+ (BFTask *)getDatabaseVersionWithCode:(NSString *)stringCode;
+ (BFTask *)getThaiMCDatabaseVersion;
+ (BFTask *)getThaiMMDatabaseVersion;
+ (BFTask *)getThaiWNDatabaseVersion;
+ (BFTask *)getThaiPBDatabaseVersion;
+ (BFTask *)getRomanCTDatabaseVersion;
+ (BFTask *)getPaliDatabaseVersion;
+ (BFTask *)getEnglishDictionaryDatabaseVersion;
+ (BFTask *)getThaiBTDatabaseVersion;


@end
