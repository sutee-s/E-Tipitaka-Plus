//
//  HighlightDict.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 3/2/23.
//  Copyright © 2023 Watnapahpong. All rights reserved.
//

#import "HighlightDict.h"

@implementation HighlightDict

- (id)initFromResultSet:(FMResultSet *)s
{
    self = [super init];
    if (self) {
        self.rowId = [s intForColumnIndex:0];
        self.type = [s intForColumn:@"type"];
        self.wid = [s intForColumn:@"wid"];
        self.selection = [s stringForColumn:@"selection"];
        self.position = [s intForColumn:@"position"];
        self.color = [s stringForColumn:@"color"];
        self.note = [s stringForColumn:@"note"];
        self.range = NSMakeRange([s intForColumn:@"start"], [s intForColumn:@"end"] - [s intForColumn:@"start"]);
    }
    return self;
}
\
- (NSDictionary *)dictionaryValue
{
    return @{
        @"rowid": [NSNumber numberWithInteger:self.rowId],
        @"type": [NSNumber numberWithInt:self.type],
        @"wid": [NSNumber numberWithInteger:self.wid],
        @"selection":self.selection,
        @"position": [NSNumber numberWithInteger:self.position],
        @"start":[NSNumber numberWithInteger:self.range.location],
        @"end":[NSNumber numberWithInteger:self.range.location+self.range.length],
        @"color" : self.color,
        @"note" : self.note ? self.note : @""
    };
}

@end
