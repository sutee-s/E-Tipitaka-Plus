//
//  WriteNote.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "WriteNote.h"
#import "NSString+PaseHTML.h"
#import "NSString+Thai.h"

@implementation WriteNote

@synthesize plainNote = _plainNote;
@synthesize snippet = _snippet;
@synthesize rowId = _rowId;

- (NSDictionary *)dictionaryValue
{
    return @{@"rowid":[NSNumber numberWithInteger:self.rowId],
             @"note":self.note ? self.note : @"",
             @"created":[NSNumber numberWithDouble:self.created.timeIntervalSince1970],
             @"important":[NSNumber numberWithBool:self.starred],
             @"main":[NSNumber numberWithBool:self.main],
             @"rank": [NSNumber numberWithInteger:self.rank]};
}

- (NSString *)plainNote
{
  if (!_plainNote) {
    _plainNote = [self.note parseHTML];
  }
  return _plainNote;
}

- (NSString *)snippet
{
  if (!_snippet) {
    _snippet = self.plainNote.length > 50 ? [self.plainNote substringToIndex:50] : self.plainNote;
    if (_rowId > 0) {
      _snippet = [[NSString stringWithFormat:@"%ld. %@", _rowId, _snippet] stringByReplacingThaiNumeric];
    }
  }
  return _snippet;
}

@end
