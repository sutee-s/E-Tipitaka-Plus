//
//  ReadingState.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 12/23/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Types.h"

@interface ReadingState : NSObject

@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) ReadingStateType state;
@property (nonatomic, assign) NSInteger count;

- (NSDictionary *)dictionaryValue;

@end
