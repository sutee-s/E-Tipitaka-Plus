//
//  BookDatabaseHelper.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 29/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <FMDB/FMDatabase.h>
#import "PageResult.h"

@interface BookDatabaseHelper : NSObject

+ (FMDatabase *) database:(LanguageCode)code;
+ (BOOL)checkDatabaseAvailability:(LanguageCode)code;
+ (NSInteger) totalPagesOfVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (NSInteger) totalItemsOfVolume:(NSInteger)volume withCodeString:(NSString *)codeString;
+ (NSInteger) totalItemsOfVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (PageResult *) queryPage:(NSInteger)page inVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (NSArray *) queryPagesInVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page withCode:(LanguageCode)code;
+ (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume withCodeString:(NSString *)codeString;
+ (NSArray *) search:(NSString *)keyword withCode:(LanguageCode)code;
+ (NSInteger) sectionOfVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (NSInteger) pageOfItem:(NSInteger)item subItem:(NSInteger)subItem inVolume:(NSInteger)volume withCodeString:(NSString *)codeString;
+ (NSInteger) pageOfItem:(NSInteger)item subItem:(NSInteger)subItem inVolume:(NSInteger)volume withCode:(LanguageCode)code;
+ (NSArray *) books;
+ (NSArray *) comparableBooks;
+ (NSArray *)book:(NSInteger)code;
+ (NSDictionary *)bookItems;
+ (NSDictionary *)bookNames;
+ (NSDictionary *)romanTitles;
+ (NSDictionary *)romanItems;
+ (NSDictionary *)romanVolumeMappingTable;
+ (NSDictionary *)romanReverseVolumeMappingTable;
+ (NSDictionary *)romanPageIndexTable;
+ (NSDictionary *)volumeMappingTable;
+ (NSDictionary *)itemMappingTableMc;
+ (NSDictionary *)itemMappingTableMs;
+ (NSString *)shortName:(LanguageCode)code;

@end
