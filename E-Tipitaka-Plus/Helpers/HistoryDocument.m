//
//  HistoryDocument.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 26/3/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "HistoryDocument.h"
#import "UserDatabaseHelper.h"

@implementation HistoryDocument

- (NSArray *)items
{
    NSMutableArray *histories = [[NSMutableArray alloc] init];
    for (History *history in [UserDatabaseHelper queryHistories]) {
        NSDictionary *data = [UserDatabaseHelper historyDataByRowId:history.rowId];
        NSMutableDictionary *dict = [[history dictionaryValue] mutableCopy];
        [dict setObject:data[@"items"] forKey:@"items"];
        [dict setObject:data[@"read"] forKey:@"read"];
        [dict setObject:data[@"skimmed"] forKey:@"skimmed"];
        [dict setObject:data[@"note_items"] forKey:@"note_items"];
        [dict setObject:data[@"search_all"] forKey:@"search_all"];
        [histories addObject:dict];
    }
    return histories;
}

@end
