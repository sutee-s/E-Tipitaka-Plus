//
//  NoteItem.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/9/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "NoteItem.h"

@implementation NoteItem

- (instancetype)initWithNote:(NSString *)note state:(NSUInteger)state
{
    self = [super init];
    if (self) {
        self.note = note;
        self.state = state;
    }
    return self;
}

@end
