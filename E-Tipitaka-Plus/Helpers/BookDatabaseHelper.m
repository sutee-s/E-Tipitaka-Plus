//
//  BookDatabaseHelper.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 29/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "BookDatabaseHelper.h"

@implementation BookDatabaseHelper

+ (FMDatabase *) database:(LanguageCode)code
{
    switch (code) {
        case LanguageCodeThaiSiam:
            return [FMDatabase databaseWithPath:THAI_DB_PATH];
        case LanguageCodePaliSiam:
            return [FMDatabase databaseWithPath:PALI_DB_PATH];
        case LanguageCodeThaiMahaMakut:
            return [FMDatabase databaseWithPath:THAIMM_DB_PATH];
        case LanguageCodeThaiMahaChula:
            return [FMDatabase databaseWithPath:THAIMC_DB_PATH];
        case LanguageCodeThaiMahaChula2:
            return [FMDatabase databaseWithPath:THAIMC2_DB_PATH];
        case LanguageCodeThaiFiveBooks:
            return [FMDatabase databaseWithPath:THAIBT_DB_PATH];
        case LanguageCodeThaiWatna:
            return [FMDatabase databaseWithPath:THAIWN_DB_PATH];
        case LanguageCodeThaiPocketBook:
            return [FMDatabase databaseWithPath:THAIPB_DB_PATH];
        case LanguageCodeRomanScript:
            return [FMDatabase databaseWithPath:ROMANCT_DB_PATH];
        case LanguageCodeThaiSupreme:
            return [FMDatabase databaseWithPath:THAIMS_DB_PATH];
        case LanguageCodeThaiVinaya:
            return [FMDatabase databaseWithPath:THAIVN_DB_PATH];
        case LanguageCodePaliMahaChula:
            return [FMDatabase databaseWithPath:PALIMC_DB_PATH];
        case LanguageCodePaliSiamNew:
            return [FMDatabase databaseWithPath:PALINEW_DB_PATH];
    }
    return nil;
}

+ (BOOL)checkDatabaseAvailability:(LanguageCode)code
{
    FMDatabase *db = [BookDatabaseHelper database:code];
    if (![db open]) return NO;
    NSString *statement = @"SELECT name FROM sqlite_master WHERE type='table'";
    FMResultSet *s = [db executeQuery:statement];
    BOOL result = [s next];
    [db close];
    return result;
}

+ (NSArray *) search:(NSString *)keyword withCode:(LanguageCode)code
{
    FMDatabase *db = [BookDatabaseHelper database:code];
    
    if (![db open]) return nil;
    
    NSMutableArray *args = [[NSMutableArray alloc] init];
    for (NSString *term in [keyword componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]) {
        [args addObject:[NSString stringWithFormat:@"*%@*", [term stringByReplacingOccurrencesOfString:@"+" withString:@" "]]];
    }

    NSMutableArray *results = [[NSMutableArray alloc] init];
    if (args.count > 0) {
        NSString *statement = @"SELECT volume,page FROM main WHERE content GLOB ?";
        for (int i=0; i < args.count-1; ++i) {
            statement = [statement stringByAppendingString:@" AND content GLOB ?"];
        }
        
        FMResultSet *s = [db executeQuery:statement withArgumentsInArray:args];
        while ([s next]) {
            [results addObject:@{@"volume": [NSNumber numberWithInteger:[[s stringForColumnIndex:0] integerValue]],
                                 @"page": [NSNumber numberWithInteger:[[s stringForColumnIndex:1] integerValue]]}];
        }
    }
    
    [db close];
    
    return results;
}

+ (NSInteger) totalPagesOfVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    FMDatabase *db = [BookDatabaseHelper database:code];
    
    if (![db open]) return 0;
    
    FMResultSet *s;
    if (code == LanguageCodeRomanScript) {
        s = [db executeQuery:@"SELECT COUNT(*) FROM main WHERE volume = ?", [NSNumber numberWithInteger:volume]];
    } else {
        s = [db executeQuery:@"SELECT COUNT(*) FROM main WHERE volume = ?", [NSString stringWithFormat:@"%02d", volume]];
    }
    

    int count = [s next] ? [s intForColumnIndex:0] : 0;

    [db close];

    return count;
}

+ (NSInteger) totalItemsOfVolume:(NSInteger)volume withCodeString:(NSString *)codeString
{
    NSDictionary *dict = [BookDatabaseHelper bookItems];
    NSInteger totalItems = 0;
    for (NSString *sub in [dict[codeString][[NSString stringWithFormat:@"%d", volume]] allKeys]) {
        NSArray *items = [[dict[codeString][[NSString stringWithFormat:@"%d", volume]][sub] allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [[NSNumber numberWithInt:[obj1 intValue]] compare:[NSNumber numberWithInt:[obj2 intValue]]];
        }];
        totalItems = MAX([items.lastObject integerValue], totalItems);
    }
    return totalItems;
}

+ (NSInteger) totalItemsOfVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    
    FMDatabase *db = [BookDatabaseHelper database:code];
    
    if (![db open]) return 0;
    
    NSInteger totalItems = 0;

    FMResultSet *s;
    if (code == LanguageCodeRomanScript) {
        s = [db executeQuery:@"SELECT items FROM main WHERE volume = ?", [NSNumber numberWithInteger:volume]];
    } else {
        s = [db executeQuery:@"SELECT items FROM main WHERE volume = ?", [NSString stringWithFormat:@"%02d", volume]];
    }
    
    while ([s next]) {
        NSString *items = [s stringForColumnIndex:0];
        totalItems = MAX(totalItems, [[items componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].lastObject integerValue]);
    }

    [db close];
    
    return totalItems;
}

+ (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page withCode:(LanguageCode)code
{
    return [BookDatabaseHelper queryPage:page inVolume:volume withCode:code].items;
}

+ (NSArray *) queryPagesInVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    FMDatabase *db = [BookDatabaseHelper database:code];

    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM main WHERE volume = ?", [NSNumber numberWithInteger:volume]];
    if (![s next]) {
        s = [db executeQuery:@"SELECT * FROM main WHERE volume = ?", [NSString stringWithFormat:@"%02d", volume]];
        if (![s next]) {
            s = [db executeQuery:@"SELECT * FROM speech WHERE book=?", [NSNumber numberWithInteger:volume]];
            [s next];
        }
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    do {
        PageResult *result = [[PageResult alloc] init];
        result.volume = [NSNumber numberWithInteger:volume];
        result.page = [NSNumber numberWithInteger:[s stringForColumn:@"page"].integerValue];
        result.code = code;
        result.content = [s stringForColumn:@"content"];
        result.header = [s stringForColumn:@"header"];
        result.footer = [s stringForColumn:@"footer"];
        result.htmlContent = [s stringForColumn:@"html"];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for (NSString *token in [[s stringForColumn:@"items"] componentsSeparatedByString:@" "]) {
            [array addObject:[NSNumber numberWithInt:token.intValue]];
        }
        result.items = array;
        [results addObject:result];
    } while ([s next]);
   
    return results;
}

+ (PageResult *) queryPage:(NSInteger)page inVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    FMDatabase *db = [BookDatabaseHelper database:code];

    if (![db open]) return nil;

    FMResultSet *s = [db executeQuery:@"SELECT * FROM main WHERE volume = ? AND page = ?", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page]];
    if (![s next]) {
        s = [db executeQuery:@"SELECT * FROM main WHERE volume = ? AND page = ?", [NSString stringWithFormat:@"%02d", volume], [NSString stringWithFormat:@"%04d", page]];
        if (![s next]) {
            s = [db executeQuery:@"SELECT * FROM speech WHERE book = ? AND page = ?", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page]];
            [s next];
        }
    }
    
    PageResult *result = nil;
    result = [[PageResult alloc] init];
    result.volume = [NSNumber numberWithInt:volume];
    result.page = [NSNumber numberWithInt:page];
    result.code = code;
    result.content = [s stringForColumn:@"content"];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSString *token in [[s stringForColumn:@"items"] componentsSeparatedByString:@" "]) {
        [array addObject:[NSNumber numberWithInt:token.intValue]];
    }
    result.items = array;
    
    [db close];
    
    return result;
}

+ (NSDictionary *)romanTitles
{
    static NSDictionary *__sharedRomanTitles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedRomanTitles = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"roman_titles" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedRomanTitles;
    
}


+ (NSDictionary *)romanItems
{
    static NSDictionary *__sharedRomanItems;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedRomanItems = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"roman_items" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedRomanItems;
    
}

+ (NSDictionary *)romanVolumeMappingTable
{
    static NSDictionary *__sharedRomanVolumeMappingTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedRomanVolumeMappingTable = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"map_cst" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedRomanVolumeMappingTable;
    
}

+ (NSDictionary *)romanReverseVolumeMappingTable
{
    static NSDictionary *__sharedReverseRomanVolumeMappingTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedReverseRomanVolumeMappingTable = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"map_cst_r" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedReverseRomanVolumeMappingTable;
    
}


+ (NSDictionary *)romanPageIndexTable
{
    static NSDictionary *__sharedRomanPageIndexTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedRomanPageIndexTable = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"roman_page_index" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedRomanPageIndexTable;
    
}

+ (NSDictionary *)bookItems
{
    static NSDictionary *__sharedBookItems;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedBookItems = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"book_item" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedBookItems;
}

+ (NSDictionary *)bookNames
{
    static NSDictionary *__sharedBookNames;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedBookNames = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"book_name" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedBookNames;
}

+ (NSDictionary *)volumeMappingTable
{
    static NSDictionary *__sharedVolumeMappingTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedVolumeMappingTable = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"maps" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedVolumeMappingTable;
}

+ (NSDictionary *)itemMappingTableMc
{
    static NSDictionary *__sharedItemMappingTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedItemMappingTable = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"mc_map" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedItemMappingTable;
}

+ (NSDictionary *)itemMappingTableMs
{
    static NSDictionary *__sharedItemMappingTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedItemMappingTable = [[NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ms_map" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil] objectFromJSONString];
    });
    return __sharedItemMappingTable;
}

+ (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume withCodeString:(NSString *)codeString
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    NSDictionary *dict = [BookDatabaseHelper bookItems];
    for (NSString *subItem in dict[codeString][[NSString stringWithFormat:@"%d", volume]]) {
        NSInteger page = [dict[codeString][[NSString stringWithFormat:@"%d", volume]][subItem][[NSString stringWithFormat:@"%d", item]][0] integerValue];
        if (page > 0) {
            [results addObject:[NSNumber numberWithInteger:page]];
        }
    }
    return results;
}


+ (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    FMDatabase *db = [BookDatabaseHelper database:code];
    
    if (![db open]) return nil;

    FMResultSet *s = [db executeQuery:@"SELECT page FROM mapping WHERE volume = ? AND item = ? AND marked = 1", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:item]];

    NSMutableArray *results = [[NSMutableArray alloc] init];
    while ([s next]) {
        [results addObject:[NSNumber numberWithInt:[s intForColumnIndex:0]]];
    }
    
    return results;
}

+ (NSInteger) sectionOfVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    if (code == LanguageCodeThaiSiam || code == LanguageCodePaliSiam || code == LanguageCodeThaiMahaChula) {
        if (volume <= 8) {
            return 1;
        } else if (volume <= 33) {
            return 2;
        } else {
            return 3;
        }
    } else if (code == LanguageCodeThaiMahaMakut) {
        if (volume <= 9) {
            return 1;
        } else if (volume <= 74) {
            return 2;
        } else {
            return 3;
        }
    } else if (code == LanguageCodeRomanScript) {
        if (volume <= 5) {
            return 1;
        } else if (volume <= 48) {
            return 2;
        } else {
            return 3;
        }
    }
    return 1;
}

+ (NSInteger)pageOfItem:(NSInteger)item subItem:(NSInteger)subItem inVolume:(NSInteger)volume withCodeString:(NSString *)codeString
{
    if ([codeString isEqualToString:@"thaimc"]) {
        NSDictionary *dict = [BookDatabaseHelper itemMappingTableMc];
        return [dict[[NSString stringWithFormat:@"v%d-%d-i%d", volume, subItem, item]] integerValue];
    } else if ([codeString isEqualToString:@"thaims"]) {
        NSDictionary *dict = [BookDatabaseHelper itemMappingTableMs];
        return [dict[[NSString stringWithFormat:@"v%d-%d-i%d", volume, subItem, item]] integerValue];
    } else if ([codeString isEqualToString:@"romanct"]) {
        NSDictionary *dict = [BookDatabaseHelper romanPageIndexTable];
        NSString *kVolume = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:volume]];
        NSString *kItem = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:item]];
        NSString *kSubItem = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:subItem]];        
        if (!dict[kVolume] || !dict[kVolume][kItem] || !dict[kVolume][kItem][kSubItem])
        {
            return 0;
        }
        return [dict[kVolume][kItem][kSubItem] integerValue];
    } else {
        NSDictionary *dict = [BookDatabaseHelper bookItems];
        codeString = [codeString isEqualToString:@"palinew"] ? @"pali" : codeString;
        return [dict[codeString][[NSString stringWithFormat:@"%d", volume]][[NSString stringWithFormat:@"%d", subItem]][[NSString stringWithFormat:@"%d", item]][0] integerValue];
    }
}

+ (NSInteger)pageOfItem:(NSInteger)item subItem:(NSInteger)subItem inVolume:(NSInteger)volume withCode:(LanguageCode)code
{
    switch (code) {
        case LanguageCodeThaiSiam:
            return [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:@"thai"];
        case LanguageCodePaliSiam:
        case LanguageCodePaliSiamNew:
            return [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:@"pali"];
        case LanguageCodeThaiMahaMakut:
            return [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:@"thaimm"];
        case LanguageCodeThaiMahaChula:
            return [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:@"thaimc"];
        case LanguageCodeThaiSupreme:
            return [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:@"thaims"];
        case LanguageCodeRomanScript:
            return [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:@"romanct"];
        default:
            return 0;
    }
}

+ (NSArray *)book:(NSInteger)code
{
    for (NSArray *book in [BookDatabaseHelper books]) {
        if ([book[0] intValue] == code) {
            return book;
            break;
        }
    }
    return nil;
}

+ (NSArray *)books
{
    return @[@[[NSNumber numberWithInt:LanguageCodeThaiSiam], @"thai", NSLocalizedString(@"Thai Royal", nil)],
             @[[NSNumber numberWithInt:LanguageCodePaliSiam], @"pali", NSLocalizedString(@"Pali Siam", nil)],
             @[[NSNumber numberWithInt:LanguageCodePaliSiamNew], @"palinew", NSLocalizedString(@"New Pali Siam", nil)],
             @[[NSNumber numberWithInt:LanguageCodeThaiWatna], @"thaiwn", NSLocalizedString(@"Buddhawajana Tipitaka", nil)],
             @[[NSNumber numberWithInt:LanguageCodeThaiMahaMakut], @"thaimm", NSLocalizedString(@"Thai Maha Makut", nil)],
             @[[NSNumber numberWithInt:LanguageCodeThaiMahaChula], @"thaimc", NSLocalizedString(@"Thai Maha Chula", nil)],
             @[[NSNumber numberWithInt:LanguageCodeThaiMahaChula2], @"thaimc", NSLocalizedString(@"Thai Maha Chula 2", nil)],
             @[[NSNumber numberWithInt:LanguageCodeThaiSupreme], @"thaims", NSLocalizedString(@"Thai Supreme", nil)],
             @[[NSNumber numberWithInt:LanguageCodeRomanScript], @"romanct", NSLocalizedString(@"Roman Script", nil)]
             ];
}

+ (NSArray *)comparableBooks
{
    return @[
             [BookDatabaseHelper books][0],
             [BookDatabaseHelper books][1],
             [BookDatabaseHelper books][3],
             [BookDatabaseHelper books][4]
             ];
}

+(NSString *)shortName:(LanguageCode)code
{
    switch (code) {
        case LanguageCodeThaiSiam:
            return NSLocalizedString(@"THAI_SHORT", nil);
        case LanguageCodePaliSiam:
            return NSLocalizedString(@"PALI_SHORT", nil);
        case LanguageCodePaliSiamNew:
            return NSLocalizedString(@"PALINEW_SHORT", nil);
        case LanguageCodeThaiFiveBooks:
            return NSLocalizedString(@"THAIBT_SHORT", nil);
        case LanguageCodeThaiMahaChula:
            return NSLocalizedString(@"THAIMC_SHORT", nil);
        case LanguageCodeThaiMahaChula2:
            return NSLocalizedString(@"THAIMC2_SHORT", nil);
        case LanguageCodeThaiMahaMakut:
            return NSLocalizedString(@"THAIMM_SHORT", nil);
        case LanguageCodeThaiWatna:
            return NSLocalizedString(@"THAIWN_SHORT", nil);
        case LanguageCodeRomanScript:
            return NSLocalizedString(@"Roman Script", nil);
        case LanguageCodeThaiSupreme:
            return NSLocalizedString(@"THAIMS_SHORT", nil);
        default:
            return @"";
    }
}

@end
