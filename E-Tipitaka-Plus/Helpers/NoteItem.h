//
//  NoteItem.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/9/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteItem : NSObject

@property (nonatomic, strong) NSString *note;
@property (nonatomic, assign) NSUInteger state;

- (instancetype)initWithNote:(NSString *)note state:(NSUInteger)state;

@end
