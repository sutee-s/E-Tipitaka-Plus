//
//  HighlightDict.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 3/2/23.
//  Copyright © 2023 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMResultSet.h>


@interface HighlightDict : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, assign) DictionaryType type;
@property (nonatomic, assign) NSInteger wid;
@property (nonatomic, strong) NSString *selection;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, assign) NSRange range;
@property (nonatomic, strong) NSString *color;
@property (nonatomic, strong) NSString *note;

- (id)initFromResultSet:(FMResultSet *)s;
- (NSDictionary *)dictionaryValue;

@end

