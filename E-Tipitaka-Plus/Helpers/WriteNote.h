//
//  WriteNote.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WriteNote : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, assign) BOOL starred;
@property (nonatomic, assign) NSInteger rank;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *plainNote;
@property (nonatomic, strong) NSString *snippet;
@property (nonatomic, assign) BOOL main;

- (NSDictionary *)dictionaryValue;

@end
