//
//  ReadingPageViewModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 25/5/15.
//  Copyright (c) 2015 Watnapahpong. All rights reserved.
//

#import <ReactiveObjC/ReactiveObjC.h>
#import "ReadingPageViewModel.h"
#import "ETDataModel.h"
#import "SharedDataModel.h"

@interface ReadingPageViewModel ()

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL pageIsAnimating;

@end

@implementation ReadingPageViewModel

@synthesize dataModel = _dataModel;
@synthesize code = _code;
@synthesize volume = _volume;
@synthesize keywords = _keywords;
@synthesize searchType = _searchType;
@synthesize results = _results;
@synthesize pageIsAnimating = _pageIsAnimating;
@synthesize resultsChangeSignal = _resultsChangeSignal;
@synthesize pageChangeSignal = _pageChangeSignal;

+ (instancetype)sharedInstance
{
    static ReadingPageViewModel *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [ReadingPageViewModel new];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.code = LanguageCodeThaiSiam;
    self.volume = 1;
    self.page = 1;
    self.resultsChangeSignal = RACObserve(self, results);
    self.pageChangeSignal = [RACObserve(self, page) distinctUntilChanged];
}

- (void)setCode:(LanguageCode)code
{
    if (_code == code && self.dataModel) { return; }
    _dataModel = [SharedDataModel sharedDataModel:code];
    _code = code;
}

- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page keywords:(NSString *)keywords
{
    _keywords = keywords;
    if (self.code != code || self.volume != volume || !self.results) {
        self.code = code;
        self.volume = volume;
        _results = [self.dataModel queryPagesInVolume:volume];
    }
    self.page = page;
}

- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page
{
    [self open:code volume:volume page:page keywords:nil];
}

- (void)open:(LanguageCode)code volume:(NSInteger)volume
{
    [self open:code volume:volume page:1];
}

- (void)open:(LanguageCode)code{
    [self open:code volume:1 page:1];
}

@end
