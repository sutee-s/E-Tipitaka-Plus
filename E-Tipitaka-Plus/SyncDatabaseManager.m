//
//  SyncDatabaseManager.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 24/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "SyncDatabaseManager.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import <AFNetworking/AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <BlocksKit/BlocksKit.h>
#import "AccountHelper.h"
#import "NSData+MD5.h"
#import "Constants.h"
#import "UserDatabaseHelper.h"

@interface SyncDatabaseManager()

@property (nonatomic, strong) NSMutableDictionary *timers;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSMutableDictionary *uploadTasks;
@property (nonatomic, strong) NSURLSessionUploadTask *uploadDatabasesTask;
@property (nonatomic, strong) NSTimer *dismissTimer;

@end

@implementation SyncDatabaseManager

@synthesize timers = _timers;
@synthesize timer = _timer;
@synthesize syncCompleted = _syncCompleted;
@synthesize uploadTasks = _uploadTasks;

- (NSMutableDictionary *)uploadTasks
{
    if (!_uploadTasks) {
        _uploadTasks = [NSMutableDictionary new];
    }
    return _uploadTasks;
}

- (void)checkSyncCompleted
{
    if (self.enabled && !self.syncCompleted) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Incomplete Sync", nil) message:NSLocalizedString(@"Your data may be lost if you try to modify the data.", nil) delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (BOOL)syncCompleted
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [AccountHelper currentToken] && [[userDefaults objectForKey:@"sync_completed"] boolValue];
}

- (void)setSyncCompleted:(BOOL)syncCompleted
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSNumber numberWithBool:syncCompleted] forKey:@"sync_completed"];
    [userDefaults synchronize];
}

- (void)database:(NSString *)dbName edited:(BOOL)edited
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *editedDb = [[userDefaults objectForKey:@"edited_db"] mutableCopy];
    if (!editedDb) {
        editedDb = [NSMutableDictionary dictionary];
        [editedDb setObject:[NSNumber numberWithBool:edited] forKey:dbName];
    }
    [userDefaults setObject:editedDb forKey:@"edited_db"];
    [userDefaults synchronize];
}

- (BOOL)databaseIsEdited:(NSString *)dbName
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *editedDb = [userDefaults objectForKey:@"edited_db"];
    return [[editedDb objectForKey:dbName] boolValue];
}

- (NSMutableDictionary *)timers
{
    if (!_timers) {
        _timers = [NSMutableDictionary dictionary];
    }
    return _timers;
}

+ (instancetype)sharedInstance
{
    static SyncDatabaseManager *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[SyncDatabaseManager alloc] init];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tapToDismiss:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
    });
    return __sharedInstance;
}

-(void)tapToDismiss:(NSNotification *)notification {
    [SVProgressHUD showErrorWithStatus:@"Cancel"];
    [self cancelAllUploadTasks];
}

-(void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
        NSProgress *progress = (NSProgress *)object;
        NSLog(@"Progress is %f", progress.fractionCompleted);
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showProgress:progress.fractionCompleted status:@"Uploading..."];
        });
    }
}

- (BOOL)enabled
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [[userDefaults objectForKey:@"auto_sync_enabled_preference"] boolValue];
}

- (void)dismiss:(NSTimer *)timer
{
    [self cancelAllUploadTasks];
}

- (void)syncManuallyWithProgressHUD:(void (^)(BOOL success))block inViewController:(UIViewController *)controller
{
    if (![AccountHelper currentToken]) {
        block(NO);
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sync Data", nil) message:NSLocalizedString(@"Please choose sync method", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Upload" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD dismiss];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Sync data"];
        [self pushAll:^(BOOL success) {
            if (block) {
                block(success);
            }
            [SVProgressHUD dismiss];
            if (success) {
                [SVProgressHUD showSuccessWithStatus:@"Sync complete"];
            } else {
                [SVProgressHUD showErrorWithStatus:@"Sync error"];
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD dismiss];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Sync data"];
        [self syncPull:^(BOOL success) {
            if (block) {
                block(success);
            }            
            [SVProgressHUD dismiss];
            if (success) {
                [SVProgressHUD showSuccessWithStatus:@"Sync complete"];
            } else {
                [SVProgressHUD showErrorWithStatus:@"Sync error"];
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];

    [controller presentViewController:alert animated:YES completion:nil];
}

- (void)syncWithProgressHUD:(void (^)(BOOL success))block
{
    __block NSProgress *progress;
    
    if ([AccountHelper currentToken]) {
        [SVProgressHUD dismiss];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Sync data"];
        NSLog(@"%@", @"sync pull user databases");
        
        [self.dismissTimer invalidate];
        
        self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(dismiss:) userInfo:nil repeats:NO];
        
        [[SyncDatabaseManager sharedInstance] sync:^(BOOL success, BOOL firstTime) {
            if (success && !firstTime) {
                [SVProgressHUD showSuccessWithStatus:@"Sync complete"];
                [self.dismissTimer invalidate];
                if (block) {
                    block(YES);
                }
            } else if (success & firstTime) {
                progress = [[SyncDatabaseManager sharedInstance] pushAll:^(BOOL success) {
                    [self.dismissTimer invalidate];
                    [SVProgressHUD dismiss];
                    [SVProgressHUD showSuccessWithStatus:@"Sync complete"];
                    if (block) {
                        block(YES);
                    }
                }];
                [progress addObserver:self
                           forKeyPath:@"fractionCompleted"
                              options:NSKeyValueObservingOptionNew
                              context:NULL];
            } else {
                [self.dismissTimer invalidate];
                [SVProgressHUD dismiss];                
                [SVProgressHUD showErrorWithStatus:@"Sync error"];
                if (block) {
                    block(NO);
                }
            }
        }];
    }
}

- (NSProgress *)push:(NSString *)dbPath onSuccess:(void (^)(BOOL))block
{
    NSProgress *progress;
    
    void (^bodyBlock)(id<AFMultipartFormData>) = ^ (id<AFMultipartFormData> formdata) {
        NSURL *filePath = [NSURL fileURLWithPath:dbPath];
        NSError *error;
        [formdata appendPartWithFileURL:filePath name:@"file" fileName:filePath.lastPathComponent mimeType:@"application/sqlite" error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    };
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:SYNC_TIME_FORMAT];
    NSDate *currentDate = [NSDate date];
    NSString *stringFromDate = [formatter stringFromDate:currentDate];
    
    NSMutableURLRequest *request
    = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                 URLString:[NSString stringWithFormat:@"%@/sync_data/", USER_DATA_HOST]
                                                                parameters:@{@"timestamp":stringFromDate}
                                                 constructingBodyWithBlock:bodyBlock
                                                                     error:nil];
    
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask = [self.uploadTasks objectForKey:dbPath];
    
    if (uploadTask) {
        [uploadTask cancel];
    }

    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:&progress
                  completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                      if (error) {
                          if (block) {
                              block(NO);
                          }
                          NSLog(@"Error: %@", error);
                      } else {
                          if (block) {
                              block(YES);
                          }
                          NSLog(@"%@ %@", response, responseObject);
                          // write sync time
                          [self writeTimestamp:dbPath timestamp:currentDate];
                          [self database:dbPath.lastPathComponent edited:NO];
                      }
                  }];
    
    [self.uploadTasks setObject:uploadTask forKey:dbPath];
    
    [uploadTask resume];
    
    return progress;
}

- (void)push:(NSTimer *)timer
{
    NSString *dbPath = timer.userInfo;
    
    [self.timers removeObjectForKey:dbPath.lastPathComponent];
    
    void (^bodyBlock)(id<AFMultipartFormData>) = ^ (id<AFMultipartFormData> formdata) {
        NSURL *filePath = [NSURL fileURLWithPath:dbPath];
        NSError *error;
        [formdata appendPartWithFileURL:filePath name:@"file" fileName:filePath.lastPathComponent mimeType:@"application/sqlite" error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    };
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:SYNC_TIME_FORMAT];
    NSDate *currentDate = [NSDate date];
    NSString *stringFromDate = [formatter stringFromDate:currentDate];
    
    NSMutableURLRequest *request
    = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                 URLString:[NSString stringWithFormat:@"%@/sync_data/", USER_DATA_HOST]
                                                                parameters:@{@"timestamp":stringFromDate}
                                                 constructingBodyWithBlock:bodyBlock
                                                                     error:nil];
    
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask = [self.uploadTasks objectForKey:dbPath];
    
    if (uploadTask) {
        [uploadTask cancel];
    }
    
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          [self writeTimestamp:dbPath timestamp:currentDate];
                          [self database:dbPath.lastPathComponent edited:NO];
                          NSLog(@"%@", responseObject);
                      }
                  }];
    
    [self.uploadTasks setObject:uploadTask forKey:dbPath];
    
    [uploadTask resume];
    
}

- (void)checkTimestamp:(NSString *)dbPath successBlock:(void (^)(BOOL))block
{
    [self syncInfo:^(NSArray *items) {
        NSString *dbName = [dbPath lastPathComponent];
        NSDate *timestamp;
        for (NSDictionary *item in items) {
            if ([dbName isEqualToString:item[@"fields"][@"name"]]) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:SYNC_TIME_FORMAT];
                timestamp = [dateFormatter dateFromString:item[@"fields"][@"created_at"]];
                break;
            }
        }
        if (block) {
            NSDate *currentTimestamp = [self readTimestamp:dbPath];
            if (currentTimestamp && [currentTimestamp compare:timestamp] >= 0) {
                block(YES);
            } else {
                block(NO);
            }
        }
    }];
}

- (void)delayPush:(NSString *)dbPath
{
    if (!AUTO_SYNC) return;
        
    dispatch_async(dispatch_get_main_queue(), ^{
        [self database:[dbPath lastPathComponent] edited:YES];
        if (!self.syncCompleted) {
            return;
        }
        
        if ([self.timers objectForKey:dbPath.lastPathComponent]) {
            [[self.timers objectForKey:dbPath.lastPathComponent] invalidate];
        }
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5  target:self selector:@selector(push:) userInfo:dbPath repeats:NO];
        [self.timers setObject:self.timer forKey:dbPath.lastPathComponent];        
    });
}

- (void)writeTimestamp:(NSString *)dbPath timestamp:(NSDate *)date
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:date forKey:dbPath.lastPathComponent];
    [userDefaults synchronize];
}

- (NSDate *)readTimestamp:(NSString *)dbPath
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:dbPath.lastPathComponent];
}

- (NSProgress *)pushDatabases:(NSArray *)dbPaths successBlock:(void (^)(BOOL))block
{
    if (dbPaths.count == 0) {
        block(YES);
        return nil;
    }
    
    void (^bodyBlock)(id<AFMultipartFormData>) = ^ (id<AFMultipartFormData> formdata) {
        int i = 0;
        for (NSString *path in dbPaths) {
            NSURL *filePath = [NSURL fileURLWithPath:path];
            NSError *error;
            unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil] fileSize];

            if (fileSize == 0) { break; }
            
            [formdata appendPartWithFileURL:filePath name:[NSString stringWithFormat:@"file%d", i] fileName:filePath.lastPathComponent mimeType:@"application/sqlite" error:&error];
            if (error) {
                NSLog(@"%@", error);
            }
            i++;
        }
    };
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:SYNC_TIME_FORMAT];
    NSDate *currentDate = [NSDate date];
    NSString *stringFromDate = [formatter stringFromDate:currentDate];
    
    NSMutableURLRequest *request
    = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                 URLString:[NSString stringWithFormat:@"%@/sync_data/", USER_DATA_HOST]
                                                                parameters:@{@"timestamp":stringFromDate}
                                                 constructingBodyWithBlock:bodyBlock
                                                                     error:nil];
    
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSProgress *progress;
    
    [self.uploadDatabasesTask cancel];
    self.uploadDatabasesTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:&progress
                  completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                      if (error) {
                          if (block) {
                              block(NO);
                          }
                          NSLog(@"Error: %@", error);
                      } else {
                          if (block) {
                              block(YES);
                          }
                          NSLog(@"%@ %@", response, responseObject);
                          // write sync time
                          for (NSString *dbPath in dbPaths) {
                              [self writeTimestamp:dbPath timestamp:currentDate];
                              [self database:dbPath.lastPathComponent edited:NO];
                          }
                          self.syncCompleted = YES;
                      }
                  }];
    
    [self.uploadDatabasesTask resume];

    return progress;
}

- (NSProgress *)pushAll:(void (^)(BOOL))block
{
    return [self pushDatabases:SYNC_DATABASES
                  successBlock:block];
}

- (void)downloadFile:(NSString *)fileName successBlock:(void (^)(BOOL))block
{
    if (![AccountHelper currentToken]) {
        return;
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/sync_data/%@/", USER_DATA_HOST, fileName]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    NSProgress *progress;
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:&progress destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:fileName];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (block && !error) {
            NSLog(@"File downloaded to: %@", filePath);
            block(YES);
        } else if (block && error) {
            NSLog(@"%@", error);
            block(NO);
        }
    }];
    
    [downloadTask resume];
}

- (void)pull:(void (^)(BOOL))block items:(NSArray *)items
{
    __block BOOL syncCompleted = YES;
    NSMutableArray *requestArray = [NSMutableArray array];
    for (NSDictionary *item in items) {
        NSString *name = item[@"fields"][@"name"];

        NSString *filePath = [DOCUMENT_PATH stringByAppendingPathComponent:name];
        
        NSDate *fileDate = [self readTimestamp:filePath];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:SYNC_TIME_FORMAT];
        NSDate *date = [dateFormatter dateFromString:item[@"fields"][@"created_at"]];
        NSLog(@"%@", date);
        
        NSString *md5 = [[NSData dataWithContentsOfFile:filePath] MD5];
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil] fileSize];
        
        if (!fileExists || !fileSize || !fileDate || ![md5 isEqualToString:item[@"fields"][@"checksum"]]) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/sync_data/%@/", USER_DATA_HOST, name]];
            NSMutableURLRequest *request = [[NSURLRequest requestWithURL:url] mutableCopy];
            [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
            AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            [AFImageResponseSerializer serializer];
            __block NSString *tmpFile = [DOCUMENT_PATH stringByAppendingPathComponent:[name stringByAppendingString:@".tmp"]];
            [[NSFileManager defaultManager] removeItemAtPath:tmpFile error:nil];
            
            op.outputStream = [NSOutputStream outputStreamToFileAtPath:tmpFile append:NO];
            op.queuePriority = NSOperationQueuePriorityLow;
            [op setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead){
                
            }];
            [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:tmpFile error:nil] fileSize];
                if (fileSize) {
                    NSData *data = [[NSFileManager defaultManager] contentsAtPath:tmpFile];
                    [data writeToFile:filePath atomically:YES];
                    [self writeTimestamp:filePath timestamp:date];
                    [self database:filePath.lastPathComponent edited:NO];
                    syncCompleted = syncCompleted && YES;
                    [[NSFileManager defaultManager] removeItemAtPath:tmpFile error:nil];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                syncCompleted = syncCompleted && NO;
            }];
            [requestArray addObject:op];
        }
    }
    
    if (requestArray.count == 0) {
        block(YES);
        return;
    }
    
    NSArray *batches = [AFURLConnectionOperation batchOfRequestOperations:requestArray progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
    } completionBlock:^(NSArray *operations) {
        [UserDatabaseHelper updateHighlightDatabase];
        [UserDatabaseHelper updateHighlightColorDatabase];
        //after all operations are completed this block is called
        if (block) {
            block(syncCompleted);
        }
    }];
    
    [[NSOperationQueue mainQueue] addOperations:batches waitUntilFinished:NO];
}

- (void)syncInfo:(void (^)(NSArray *))block
{
    NSURL *baseURL = [NSURL URLWithString:USER_DATA_HOST];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    [manager GET:@"sync_data_list/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject) {
            NSData *jsonData = [responseObject[@"items"] dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error;
            NSArray *items = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            NSLog(@"%@", items);
            if (block) {
                block(items);
            }
        } else {
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
        block(nil);
    }];
    
}

- (void)syncPull:(void (^)(BOOL))block
{
    [self syncInfo:^(NSArray *items) {
        if (block && items && items.count == 0) {
            block(YES);
        } else if (block && items && items.count > 0) {
            self.syncCompleted = NO;
            [self pull:^(BOOL success) {
                self.syncCompleted = success;
                block(success);
            } items:items];
        } else if (block) {
            block(NO);
        }
    }];
}

- (void)sync:(void (^)(BOOL, BOOL))block
{
    NSArray *editedDbPaths = [SYNC_DATABASES bk_reject:^BOOL(NSString *path) {
        return ![self databaseIsEdited:path];
    }];

    [self syncInfo:^(NSArray *items) {
        if (block) {
            if (items && items.count == 0) {
                block(YES, YES);
            } else if (items.count > 0) {
                [self pushDatabases:editedDbPaths successBlock:^(BOOL success) {
                    // write sync incompleted
                    self.syncCompleted = NO;
                    [self pull:^(BOOL success) {
                        if (success) {
                            self.syncCompleted = YES;
                        }
                        block(success, NO);
                    } items:items];
                }];
            } else {
                block(NO, NO);
            }
        }
    }];
}

- (void)cancelAllUploadTasks
{
    [self.uploadDatabasesTask cancel];
    [self.uploadTasks enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSURLSessionUploadTask* obj, BOOL * stop) {
        [obj cancel];
    }];
}

@end
