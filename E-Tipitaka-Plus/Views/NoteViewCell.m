//
//  NoteViewCell.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "NoteViewCell.h"

@implementation NoteViewCell

@synthesize noteLabel = _noteLabel;
@synthesize volumeLabel = _volumeLabel;
@synthesize infoLabel = _infoLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
