//
//  AutoCompleteFetchOperation.h
//
//  Created by Matthew Ott on 3/4/15.
//

#import <Foundation/Foundation.h>
#import "AutoCompleteModel.h"

@interface AutoCompleteFetchOperation : NSOperation {
    id<AutoCompleteSearchDelegate> searchDelegate;
    NSString* searchString;
    NSString* stringCode;
}
-(instancetype)initWithSearchString:(NSString*)string code:(NSString *)code delegate:(id<AutoCompleteSearchDelegate>)delegate;
@end
