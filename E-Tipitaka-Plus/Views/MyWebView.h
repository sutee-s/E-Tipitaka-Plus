//
//  MyWebView.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 6/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWebView : UIWebView

@end
