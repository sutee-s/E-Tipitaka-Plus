//
//  MyTabBarController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 10/4/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTabBarController : UITabBarController

@end
