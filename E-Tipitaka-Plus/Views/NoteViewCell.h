//
//  NoteViewCell.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *noteLabel;
@property (nonatomic, weak) IBOutlet UILabel *volumeLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UILabel *tagLabel;

@end
