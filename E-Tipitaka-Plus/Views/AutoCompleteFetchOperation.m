//
//  AutoCompleteFetchOperation.m
//
//  Created by Matthew Ott on 3/4/15.
//

#import "AutoCompleteFetchOperation.h"
#import "AutoCompleteModel.h"
#import "UserDatabaseHelper.h"

@implementation AutoCompleteFetchOperation

-(instancetype)initWithSearchString:(NSString*)string code:(NSString *)code delegate:(id<AutoCompleteSearchDelegate>)delegate {
    if (self = [super init]) {
        searchDelegate = delegate;
        self->searchString = string;
        self->stringCode = code;
    }
    return self;
}

- (void)main {
    if (searchDelegate) {
        NSMutableArray* results = [@[] mutableCopy];
        // Don't bother looking up new results if the input is empty.
        NSString *input = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (searchString && ![@"" isEqualToString:input] && input.length > 2 && ([stringCode isEqualToString:@"thai"] || [stringCode hasPrefix:@"thai"])) {
            for (NSString *word in [UserDatabaseHelper getSuggestWords:input code:@"thai"]) {
                [results addObject:word];
            }
        }
        
        // Alphabetically sort the results.
        [results sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if ([obj1 isKindOfClass:[NSString class]] && [obj2 isKindOfClass:[NSString class]]) {
                return [(NSString*)obj1 compare:(NSString*)obj2];
            } else {
                return NSOrderedSame;
            }
        }];

        [(NSObject*)searchDelegate performSelectorOnMainThread:@selector(onAutoCompleteResultsReceived:)
                                                    withObject:results
                                                 waitUntilDone:NO];
    }
}

@end
