//
//  AutoCompleteSearchBar.h
//
//  Created by Matthew Ott on 2/27/15.
//

#import <UIKit/UIKit.h>
#import "AutoCompleteFetchOperation.h"

@class AutoCompleteSearchBar;

@protocol AutoCompleteSearchBarDataSource <NSObject>

- (NSString *)autoCompleteSearchBarStringCode;

@end

@protocol AutoCompleteSearchBarDelegate <NSObject>

- (void)autoCompleteSearchBar:(AutoCompleteSearchBar *)searchBar didSelectSuggest:(NSString *)suggest;

@end

@interface AutoCompleteSearchBar : UISearchBar<AutoCompleteSearchDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSArray* autoCompleteResults;
    UIGestureRecognizer* tapGestureRecognizer;
}

@property (strong, readwrite) UITableView *autoCompleteTableView;
@property NSOperationQueue* operationQueue;
@property (nonatomic, weak) id<AutoCompleteSearchBarDataSource> autoCompleteDataSource;
@property (nonatomic, weak) id<AutoCompleteSearchBarDelegate> autoCompleteDelegate;


- (void)reloadData;
- (void)hideAutoCompleteView;
@end
