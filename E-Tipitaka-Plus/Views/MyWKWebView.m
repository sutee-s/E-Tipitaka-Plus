//
//  MyWKWebView.m
//  E-Tipitaka-Plus
//
//  Created by SUTEE SUDPRASERT on 6/11/2567 BE.
//  Copyright © 2567 BE Watnapahpong. All rights reserved.
//

#import "MyWKWebView.h"

@implementation MyWKWebView

- (void)buildMenuWithBuilder:(id<UIMenuBuilder>)builder API_AVAILABLE(ios(13.0))  {
  if (@available(iOS 16.0, *)) {
    [builder removeMenuForIdentifier:UIMenuLookup];
    [builder removeMenuForIdentifier:UIMenuShare];
    [builder removeMenuForIdentifier:UIMenuSpeech];
    [builder removeMenuForIdentifier:UIMenuStandardEdit];
  }
    
  if (@available(iOS 17.0, *)) {
    [builder removeMenuForIdentifier:UIMenuAutoFill];
  }
  [super buildMenuWithBuilder:builder];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
  if (action == NSSelectorFromString(@"copy:") || action == NSSelectorFromString(@"selectAll:")) {
    return YES;
  }
  
  if (action == NSSelectorFromString(@"cut:")) {
    return YES;
  }
          
  return NO;
}

-(BOOL)canBecomeFirstResponder {
  return YES;
}

@end
