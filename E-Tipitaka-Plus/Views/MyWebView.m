//
//  MyWebView.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 6/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "MyWebView.h"

@implementation MyWebView

- (void)buildMenuWithBuilder:(id<UIMenuBuilder>)builder API_AVAILABLE(ios(13.0))  {
  if (@available(iOS 16.0, *)) {
    [builder removeMenuForIdentifier:UIMenuLookup];
    [builder removeMenuForIdentifier:UIMenuShare];
    [builder removeMenuForIdentifier:UIMenuSpeech];
    [builder removeMenuForIdentifier:UIMenuStandardEdit];
  }
  [super buildMenuWithBuilder:builder];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
  if (action == NSSelectorFromString(@"copy:") || action == NSSelectorFromString(@"selectAll:")) {
    return YES;
  }
  
  if (action == NSSelectorFromString(@"cut:")) {
    return NO;
  }
    
  if (action == NSSelectorFromString(@"clear:")) {
    return NO;
  }
  
  return NO;
}

-(BOOL)canBecomeFirstResponder {
  return YES;
}

@end
