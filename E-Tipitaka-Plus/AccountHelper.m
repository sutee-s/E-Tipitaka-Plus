//
//  AccountHelper.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "AccountHelper.h"

@implementation AccountHelper

+ (NSString *)currentToken
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:@"AccountToken"]) {
        return [userDefaults objectForKey:@"AccountToken"][@"token"];
    }
    return nil;
}

+ (NSString *)currentUsername
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:@"AccountToken"]) {
        return [userDefaults objectForKey:@"AccountToken"][@"username"];
    }
    return nil;
}

+ (void)deleteCurrentAccount
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"AccountToken"];
    [userDefaults synchronize];
}

+ (void)updateCurrentAccount:(NSString *)username withToken:(NSString *)token
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@{@"username":username, @"token":token} forKey:@"AccountToken"];
    [userDefaults synchronize];
}

@end
