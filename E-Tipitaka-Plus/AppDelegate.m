//
//  AppDelegate.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 23/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "AppDelegate.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import "UserDatabaseHelper.h"
#import "MyDocument.h"
#import "HistoryDocument.h"

#import <SVProgressHUD/SVProgressHUD.h>
#import <QuartzCore/QuartzCore.h>

@import UIKit;

@implementation AppDelegate

@synthesize queries = _queries;

- (void)loadDocument:(NSString *)filename
{
    NSMetadataQuery *query = [[NSMetadataQuery alloc] init];
    
    [self.queries setObject:query forKey:filename];
    
    query.searchScopes = @[NSMetadataQueryUbiquitousDataScope];
    query.predicate = [NSPredicate predicateWithFormat:@"%K == %@", NSMetadataItemFSNameKey, filename];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queryDidFinishGathering:) name:NSMetadataQueryDidFinishGatheringNotification object:@{@"query":query, @"filename":filename}];
    
    [query startQuery];
}

- (void)queryDidFinishGathering:(NSNotification *)notification
{
    NSMetadataQuery *query = notification.object[@"query"];
    [query disableUpdates];
    [query stopQuery];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSMetadataQueryDidFinishGatheringNotification object:query];

    [self.queries setObject:[NSNull null] forKey:notification.object[@"filename"]];
	
    [self loadData:query filename:notification.object[@"filename"]];
}

- (void)loadData:(NSMetadataQuery *)query filename:(NSString *)filename
{
    if ([query resultCount] == 1) {
        NSMetadataItem *item = [query resultAtIndex:0];
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        NSLog(@"url = %@", url);
        
        MyDocument *doc = nil;
        if ([filename isEqualToString:kHistoriesFilename]) {
            doc = [[HistoryDocument alloc] initWithFileURL:url];
            NSLog(@"%@", doc.jsonString);
        }
        
	} else {
        NSURL *ubiq = [[NSFileManager defaultManager]
                       URLForUbiquityContainerIdentifier:nil];
        NSURL *ubiquitousPackage = [[ubiq URLByAppendingPathComponent:@"Documents"] URLByAppendingPathComponent:filename];
        NSLog(@"ubiquitousPackage = %@", ubiquitousPackage);
        
        MyDocument *doc = nil;
        if ([filename isEqualToString:kHistoriesFilename]) {
            doc = [[HistoryDocument alloc] initWithFileURL:ubiquitousPackage];
        }
        
        [doc importData];
        
        [doc saveToURL:doc.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            if (success) {
                [doc openWithCompletionHandler:^(BOOL success) {
                    NSLog(@"new document opened from iCloud");
                }];
            }
        }];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [SVProgressHUD setFadeInAnimationDuration:0.05];
    [SVProgressHUD setMinimumDismissTimeInterval:0];
    [SVProgressHUD setFadeOutAnimationDuration:0.05];
    
    self.queries = [[NSMutableDictionary alloc] init];
    
    NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];

    if (ubiq) {
        NSLog(@"iCloud access at %@", ubiq);
    } else {
        NSLog(@"No iCloud access");
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:HISTORY_DB_PATH]) {
        [UserDatabaseHelper updateHistoryDatabase];
    }
    
    [UserDatabaseHelper createHighlightDatabaseTable];
    [UserDatabaseHelper updateHighlightDatabase];
    
    [UserDatabaseHelper createHighlightNoteDatabaseTable];
    
    [UserDatabaseHelper createHighlightColorDatabaseTable];
    [UserDatabaseHelper updateHighlightColorDatabase];
    
    [UserDatabaseHelper createSavedLexiconDatabaseTable];
    
    [UserDatabaseHelper createUserDataDatabaseTable];
    
    [UserDatabaseHelper createTagDatabaseTable];
    [UserDatabaseHelper updateTagDatabase];
    
    [UserDatabaseHelper createReadingStateDatabaseTable];
    [UserDatabaseHelper updateReadingStateDatabase];
    [UserDatabaseHelper initReadingStateDatabase];
    
    [UserDatabaseHelper createOthersDatabaseTable];
    [UserDatabaseHelper createNoteDatabaseTable];
    [UserDatabaseHelper updateNoteDatabase];
  
    [UserDatabaseHelper createHighlightDictDatabaseTable];
    
    MMDrawerController * drawerController = (MMDrawerController *)self.window.rootViewController;
    [drawerController setMaximumRightDrawerWidth:UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad ? 350.0 : 280.0];
    [drawerController setMaximumLeftDrawerWidth:UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad ? 350.0 : 280.0];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [drawerController setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
        MMDrawerControllerDrawerVisualStateBlock block;
        block = [MMDrawerVisualState slideVisualStateBlock];
        if(block){
            block(drawerController, drawerSide, percentVisible);
        }
    }];
    
    NSURL *url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
    [self handleOpenURL:url];
        
    return YES;
}

- (BOOL)handleOpenURL:(NSURL *)url {
    if (url && [url isFileURL]) {
        [UserDatabaseHelper importUserDatabase:url];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [self handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [self handleOpenURL:url];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
