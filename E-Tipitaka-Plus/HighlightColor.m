//
//  HighlightColor.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "HighlightColor.h"

@implementation HighlightColor

@synthesize name = _name;
@synthesize inlineColors = _inlineColors;
@synthesize footerColors = _footerColors;
@synthesize noteColors = _noteColors;


@end
