//
//  AppDelegate.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 23/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong) NSMutableDictionary *queries;

@end
