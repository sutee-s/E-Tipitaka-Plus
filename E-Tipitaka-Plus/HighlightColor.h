//
//  HighlightColor.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HighlightColor : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *inlineColors;
@property (nonatomic, strong) NSMutableArray *footerColors;
@property (nonatomic, strong) NSMutableArray *noteColors;

@end
