//
//  Tag.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tag : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *priority;
@property (nonatomic, strong) NSMutableArray *history;
@property (nonatomic, strong) NSMutableArray *searchAllhistory;
@property (nonatomic, strong) NSMutableArray *note;
@property (nonatomic, strong) NSMutableArray *highlight;
@property (nonatomic, assign) LanguageCode code;

- (instancetype)initWithName:(NSString *)name;
- (NSDictionary *)dictionaryValue;

@end
