//
//  SearchAllTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "SearchAllTableViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "Models.h"
#import "NSString+Thai.h"
#import "PageInfo.h"
#import "UserDatabaseHelper.h"
#import "NoteItemsHistoryViewController.h"
#import "NoteItem.h"
#import "SearchAllHistory.h"

@interface SearchAllTableViewController ()<UISearchBarDelegate> {
    BOOL _marked;
}

@property (strong, nonatomic) NSArray *codes;
@property (strong, nonatomic) NSMutableArray *totalResults;
@property (strong, nonatomic) NSMutableArray *totalHistories;
@property (strong, nonatomic) NSString *keywords;
@property (strong, nonatomic) NSMutableArray *totalSkimmedItems;
@property (strong, nonatomic) NSMutableArray *totalReadItems;
@property (strong, nonatomic) NSMutableArray *totalMarkedItems;
@property (strong, nonatomic) NSMutableArray *totalNoteItems;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;


@end

@implementation SearchAllTableViewController


- (NSArray *)codes {
    if (!_codes) {
        _codes = @[@(LanguageCodeThaiSiam),
                   @(LanguageCodeThaiMahaChula),
                   @(LanguageCodeThaiMahaMakut),
                   @(LanguageCodeThaiSupreme)];
    }
    return _codes;
}

- (NSMutableArray *)totalNoteItems {
    if (!_totalNoteItems) {
        _totalNoteItems = [[NSMutableArray alloc] initWithCapacity:self.codes.count];
    }
    return _totalNoteItems;
}

- (NSMutableArray *)totalSkimmedItems {
    if (!_totalSkimmedItems) {
        _totalSkimmedItems = [[NSMutableArray alloc] initWithCapacity:self.codes.count];
    }
    return _totalSkimmedItems;
}

- (NSMutableArray *)totalReadItems {
    if (!_totalReadItems) {
        _totalReadItems = [[NSMutableArray alloc] initWithCapacity:self.codes.count];
    }
    return _totalReadItems;
}

- (NSMutableArray *)totalMarkedItems {
    if (!_totalMarkedItems) {
        _totalMarkedItems = [[NSMutableArray alloc] initWithCapacity:self.codes.count];
    }
    return _totalMarkedItems;
}

- (NSMutableArray *)totalHistories {
    if (!_totalHistories) {
        _totalHistories = [[NSMutableArray alloc] initWithCapacity:self.codes.count];
    }
    return _totalHistories;
}

- (NSMutableArray *)totalResults {
    if (!_totalResults) {
        _totalResults = [[NSMutableArray alloc] initWithCapacity:self.codes.count];
    }
    return _totalResults;
}

- (void)refresh
{
    [self.totalNoteItems removeAllObjects];
    NSUInteger index = 0;
    for (History *history in self.totalHistories) {
        NSMutableDictionary* item = [UserDatabaseHelper getHistoryNoteItemsByRowId:history.rowId];
        [self.totalNoteItems insertObject:item atIndex:index];
        index++;
    }
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.delegate = self;
    self.clearsSelectionOnViewWillAppear = NO;
    self.navigationItem.title = NSLocalizedString(@"Search All Thai", nil);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadSearchAllHistory:) name:LoadSearchAllHistoryNotification object:nil];
}

- (void)doLoadSearchAllHistory:(SearchAllHistory *)searchAllHistory
{
    self.keywords = searchAllHistory.keywords;
    [self.totalSkimmedItems removeAllObjects];
    [self.totalReadItems removeAllObjects];
    [self.totalMarkedItems removeAllObjects];
    [self.totalResults removeAllObjects];
    [self.totalHistories removeAllObjects];
    self.searchBar.text = self.keywords;
    
    int index = 0;
    for (NSNumber *numberCode in self.codes) {
        History *history = [UserDatabaseHelper getHistory:searchAllHistory.keywords withCode:numberCode.intValue];
        NSArray *skimmedItems = [UserDatabaseHelper getHistorySkimmedItemsByRowId:history.rowId];
        NSArray *readItems = [UserDatabaseHelper getHistoryReadItemsByRowId:history.rowId];
        NSArray *markedItems = [UserDatabaseHelper getHistoryMarkedItemsByRowId:history.rowId];
        NSArray *results = [UserDatabaseHelper historyItemsById:history.rowId];
        
        [self.totalHistories insertObject:history atIndex:index];
        [self.totalSkimmedItems insertObject:skimmedItems atIndex:index];
        [self.totalReadItems insertObject:readItems atIndex:index];
        [self.totalMarkedItems insertObject:markedItems atIndex:index];
        [self.totalResults insertObject:results atIndex:index];
        index++;
    }
    [self refresh];
}

- (void)loadSearchAllHistory:(NSNotification *)notification
{
    [self.tabBarController setSelectedViewController:self.tabBarController.viewControllers[3]];
    SearchAllHistory *history = notification.object;
    [self doLoadSearchAllHistory:history];
}

- (NSString *)createHistoryDetail
{
    int index = 0;
    NSString *detail = @"";
    int total = 0;
    for (NSNumber *numberCode in self.codes) {
        self.code = numberCode.intValue;
        NSArray *results = [self.totalResults objectAtIndex:index];
        ;
        detail = [NSString stringWithFormat:@"%@ %@(%lu)", detail, [self.dataModel abbrTitle], results.count];
        total += results.count;
        index++;
    }
    return [[NSString stringWithFormat:NSLocalizedString(@"total %1$i pages:%2$@", nil), total, detail] stringByReplacingThaiNumeric];
}

- (void)doSearch:(NSString *)searchText
{
    [self.totalSkimmedItems removeAllObjects];
    [self.totalReadItems removeAllObjects];
    [self.totalMarkedItems removeAllObjects];
    [self.totalResults removeAllObjects];
    [self.totalHistories removeAllObjects];
    self.keywords = searchText;
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        int total = 0;
        int index = 0;
        for (NSNumber *numberCode in self.codes) {
            self.code = numberCode.intValue;
            NSString *status = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Searching", nil), self.dataModel.shortTitle];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:status];
            });
            NSArray *results = [self.dataModel search:searchText];
            
            [UserDatabaseHelper saveSearchResults:results ofKeywords:searchText ofType:SearchTypeAll withCode:self.code totalSearch:YES];
                        
            History *history = [UserDatabaseHelper getHistory:searchText withCode:self.code];
            [self.totalHistories insertObject:history atIndex:index];
            
            NSArray *skimmedItems = [UserDatabaseHelper getHistorySkimmedItemsByRowId:history.rowId];
            NSArray *readItems = [UserDatabaseHelper getHistoryReadItemsByRowId:history.rowId];
            NSArray *markedItems = [UserDatabaseHelper getHistoryMarkedItemsByRowId:history.rowId];
            
            [self.totalSkimmedItems insertObject:skimmedItems atIndex:index];
            [self.totalReadItems insertObject:readItems atIndex:index];
            [self.totalMarkedItems insertObject:markedItems atIndex:index];
            
            status = [NSString stringWithFormat:NSLocalizedString(@"Found %1$i times", nil), [results count]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:[status stringByReplacingThaiNumeric]];
            });
            total += [results count];
            [self.totalResults insertObject:results atIndex:index];
            [NSThread sleepForTimeInterval:1.0f];
            index++;
        }
        
        NSString *detail = [self createHistoryDetail];
        [UserDatabaseHelper saveSearchAllHistory:searchText withDetail:detail];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *status = [NSString stringWithFormat:NSLocalizedString(@"Totally found %1$i times", nil), total];
            [SVProgressHUD showSuccessWithStatus:[status stringByReplacingThaiNumeric]];
            [self refresh];
        });
    });
}

- (void)saveMarking:(NSInteger)volume page:(NSInteger)page
{
    History *history = [self.totalHistories objectAtIndex:self.selectedIndexPath.section];
    if (history) {
        [UserDatabaseHelper updateHistoryMarkedItem:history.rowId volume:volume page:page];
        [self.totalMarkedItems replaceObjectAtIndex:self.selectedIndexPath.section withObject:[UserDatabaseHelper getHistoryMarkedItemsByRowId:history.rowId]];
        [self refresh];
    }
}

- (void)removeMarking:(NSInteger)volume page:(NSInteger)page
{
    History *history = [self.totalHistories objectAtIndex:self.selectedIndexPath.section];
    if (history) {
        [UserDatabaseHelper deleteHistoryMarkedItem:history.rowId volume:volume page:page];
        [self.totalMarkedItems replaceObjectAtIndex:self.selectedIndexPath.section withObject:[UserDatabaseHelper getHistoryMarkedItemsByRowId:history.rowId]];
        [self refresh];
    }
}

- (void)markItem
{
    NSArray *results = [self.totalResults objectAtIndex:self.selectedIndexPath.section];
    NSDictionary *result = [results objectAtIndex:self.selectedIndexPath.row];
    if (_marked) {
        [self saveMarking:[[result objectForKey:@"volume"] integerValue] page:[[result objectForKey:@"page"] integerValue]];
    } else {
        [self removeMarking:[[result objectForKey:@"volume"] integerValue] page:[[result objectForKey:@"page"] integerValue]];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Note"]) {
        NoteItemsHistoryViewController *controller = (NoteItemsHistoryViewController *)segue.destinationViewController;
        History *history = [self.totalHistories objectAtIndex:self.selectedIndexPath.section];
        controller.rowId = history.rowId;
        controller.index = self.selectedIndexPath.row;
    }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    self.selectedIndexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSArray *markedItems = [self.totalMarkedItems objectAtIndex:self.selectedIndexPath.section];
        _marked = ![markedItems containsObject:[NSNumber numberWithInteger:self.selectedIndexPath.row]];
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose action", nil) message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Compare", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(_marked ? NSLocalizedString(@"Starred", nil) : NSLocalizedString(@"Unstarred", nil), nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self markItem];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(NSLocalizedString(@"Take note", nil), nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self performSegueWithIdentifier:@"Show Note" sender:self];
        }]];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                      popoverPresentationController];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
        popPresenter.sourceView = cell;
        popPresenter.sourceRect = cell.bounds;
                
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    
    LanguageCode code = [[self.codes objectAtIndex:indexPath.section] intValue];
    NSArray *results = [self.totalResults objectAtIndex:indexPath.section];
    NSDictionary *result = [results objectAtIndex:indexPath.row];
    PageInfo *pageInfo = [[PageInfo alloc] initWithCode:code volume:[result[@"volume"] integerValue] page:[result[@"page"] integerValue]];
    pageInfo.keywords = self.keywords;
    [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabasAndVolumeAndPageNotification object:pageInfo];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.codes.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.totalResults.count && [self.totalResults objectAtIndex:section] ? [[self.totalResults objectAtIndex:section] count] : 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    self.code = [[self.codes objectAtIndex:section] intValue];
    NSArray *results = self.totalResults.count ? [self.totalResults objectAtIndex:section] : nil;
     
    if (results) {
        NSString *countString = [[NSString stringWithFormat:NSLocalizedString(@"Found %1$i times", nil), results.count] stringByReplacingThaiNumeric];
        return [NSString stringWithFormat:@"%@ - %@", [self.dataModel shortTitle], countString];
    }
    return [self.dataModel shortTitle];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoteItem *noteItem = [[NoteItem alloc] initWithNote:@"" state:0];
    NSMutableDictionary *noteItems = [self.totalNoteItems objectAtIndex:indexPath.section];
    if ([noteItems objectForKey:[NSNumber numberWithInteger:indexPath.row]]) {
        noteItem = [noteItems objectForKey:[NSNumber numberWithInteger:indexPath.row]];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    NSMutableArray *markedItems = [self.totalMarkedItems objectAtIndex:indexPath.section];
    NSMutableArray *skimmedItems = [self.totalMarkedItems objectAtIndex:indexPath.section];
    NSMutableArray *readItems = [self.totalReadItems objectAtIndex:indexPath.section];
    
    UIImageView *starView = nil;
    if ([markedItems containsObject:[NSNumber numberWithInteger:indexPath.row]]) {
        starView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_star"]];
    }
    if ([skimmedItems containsObject:[NSNumber numberWithInteger:indexPath.row]]) {
        [cell setBackgroundColor:[UIColor colorWithRed:.67 green:.85 blue:.91 alpha:1]];
    } else if ([readItems containsObject:[NSNumber numberWithInteger:indexPath.row]]) {
        [cell setBackgroundColor:[UIColor colorWithRed:0.52 green:.80 blue:.99 alpha:1]];
    }
    
    UIButton *stateView = nil;
    if (noteItem.state > 0) {
        stateView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        if (noteItem.state == 1) {
            [stateView setTitle:@"✕" forState:UIControlStateNormal];
            [stateView setBackgroundColor:[UIColor redColor]];
        } else if (noteItem.state == 2) {
            [stateView setTitle:@"✔︎" forState:UIControlStateNormal];
            [stateView setBackgroundColor:[UIColor colorWithRed:0.196 green:0.8 blue:0.196 alpha:1]];
        }
        [stateView.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [stateView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    cell.accessoryView = nil;
    if (stateView && starView) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
        starView.frame = CGRectMake(0, 0, 20, 20);
        stateView.frame = CGRectMake(30, 0, 20, 20);
        [view addSubview:starView];
        [view addSubview:stateView];
        cell.accessoryView = view;
    } else if (stateView) {
        cell.accessoryView = stateView;
    } else if (starView) {
        cell.accessoryView = starView;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"SearchAll Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    [cell addGestureRecognizer:lpgr];
    
    NoteItem *noteItem = [[NoteItem alloc] initWithNote:@"" state:0];
    NSMutableDictionary *noteItems = [self.totalNoteItems objectAtIndex:indexPath.section];
    if ([noteItems objectForKey:[NSNumber numberWithInteger:indexPath.row]]) {
        noteItem = [noteItems objectForKey:[NSNumber numberWithInteger:indexPath.row]];
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
    
    NSArray *results = [self.totalResults objectAtIndex:indexPath.section];
    NSDictionary *result = [results objectAtIndex:indexPath.row];
    NSString *title = [[NSString stringWithFormat:NSLocalizedString(@"Volume %1$i Page %2$i", nil), [result[@"volume"] intValue], [result[@"page"] intValue]] stringByReplacingThaiNumeric];
    cell.textLabel.text = noteItem.note.length ? [NSString stringWithFormat:@"%@ (%@)", title, noteItem.note] : title;
    self.code = [[self.codes objectAtIndex:indexPath.section] intValue];
    cell.detailTextLabel.text = [self.dataModel volumeTitle:[result[@"volume"] intValue]];
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchText = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([searchText length] == 0) return;
    
    [searchBar resignFirstResponder];
    [self doSearch:searchText];
}


@end
