//
//  NoteSearchAllHistoryViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "NoteHistoryViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NoteSearchAllHistoryViewController : NoteHistoryViewController

@end

NS_ASSUME_NONNULL_END
