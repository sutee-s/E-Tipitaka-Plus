//
//  OtherHistoryListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherHistoryListViewController.h"
#import "History.h"
#import "UserDatabaseHelper.h"
#import "HistoryTableViewCell.h"
#import "BookDatabaseHelper.h"
#import "UIViewController+MMDrawerController.h"

@interface OtherHistoryListViewController ()

@property (nonatomic, strong) NSMutableArray *histories;

@end

@implementation OtherHistoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)refresh
{
    if (self.showAllSwitch.isOn) {
        self.histories = [[UserDatabaseHelper queryHistories:self.searchBar.text ofUser:self.user] mutableCopy];
    } else {
        self.histories = [[UserDatabaseHelper queryHistories:self.searchBar.text withCode:self.selectedCode ofUser:self.user] mutableCopy];
    }
    
    [self sort:nil];
}

- (IBAction)toggleShowAll:(id)sender
{
    [self refresh];
}

- (IBAction)sort:(id)sender
{
    self.histories = [[self.histories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        History *history1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        History *history2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [history1.created compare:history2.created];
    }] mutableCopy];
    
    self.histories = [[self.histories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        History *history1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        History *history2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [history1.keywords compare:history2.keywords];
            case 1:
                return [[NSNumber numberWithInteger:history1.state] compare:[NSNumber numberWithInteger:history2.state]];
            case 2:
                return [history1.created compare:history2.created];
            case 3:
                return [[NSNumber numberWithInteger:history1.priority] compare:[NSNumber numberWithInteger:history2.priority]];
            default:
                return [history1.created compare:history2.created];
        }
    }] mutableCopy];
    [self.tableView reloadData];
}

#pragma mark - Tablie view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mm_drawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [self.mm_drawerController openDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabaseNotification object:[NSNumber numberWithInt:self.selectedCode]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                History *history = [self.histories objectAtIndex:indexPath.row];
                [[NSNotificationCenter defaultCenter] postNotificationName:LoadHistoryNotification object:history];
            });
        }];
    }];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.histories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"History Cell";
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[HistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad ? 15 : 13];
    
    History *history = [self.histories objectAtIndex:indexPath.row];
    
    switch (history.state) {
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"state_01"];
            break;
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"state_02"];
            break;
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"state_03"];
            break;
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"state_04"];
            break;
        default:
            cell.imageView.image = nil;
            break;
    }
    
    NSString *title;
    if (self.showAllSwitch.isOn) {
        title = [NSString stringWithFormat:@"%@ - %@", [BookDatabaseHelper shortName:history.code], history.keywords];
    } else {
        title = history.keywords;
    }
    
    cell.textLabel.text = history.note.length ? [NSString stringWithFormat:@"%@ (%@)", title, history.note] : title;
    
    if ([UserDatabaseHelper isTipitaka:history.code]) {
        NSArray *tokens = [history.detail componentsSeparatedByString:@" "];
        NSString *detail = [NSString stringWithFormat:@"%@  %@", [[tokens subarrayWithRange:NSMakeRange(0, 2)] componentsJoinedByString:@" "], [[tokens subarrayWithRange:NSMakeRange(2, 3)] componentsJoinedByString:@"   "]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", detail];
    } else {
        if (history.type == SearchTypeBuddhawaj) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@)", history.detail, NSLocalizedString(@"Only Buddhawaj", nil)];
        } else {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", history.detail];
        }
    }
    
    cell.accessoryView = nil;
    if (history.noteState > 0) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        if (history.noteState == 1) {
            [button setTitle:@"✕" forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor redColor]];
        } else if (history.noteState == 2) {
            [button setTitle:@"✔︎" forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor colorWithRed:0.196 green:0.8 blue:0.196 alpha:1]];
        }
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.accessoryView = button;
    }
        
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

@end
