//
//  TagsViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "TagsViewController.h"
#import "Tag.h"
#import "UserDatabaseHelper.h"
#import "UIView+Snapshot.h"
#import "TagHighlightViewController.h"
#import "PageInfo.h" 
#import "TagBookmarkListViewController.h"
#import "TagHistoryListViewController.h"
#import "TagSearchAllListViewController.h"
#import "UITableView+LongPressMove.h"

#define kConfirmCreatingTagAlert    1000
#define kConfirmDeletingAlert       1001
#define kEditTagNameAlert           1002

@interface TagsViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, LongPressMoveDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) Tag *selectedTag;
@property (nonatomic, assign) LanguageCode newCode;

@end

@implementation TagsViewController

@synthesize items = _items;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (LanguageCode)newCode
{
    return self.segmentedControl.selectedSegmentIndex == 3 ? 0 : self.code;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeCode:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        self.code = [notification.object intValue];
        [self reloadData];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        [self reloadData];
        self.code = ((PageInfo *)notification.object).code;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Tag", nil), self.dataModel.shortTitle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.longPressMoveDelegate = self;
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    recognizer.minimumPressDuration = 1.0;
    recognizer.delegate = self;
    [self.tableView addGestureRecognizer:recognizer];
    [self.tableView enableLongPressMove:2];
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadData];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.selectedTag = [self.items objectAtIndex:indexPath.row];
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose action", nil) message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Edit tag name", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self edit];
        }]];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                      popoverPresentationController];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        popPresenter.sourceView = cell;
        popPresenter.sourceRect = cell.bounds;
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.items exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [self calculatePriority];
}

- (void)calculatePriority
{
    NSInteger start = 0;
    for (Tag *tag in self.items) {
        if (tag.priority.integerValue != start) {
            [UserDatabaseHelper updateTagPriority:[NSNumber numberWithInteger:start] ofTag:tag.name withCode:self.newCode];
        }
        start += 1;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)reloadData
{
    self.items = [UserDatabaseHelper queryTags:self.newCode];
    [self.tableView reloadData];
}

- (IBAction)add:(id)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedString(@"Create a new tag", nil)
                                message:NSLocalizedString(@"Enter tag name", nil)
                                preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {}];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *input = [alert textFields][0];
        NSString *name = input.text;
        if (name.length) {
            [UserDatabaseHelper addTag:name withCode:self.newCode];
            [self reloadData];
        }
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)edit
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedString(@"Edit tag name", nil)
                                message:NSLocalizedString(@"Enter tag name", nil)
                                preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = self.selectedTag.name;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *input = [alert textFields][0];
        LanguageCode code = self.segmentedControl.selectedSegmentIndex == 3 ? 0 : self.selectedTag.code;
        if (input.text.length > 0 && ![UserDatabaseHelper getTag:input.text withCode:code]) {
            [UserDatabaseHelper updateTagName:input.text ofTag:self.selectedTag];
            [self reloadData];
        }
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)confirmDelete
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:self.selectedTag.name
                                message:NSLocalizedString(@"Do you want to delete this tag?", nil)
                                preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction
                      actionWithTitle:NSLocalizedString(@"Yes", nil)
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction * action) {
        [UserDatabaseHelper deleteTag:self.selectedTag.name withCode:self.newCode];
        [self reloadData];
    }]];
    [alert addAction:[UIAlertAction
                      actionWithTitle:NSLocalizedString(@"No", nil)
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction * action) {}]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)changeType:(id)sender
{
    [self reloadData];
    if (self.segmentedControl.selectedSegmentIndex == 3) {
        self.navigationItem.title = NSLocalizedString(@"Tag", nil);
    } else {
        self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Tag", nil), self.dataModel.shortTitle];
    }
}

#pragma mark - Table view delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TagCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Tag *tag = [self.items objectAtIndex:indexPath.row];
    cell.textLabel.text = tag.name;
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", tag.history.count];
            break;
        case 1:
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", tag.note.count];
            break;
        case 2:
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", tag.highlight.count];
            break;
        case 3:
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", tag.searchAllhistory.count];
            break;
        default:
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", 0];
            break;
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        self.selectedTag = [self.items objectAtIndex:indexPath.row];
        [self confirmDelete];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tag *tag = [self.items objectAtIndex:indexPath.row];
    if (self.segmentedControl.selectedSegmentIndex == 0 && tag.history.count == 0) {
        return nil;
    }
    if (self.segmentedControl.selectedSegmentIndex == 1 && tag.note.count == 0) {
        return nil;
    }
    if (self.segmentedControl.selectedSegmentIndex == 2 && tag.highlight.count == 0) {
        return nil;
    }
    if (self.segmentedControl.selectedSegmentIndex == 3 && tag.searchAllhistory.count == 0) {
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.segmentedControl.selectedSegmentIndex == 2) {
        [self performSegueWithIdentifier:@"Highlight" sender:indexPath];
    } else if (self.segmentedControl.selectedSegmentIndex == 1) {
        [self performSegueWithIdentifier:@"Bookmark" sender:indexPath];
    } else if (self.segmentedControl.selectedSegmentIndex == 0) {
        [self performSegueWithIdentifier:@"History" sender:indexPath];
    } else if (self.segmentedControl.selectedSegmentIndex == 3) {
        [self performSegueWithIdentifier:@"SearchAllHistory" sender:indexPath];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = sender;
    if ([segue.identifier isEqualToString:@"Highlight"]) {
        TagHighlightViewController *controller = segue.destinationViewController;
        controller.tag = [self.items objectAtIndex:indexPath.row];
    } else if ([segue.identifier isEqualToString:@"Bookmark"]) {
        TagBookmarkListViewController *controller = segue.destinationViewController;
        controller.tag = [self.items objectAtIndex:indexPath.row];
    } else if ([segue.identifier isEqualToString:@"History"]) {
        TagHistoryListViewController *controller = segue.destinationViewController;
        controller.tag = [self.items objectAtIndex:indexPath.row];
    } else if ([segue.identifier isEqualToString:@"SearchAllHistory"]) {
        TagSearchAllListViewController *controller = segue.destinationViewController;
        controller.tag = [self.items objectAtIndex:indexPath.row];
    }
}

@end
