//
//  NoteItemsHistoryViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/9/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteItemsHistoryViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextView *noteTextView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, assign) NSInteger index;

@end
