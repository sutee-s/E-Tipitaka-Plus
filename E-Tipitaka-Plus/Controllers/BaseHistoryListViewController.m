//
//  BaseHistoryListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <HHPanningTableViewCell/HHPanningTableViewCell.h>

#import "BaseHistoryListViewController.h"
#import "UserDatabaseHelper.h"
#import "History.h"
#import "HistoryTableViewCell.h"
#import "BookDatabaseHelper.h"

@interface BaseHistoryListViewController()<HHPanningTableViewCellDelegate, UIAlertViewDelegate>

@end

@implementation BaseHistoryListViewController

@synthesize tableView = _tableView;
@synthesize showAllSwitch = _showAllSwitch;
@synthesize activeCell = _activeCell;
@synthesize activeHistory = _activeHistory;
@synthesize histories = _histories;
@synthesize selectedIndexPath = _selectedIndexPath;

- (void)updateState:(NSInteger)state atRowId:(NSInteger)rowId
{
    [UserDatabaseHelper updateHistoryAtColumn:@"state" setValue:[NSNumber numberWithInteger:state] byRowId:rowId];
}
  
- (void)setActiveItem:(NSIndexPath *)indexPath
{
    self.activeHistory = [self.histories objectAtIndex:indexPath.row];
}

- (NSString *)activeKeywords
{
    return self.activeHistory.keywords;
}

- (NSInteger)activeRowId
{
    return self.activeHistory.rowId;
}

- (NSString *)confirmDeleteMessage
{
    return @"";
}

- (void)refresh
{
    
}

- (NSInteger)itemsCount
{
    return self.histories.count;
}

- (NSInteger)stateAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.state;
}

- (LanguageCode)codeAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.code;
}

- (NSString *)keywordsAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.keywords;
}

- (NSString *)noteAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.note;
}

- (NSString *)detailAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.detail;
}

- (NSInteger)searchTypeAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.type;
}

- (NSInteger)noteStateAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.noteState;
}

- (NSInteger)rowIdAtIndexPath:(NSIndexPath *)indexPath
{
    History *history = [self.histories objectAtIndex:indexPath.row];
    return history.rowId;
}

- (NSArray *)queryTagNames:(NSInteger)rowId code:(LanguageCode)code
{
    return [UserDatabaseHelper queryTagNamesFrom:@"history" byRowId:rowId withCode:code];
}

#pragma mark - HHPanningTableViewCellDelegate

- (BOOL)panningTableViewCell:(HHPanningTableViewCell *)cell shouldReceivePanningTouch:(UITouch*)touch
{
    self.activeCell = cell;
    return YES;
}

- (UIView *)drawerView:(CGRect)frame
{
    UIView *drawerView = [[UIView alloc] initWithFrame:frame];
    drawerView.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
    
    CGFloat width = frame.size.width/6;
    
    UIButton *button0 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, frame.size.height)];
    button0.tag = 0;
    [button0 setTitle:@"0" forState:UIControlStateNormal];
    [button0 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button0 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button1 = [[UIButton alloc] initWithFrame:CGRectMake(width, 0, width, frame.size.height)];
    button1.tag = 1;
    [button1 setTitle:@"1" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button1 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(width*2, 0, width, frame.size.height)];
    button2.tag = 2;
    [button2 setTitle:@"2" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button2 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button3 = [[UIButton alloc] initWithFrame:CGRectMake(width*3, 0, width, frame.size.height)];
    button3.tag = 3;
    [button3 setTitle:@"3" forState:UIControlStateNormal];
    [button3 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button3 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button4 = [[UIButton alloc] initWithFrame:CGRectMake(width*4, 0, width, frame.size.height)];
    button4.tag = 4;
    [button4 setTitle:@"4" forState:UIControlStateNormal];
    [button4 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button4 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button5 = [[UIButton alloc] initWithFrame:CGRectMake(width*5, 0, width, frame.size.height)];
    button5.tag = 5;
    [button5 setTitle:@"✕" forState:UIControlStateNormal];
    [button5.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [button5 setBackgroundColor:[UIColor redColor]];
    [button5 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button5 addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [drawerView addSubview:button0];
    [drawerView addSubview:button1];
    [drawerView addSubview:button2];
    [drawerView addSubview:button3];
    [drawerView addSubview:button4];
    [drawerView addSubview:button5];
    
    return drawerView;
}

- (void)deleteSelectedHistory
{
}

- (void)showConfirmDeleteHistory
{
    UIAlertController * alert = [UIAlertController
                    alertControllerWithTitle:[self activeKeywords]
                                     message:[self confirmDeleteMessage]
                              preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* deleteButton = [UIAlertAction
                        actionWithTitle:NSLocalizedString(@"Delete", nil)
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        [self deleteSelectedHistory];
    }];

    UIAlertAction* cancelButton = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                      style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {}];
    [alert addAction:deleteButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)buttonClick:(UIButton *)sender
{
    [self.activeCell setDrawerRevealed:NO animated:YES];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:self.activeCell];
    self.selectedIndexPath = indexPath;
    
    [self setActiveItem:indexPath];
    
    if (sender.tag < 5) {
        [self updateState:sender.tag atRowId:[self activeRowId]];
    } else {
        [self showConfirmDeleteHistory];
    }
    
    [self refresh];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self itemsCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"History Cell";
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[HistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.drawerView = [self drawerView:cell.frame];
    cell.delegate = self;
    cell.directionMask = HHPanningTableViewCellDirectionLeft + HHPanningTableViewCellDirectionRight;
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad ? 15 : 13];
    
    NSInteger state = [self stateAtIndexPath:indexPath];
    LanguageCode code = [self codeAtIndexPath:indexPath];
    NSString *keywords = [self keywordsAtIndexPath:indexPath];
    NSString *detail = [self detailAtIndexPath:indexPath];
    NSString *note = [self noteAtIndexPath:indexPath];
    NSInteger type = [self searchTypeAtIndexPath:indexPath];
    NSInteger noteState = [self noteStateAtIndexPath:indexPath];
    NSInteger rowId = [self rowIdAtIndexPath:indexPath];
    
    switch (state) {
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"state_01"];
            break;
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"state_02"];
            break;
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"state_03"];
            break;
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"state_04"];
            break;
        default:
            cell.imageView.image = nil;
            break;
    }
    
    NSString *title;
    if (self.showAllSwitch && self.showAllSwitch.isOn) {
        title = [NSString stringWithFormat:@"%@ - %@", [BookDatabaseHelper shortName:code], keywords];
    } else {
        title = keywords;
    }
    
    cell.textLabel.text = note.length ? [NSString stringWithFormat:@"%@ (%@)", title, note] : title;
    if (!code) {
        cell.detailTextLabel.text = detail;
    } else if ([UserDatabaseHelper isTipitaka:code]) {
        NSArray *tokens = [detail componentsSeparatedByString:@" "];
        NSString *detail = [NSString stringWithFormat:@"%@  %@", [[tokens subarrayWithRange:NSMakeRange(0, 2)] componentsJoinedByString:@" "], [[tokens subarrayWithRange:NSMakeRange(2, 3)] componentsJoinedByString:@"   "]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", detail];
    } else {
        if (type == SearchTypeBuddhawaj) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@)", detail, NSLocalizedString(@"Only Buddhawaj", nil)];
        } else {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", detail];
        }
    }
    
    cell.accessoryView = nil;
    if (noteState > 0) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        if (noteState == 1) {
            [button setTitle:@"✕" forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor redColor]];
        } else if (noteState == 2) {
            [button setTitle:@"✔︎" forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor colorWithRed:0.196 green:0.8 blue:0.196 alpha:1]];
        }
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.accessoryView = button;
    }
    
    NSArray *tagNames = [self queryTagNames:rowId code:code];
    if (tagNames.count > 0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ [%@]", cell.detailTextLabel.text, [tagNames componentsJoinedByString:@"|"]];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tabBarController setSelectedViewController:self.tabBarController.viewControllers[1]];
    History *history = [self.histories objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:LoadHistoryNotification object:history];
    return indexPath;
}


@end
