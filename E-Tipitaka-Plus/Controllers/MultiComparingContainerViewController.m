//
//  MultiComparingContainerViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "MultiComparingContainerViewController.h"
#import "PageInfo.h"
#import "MultiComparingViewController.h"
#import "NSString+Thai.h"

@interface MultiComparingContainerViewController ()<MultiComparingViewControllerDelegate, UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) NSMutableDictionary *controllers;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) BOOL isFirstTime;
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;

@end

@implementation MultiComparingContainerViewController

@synthesize keywords = _keywords;
@synthesize controllers = _controllers;
@synthesize firstPageInfo = _firstPageInfo;
@synthesize secondPageInfo = _secondPageInfo;

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSMutableDictionary *)controllers
{
    if (_controllers == nil) {
        _controllers = [[NSMutableDictionary alloc] init];
    }
    return _controllers;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.count = 0;
    self.isFirstTime = YES;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotated:) name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.isFirstTime) {
        [self displayComparingViewController:self.firstPageInfo];
        [self displayComparingViewController:self.secondPageInfo];
        self.isFirstTime = NO;
    }
}

- (void)rotated:(NSNotification *)notifcation
{
    [self calculateChildControllersFrame];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)calculateChildControllersFrame
{
    NSArray *sortedController = [[self.controllers allValues] sortedArrayUsingComparator:^NSComparisonResult(UINavigationController *obj1, UINavigationController *obj2) {
        MultiComparingViewController *controller1 = (MultiComparingViewController *)obj1.topViewController;
        MultiComparingViewController *controller2 = (MultiComparingViewController *)obj2.topViewController;
        return [[NSNumber numberWithUnsignedInteger:controller1.number] compare:[NSNumber numberWithUnsignedInteger:controller2.number]];
    }];
    CGFloat width = self.view.frame.size.width/sortedController.count;
    for (int i=0; i < sortedController.count; i++) {
        UIViewController *controller = sortedController[i];
        CGRect rect = CGRectMake(i*width, 0, width, self.view.frame.size.height);
        controller.view.frame = rect;
    }
}

- (void)displayComparingViewController:(PageInfo *)pageInfo
{
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"NavMultiComparingViewController"];
    MultiComparingViewController *controller = (MultiComparingViewController *)navController.topViewController;
    controller.delegate = self;
    controller.pageInfo = pageInfo;
    controller.number = self.count;
    controller.shortNoteSwitch.on = self.switchOn;
    
    [self.controllers setObject:navController forKey:[NSNumber numberWithUnsignedInteger:self.count]];
    [self addChildViewController:navController];
    [self calculateChildControllersFrame];
    navController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    
    navController.view.layer.borderWidth = 0.5;
    navController.view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.view addSubview:navController.view];
    [navController didMoveToParentViewController:self];
    self.count += 1;
}

#pragma mark - MultiComparingViewControllerDelegate

- (void)multiComparingViewControllerViewConvertToPdf:(MultiComparingViewController *)controller showInButton:(UIBarButtonItem *)button
{
    CGFloat headerHeight = 50;
    int height = 0;
    CGFloat width = 0;
    for (int i=0; i<self.count; i++) {
        UINavigationController *navController = [self.controllers objectForKey:[NSNumber numberWithInt:i]];
        MultiComparingViewController *controller = (MultiComparingViewController *)navController.topViewController;
        UIWebView *webView = controller.readingPageViewController.webView;
        NSString *heightStr = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
        height = heightStr.intValue > height ? heightStr.intValue : height;
        width += webView.frame.size.width;
    }
        
    NSMutableData *pdfData = [NSMutableData data];
    CGRect singlePageRect = CGRectMake(0, 0, width, height+headerHeight);
    UIGraphicsBeginPDFContextToData(pdfData, singlePageRect, nil);
    UIGraphicsBeginPDFPageWithInfo(singlePageRect, nil);
    
    for (int i=0; i<self.count; i++) {
        UINavigationController *navController = [self.controllers objectForKey:[NSNumber numberWithInt:i]];
        MultiComparingViewController *controller = (MultiComparingViewController *)navController.topViewController;
        UIWebView *webView = controller.readingPageViewController.webView;
        CGFloat screenHeight = webView.bounds.size.height;
        CGFloat screenWidth = webView.bounds.size.width;
        CGRect frame = [webView frame];
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth, headerHeight)];
        header.textAlignment = NSTextAlignmentCenter;
        header.text = [[NSString stringWithFormat:@"%@ %@ %@", navController.title, NSLocalizedString(@"Page", nil), [NSNumber numberWithInteger:controller.page]] stringByReplacingThaiNumeric];
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        [header.layer renderInContext:currentContext];
        CGContextTranslateCTM(currentContext, 0, header.bounds.size.height);
        int pages = ceil(height / screenHeight);
        for (int i = 0; i < pages; i++) {
            // Check to screenHeight if page draws more than the height of the UIWebView
            if ((i+1) * screenHeight  > height) {
                CGRect f = [webView frame];
                f.size.height -= (((i+1) * screenHeight) - height);
                [webView setFrame: f];
            }
            CGContextTranslateCTM(currentContext, 0, (i > 0 ? screenHeight : 0));
            [[[webView subviews] lastObject] setContentOffset:CGPointMake(0, screenHeight * i) animated:NO];
            [webView.layer renderInContext:currentContext];
        }
        CGContextTranslateCTM(currentContext, screenWidth, -(pages-1)*screenHeight-headerHeight);
        [webView setFrame:frame];
    }
    UIGraphicsEndPDFContext();
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString *filename = @"compare.pdf";
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:filename];
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    NSLog(@"%@", documentDirectoryFilename);
    NSURL *URL = [NSURL fileURLWithPath:documentDirectoryFilename];
    self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
    self.documentInteractionController.delegate = self;
    [self.documentInteractionController presentOpenInMenuFromBarButtonItem:button animated:YES];
}

- (void)multiComparingViewControllerViewDidLoad:(MultiComparingViewController *)controller
{
    if (controller.number == 0) {
        controller.readingPageViewController.keywords = self.keywords;
        controller.readingPageViewController.searchType = self.searchType;
    }
    
    [controller.readingPageViewController open:controller.pageInfo keywords:(controller.number == 0 ? self.keywords : nil)];
}

- (void)multiComparingViewControllerViewWillClose:(MultiComparingViewController *)controller
{
    [controller willMoveToParentViewController:nil];
    UINavigationController *navController = [self.controllers objectForKey:[NSNumber numberWithUnsignedInteger:controller.number]];
    [navController.view removeFromSuperview];
    [controller removeFromParentViewController];
    [self.controllers removeObjectForKey:[NSNumber numberWithUnsignedInteger:controller.number]];
    [self calculateChildControllersFrame];
}

- (void)multiComparingViewControllerView:(MultiComparingViewController *)controller withCompare:(PageInfo *)pageInfo
{
    [self displayComparingViewController:pageInfo];
}

@end
