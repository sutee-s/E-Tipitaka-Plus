//
//  TagSearchAllListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 10/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "TagSearchAllListViewController.h"
#import "UITableView+LongPressMove.h"
#import "UserDatabaseHelper.h"

@interface TagSearchAllListViewController ()<LongPressMoveDelegate>

@end

@implementation TagSearchAllListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.tag.name;
    self.tableView.longPressMoveDelegate = self;
    [self.tableView enableLongPressMove:1];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.tag.searchAllhistory exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [UserDatabaseHelper updateTagItems:@"search_all" items:self.tag.searchAllhistory ofTag:self.tag.name withCode:self.tag.code];
}

- (void)refresh
{
    self.histories = [[UserDatabaseHelper querySearchAllHistoriesInRowIds:self.tag.searchAllhistory] mutableCopy];
    [self.tableView reloadData];
}

- (void)deleteSelectedHistory
{
    SearchAllHistory *history = [self.histories objectAtIndex:self.selectedIndexPath.row];
    [self.histories removeObjectAtIndex:self.selectedIndexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tag.searchAllhistory removeObject:[NSNumber numberWithInteger:history.rowId]];
    [UserDatabaseHelper updateTagItems:@"search_all" items:self.tag.searchAllhistory ofTag:self.tag.name withCode:self.tag.code];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Search All History", nil);
}

- (NSString *)confirmDeleteMessage
{
    return NSLocalizedString(@"Do you want to remove the item from this tag?", nil);
}

@end
