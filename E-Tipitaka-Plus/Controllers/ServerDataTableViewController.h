//
//  ServerDataTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerDataTableViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UILabel *accountLabel;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *refreshButtonItem;

@end
