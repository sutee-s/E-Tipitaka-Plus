//
//  MultiComparingViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 3/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "MultiComparingViewController.h"
#import "SearchViewController.h"
#import "History.h"

@interface MultiComparingViewController ()<UIBarPositioningDelegate, UIActionSheetDelegate, SearchViewControllerDelegate>

@property (nonatomic, assign) BOOL isFirstTime;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, strong) History *history;

@end

@implementation MultiComparingViewController

@synthesize pageInfo = _pageInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isFirstTime = YES;
    [self.delegate multiComparingViewControllerViewDidLoad:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    
    if (self.isFirstTime && self.number > 0) {
        NSMutableArray *items = [self.toolbar.items mutableCopy];
        [items removeObjectAtIndex:items.count-2];
        [items removeObjectAtIndex:items.count-2];
        [items removeObjectAtIndex:items.count-2];
        [self.toolbar setItems:items];
        self.isFirstTime = NO;
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)changeCode:(NSNotification *)notification
{
}

- (void)backoff:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        PageInfo *pageInfo = [[PageInfo alloc] initWithCode:self.code volume:self.volume page:self.page-([self.dataModel startPageOfVolume:self.volume]-1)];
        pageInfo.keywords = self.readingPageViewController.pageViewController.keywords;
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabasAndVolumeAndPageNotification object:pageInfo];
    }];    
}

- (void)close:(id)sender
{
    [self.delegate multiComparingViewControllerViewWillClose:self];
}

- (void)setupLeftMenuButton
{
    if (self.number > 0) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(close:)];
    }
}

- (void)setupRightMenuButton
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(backoff:)];
}

- (void)compareWithPageInfo:(PageInfo *)pageInfo
{
    [self.delegate multiComparingViewControllerView:self withCompare:pageInfo];
    [SVProgressHUD dismiss];
}

- (IBAction)compactSearch:(id)sender
{
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewNavigationController"];
    SearchViewController *controller = (SearchViewController *)navController.topViewController;
    controller.code = self.code;
    controller.number = self.number;
    controller.keywords = self.keywords;
    controller.history = self.history;
    controller.comparingMode = YES;
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)convertToPdf:(id)sender
{
    [self.delegate multiComparingViewControllerViewConvertToPdf:self showInButton:self.pdfButton];
}

# pragma mark - SearchViewControllerDelegate

- (void)searchViewController:(SearchViewController *)controller didSearch:(NSString *)keywords saveHistory:(History *)history
{
    self.keywords = keywords;
    self.history = history;
}

# pragma mark - UIBarPositioningDelegate

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

@end
