//
//  TagsViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETViewController.h"

@interface TagsViewController : ETViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;

@end
