//
//  BookListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "BookListViewController.h"
#import "NSString+Thai.h"
#import "PageInfo.h"

@interface BookListViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation BookListViewController

@synthesize selectedIndexPath = _selectedIndexPath;
@synthesize tableView = _tableView;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSelectedVolume:) name:ChangeSelectedVolumeNotification object:nil];
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (void)changeCode:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        self.code = [notification.object integerValue];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        self.code = ((PageInfo *)notification.object).code;
        self.selectedIndexPath = [self indexPathFromVolume:((PageInfo *)notification.object).volume];
    }
}

- (void)changeSelectedVolume:(NSNotification *)notification
{
    self.selectedIndexPath = [self indexPathFromVolume:[notification.object integerValue]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:self.selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.dataModel isValidVolume:[self volumeFromIndexPath:indexPath]]) {
        self.selectedIndexPath = indexPath;
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeNotification object:[NSNumber numberWithInteger:[self volumeFromIndexPath:indexPath]]];
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
        return indexPath;
    }
    return nil;
}

- (NSIndexPath *)indexPathFromVolume:(NSInteger)volume
{
    if (!self.dataModel.isTipitaka) {
        return [NSIndexPath indexPathForRow:volume-1 inSection:0];
    }
    
    if (volume <= [self.dataModel totalVolumesInSection:1]) {
        return [NSIndexPath indexPathForRow:volume-1 inSection:0];
    } else if (volume <= [self.dataModel totalVolumesInSection:1] + [self.dataModel totalVolumesInSection:2]) {
        return [NSIndexPath indexPathForRow:volume-[self.dataModel totalVolumesInSection:1]-1 inSection:1];
    }
    return [NSIndexPath indexPathForRow:volume-[self.dataModel totalVolumesInSection:1]-[self.dataModel totalVolumesInSection:2]-1 inSection:2];
}

- (NSInteger)volumeFromIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = 1;
    if (indexPath.section == 1) {
        index += [self.dataModel totalVolumesInSection:1];
    } else if (indexPath.section == 2) {
        index += [self.dataModel totalVolumesInSection:1] + [self.dataModel totalVolumesInSection:2];
    }
    return index + indexPath.row;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataModel.totalSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataModel totalVolumesInSection:section+1];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Book Item Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSInteger volume = [self volumeFromIndexPath:indexPath];
    
    if (self.dataModel.code == LanguageCodeRomanScript) {
        cell.textLabel.text = [NSString stringWithFormat:@"%2d. %@", volume, [self.dataModel volumeTitle:volume]];
    } else if (self.dataModel.isTipitaka) {
        NSMutableArray *tokens = [[[self.dataModel volumeTitle:volume] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
        [tokens removeObjectAtIndex:0];
        cell.textLabel.text = [[NSString stringWithFormat:@"%2d. %@", volume, [tokens componentsJoinedByString:@" "]] stringByReplacingThaiNumeric];
    } else {
        cell.textLabel.text = [[NSString stringWithFormat:@"%d. %@", volume, [self.dataModel volumeTitle:volume]] stringByReplacingThaiNumeric];
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (!self.dataModel.isTipitaka) {
        return nil;
    }
    switch (section) {
        case 0:
            return NSLocalizedString(@"Section 1", nil);
        case 1:
            return NSLocalizedString(@"Section 2", nil);
        case 2:
            return NSLocalizedString(@"Section 3", nil);
    }
    
    return @"";
}

@end
