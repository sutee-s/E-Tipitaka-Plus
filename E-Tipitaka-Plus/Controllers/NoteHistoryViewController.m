//
//  NoteHistoryViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/9/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "NoteHistoryViewController.h"
#import "UserDatabaseHelper.h"
#import "History.h"

@interface NoteHistoryViewController ()

@end

@implementation NoteHistoryViewController

@synthesize segmentedControl = _segmentedControl;
@synthesize noteTextView = _noteTextView;

- (NSString *)note
{
    History *history = [UserDatabaseHelper getHistoryByRowId:self.rowId];
    return history.note;
}

- (NSInteger)noteState
{
    History *history = [UserDatabaseHelper getHistoryByRowId:self.rowId];;
    return history.noteState;
}

- (void)updateNote
{
    [UserDatabaseHelper updateHistoryNote:self.rowId note:self.noteTextView.text state:self.segmentedControl.selectedSegmentIndex];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.noteTextView.layer.borderWidth = 1;
    self.noteTextView.layer.borderColor = [[UIColor blackColor] CGColor];
        
    self.noteTextView.text = [self note];
    self.segmentedControl.selectedSegmentIndex = [self noteState];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.noteTextView becomeFirstResponder];
}

- (IBAction)save:(id)sender
{
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", nil)];
    [self updateNote];
}

@end
