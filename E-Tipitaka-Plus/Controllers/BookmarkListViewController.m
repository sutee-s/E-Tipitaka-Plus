//
//  BookmarkListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "BookmarkListViewController.h"
#import "NoteViewCell.h"
#import "UserDatabaseHelper.h"
#import "NSString+Thai.h"
#import "PageInfo.h"
#import "ActionSheetPicker.h"

@interface BookmarkListViewController ()<UISearchBarDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation BookmarkListViewController


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:RefreshBookmarkListNotification object:nil];
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    recognizer.minimumPressDuration = 1.5;
    recognizer.delegate = self;
    [self.tableView addGestureRecognizer:recognizer];
    
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.selectedIndexPath = indexPath;
      
      UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose action", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
      [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{}];
      }]];
      [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Add tag", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
          [self chooseTags];
        }];
      }]];
      [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)chooseTags
{
    NSArray *names = [UserDatabaseHelper queryTagNames];
    if (!names.count) {
      UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"No tag found", nil) message:NSLocalizedString(@"Please create a tag", nil) preferredStyle:UIAlertControllerStyleAlert];
      [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Close", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{}];
      }]];
      [self presentViewController:alert animated:YES completion:nil];
      return;
    }
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"Select a tag", nil)
                                            rows:names
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
      NSLog(@"Picker: %@, Index: %ld, value: %@",
            picker, (long)selectedIndex, selectedValue);
                                           Bookmark *bookmark = [self.bookmarks objectAtIndex:self.selectedIndexPath.row];
                                           [UserDatabaseHelper addTagItems:@"note" rowId:bookmark.rowId ofTag:selectedValue withCode:self.code];
                                           [self.tableView beginUpdates];
                                           [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                                           [self.tableView endUpdates];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self.view];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)changeCode:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        self.code = [notification.object integerValue];
        [self refresh];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        [self refresh];
        self.code = ((PageInfo *)notification.object).code;
    }
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Bookmark", nil), self.dataModel.shortTitle];    
}

- (void)refresh:(NSNotification *)notification
{
    [self refresh];
}

- (void)refresh
{
    self.bookmarks = [[UserDatabaseHelper queryBookmarks:self.searchBar.text withCode:self.code] mutableCopy];
    [self sort:nil];
}

- (IBAction)sort:(id)sender
{
    self.bookmarks = [[self.bookmarks sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Bookmark *bookmark1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        Bookmark *bookmark2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [bookmark1.created compare:bookmark2.created];
    }] mutableCopy];
    
    self.bookmarks = [[self.bookmarks sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Bookmark *bookmark1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        Bookmark *bookmark2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;

        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [bookmark1.plainNote compare:bookmark2.plainNote];
            case 1:
                return [[NSNumber numberWithInt:bookmark1.volume] compare:[NSNumber numberWithInt:bookmark2.volume]] == NSOrderedSame ? [[NSNumber numberWithInt:bookmark1.page] compare:[NSNumber numberWithInt:bookmark2.page]] : [[NSNumber numberWithInt:bookmark1.volume] compare:[NSNumber numberWithInt:bookmark2.volume]];
            case 2:
                return [bookmark1.created compare:bookmark2.created];
            case 3:
            default:
                return [[NSNumber numberWithBool:bookmark1.starred] compare:[NSNumber numberWithBool:bookmark2.starred]];
        }
    }] mutableCopy];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Bookmark *bookmark = [self.bookmarks objectAtIndex:indexPath.row];
        [UserDatabaseHelper deleteBookmarkByRowId:bookmark.rowId];
        [self refresh];
    }
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}

#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self chooseTags];
    }
}


@end
