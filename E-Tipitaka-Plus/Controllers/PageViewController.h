//
//  PageViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 30/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Highlight.h"

@class PageViewController;

@protocol PageViewControllerDelegate <NSObject>

- (void)pageViewController:(PageViewController *)controller didClickAtHighlight:(NSInteger)rowId;
- (void)pageViewController:(PageViewController *)controller didClickAtHighlightNote:(NSInteger)rowId;
- (BOOL)pageViewControllerShouldShowHighlightDetail:(PageViewController *)controller;

@end

@interface PageViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *htmlContent;
@property (nonatomic, strong) NSString *footer;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, assign) NSInteger fontSize;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, assign) NSInteger anchoredItem;
@property (nonatomic, assign) BOOL shouldScrollToKeywords;
@property (nonatomic, assign) SearchType searchType;

@property (nonatomic, weak) id<PageViewControllerDelegate>delegate;

- (void)reload;
- (void)update;
- (void)removeHighlight:(Highlight *)highlight;

@end
