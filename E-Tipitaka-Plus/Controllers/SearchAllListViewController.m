//
//  SearchAllListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "SearchAllListViewController.h"
#import <HHPanningTableViewCell/HHPanningTableViewCell.h>
#import "UIView+Snapshot.h"
#import "UITableView+LongPressMove.h"
#import "UserDatabaseHelper.h"
#import "SearchAllHistory.h"
#import "NoteSearchAllHistoryViewController.h"
#import "ActionSheetPicker.h"

@interface SearchAllListViewController ()<HHPanningTableViewCellDelegate, UIGestureRecognizerDelegate, LongPressMoveDelegate>

@property (nonatomic, readonly) BOOL canBeMoved;

@end

@implementation SearchAllListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];
    
    UILongPressGestureRecognizer *recognizer1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    recognizer1.minimumPressDuration = 1.5;
    recognizer1.delegate = self;
    [self.tableView addGestureRecognizer:recognizer1];
    
    self.navigationItem.title = NSLocalizedString(@"Search All History", nil);
    
    self.tableView.longPressMoveDelegate = self;
    [self.tableView enableLongPressMove:2];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)updateState:(NSInteger)state atRowId:(NSInteger)rowId
{
    [UserDatabaseHelper updateSearchAllHistoryAtColumn:@"state" setValue:[NSNumber numberWithInteger:state] byRowId:rowId];
}

- (void)refresh
{
    self.histories = [[UserDatabaseHelper querySearchAllHistories:self.searchBar.text] mutableCopy];
    [self sort:nil];
}

- (void)setActiveItem:(NSIndexPath *)indexPath
{
    self.activeSearchAllHistory = [self.histories objectAtIndex:indexPath.row];
}

- (NSString *)activeKeywords
{
    return self.activeSearchAllHistory.keywords;
}

- (NSInteger)activeRowId
{
    return self.activeSearchAllHistory.rowId;
}

- (NSString *)confirmDeleteMessage
{
    return NSLocalizedString(@"Do you want to delete this search history?", nil);
}

- (NSInteger)stateAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.state;
}

- (LanguageCode)codeAtIndexPath:(NSIndexPath *)indexPath
{
    return 0;
}

- (NSString *)keywordsAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.keywords;
}

- (NSString *)noteAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.note;
}

- (NSString *)detailAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.detail;
}

- (NSInteger)searchTypeAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.type;
}

- (NSInteger)noteStateAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.noteState;
}

- (NSInteger)rowIdAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    return history.rowId;
}

- (void)deleteSelectedHistory
{
    [UserDatabaseHelper deleteSearchAllHistoryByRowId:self.activeSearchAllHistory.rowId];
    [self refresh];
}

- (NSArray *)queryTagNames:(NSInteger)rowId code:(LanguageCode)code
{
    return [UserDatabaseHelper queryTagNamesFrom:@"search_all" byRowId:rowId withCode:0];
}

- (IBAction)sort:(id)sender
{
    self.histories = [[self.histories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        SearchAllHistory *history1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        SearchAllHistory *history2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [history1.created compare:history2.created];
    }] mutableCopy];
    
    self.histories = [[self.histories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        SearchAllHistory *history1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        SearchAllHistory *history2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [history1.keywords compare:history2.keywords];
            case 1:
                return [[NSNumber numberWithInteger:history1.state] compare:[NSNumber numberWithInteger:history2.state]];
            case 2:
                return [history1.created compare:history2.created];
            case 3:
                return [[NSNumber numberWithInteger:history1.priority] compare:[NSNumber numberWithInteger:history2.priority]];
            default:
                return [history1.created compare:history2.created];
        }
    }] mutableCopy];
    [self.tableView reloadData];
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.histories exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [self calculatePriority];
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return !self.canBeMoved;
}

- (BOOL)canBeMoved
{
    return self.sortingTypeSegmentedControl.selectedSegmentIndex == 3;
}

- (void)calculatePriority
{
    // calculate new priority
    if (self.sortingOrderSegmentedControl.selectedSegmentIndex == 0) {
        // desc
        NSUInteger start = self.histories.count;
        for (SearchAllHistory *h in self.histories) {
            if (h.priority != start) {
                h.priority = start;
                [UserDatabaseHelper updateSearchAllHistoryAtColumn:@"priority" setValue:[NSNumber numberWithUnsignedInteger:start] byRowId:h.rowId];
            }
            start -= 1;
        }
    } else {
        // asc
        NSInteger start = 0;
        for (SearchAllHistory *h in self.histories) {
            if (h.priority != start) {
                h.priority = start;
                [UserDatabaseHelper updateSearchAllHistoryAtColumn:@"priority" setValue:[NSNumber numberWithUnsignedInteger:start] byRowId:h.rowId];
            }
            start += 1;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Note"]) {
        NoteSearchAllHistoryViewController *controller = segue.destinationViewController;
        controller.rowId = self.selectedSearchAllHistory.rowId;
    }
}

- (void)chooseTags
{
    NSArray *names = [UserDatabaseHelper queryTagNames];
    if (!names.count) {
        UIAlertController *alert = [UIAlertController
                        alertControllerWithTitle:NSLocalizedString(@"No tag found", nil)
                                         message:NSLocalizedString(@"Please add a tag", nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction
                          actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                    style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {}]];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"Select a tag", nil)
                                            rows:names
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [UserDatabaseHelper addTagItems:@"search_all" rowId:self.selectedSearchAllHistory.rowId ofTag:selectedValue withCode:0];
                                           [self.tableView beginUpdates];
                                           [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                                           [self.tableView endUpdates];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {}
                                          origin:self.view];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.selectedSearchAllHistory = [self.histories objectAtIndex:indexPath.row];
        self.selectedIndexPath = indexPath;
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose action", nil) message:@"" preferredStyle:UIAlertControllerStyleActionSheet];

        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Add note", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self performSegueWithIdentifier:@"Show Note" sender:self.selectedIndexPath];
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Add tag", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self chooseTags];
        }]];

        UIPopoverPresentationController *popPresenter = [actionSheet
                                                      popoverPresentationController];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
        popPresenter.sourceView = cell;
        popPresenter.sourceRect = cell.bounds;
                
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tabBarController setSelectedViewController:self.tabBarController.viewControllers[3]];
    SearchAllHistory *history = [self.histories objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:LoadSearchAllHistoryNotification object:history];
    return indexPath;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}

#pragma mark - HHPanningTableViewCellDelegate
- (BOOL)panningTableViewCell:(HHPanningTableViewCell *)cell shouldReceivePanningTouch:(UITouch*)touch
{
    self.activeCell = cell;
    return YES;
}

@end
