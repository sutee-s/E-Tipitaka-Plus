//
//  NoteSearchAllHistoryViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "NoteSearchAllHistoryViewController.h"
#import "UserDatabaseHelper.h"
#import "SearchAllHistory.h"

@interface NoteSearchAllHistoryViewController ()

@end

@implementation NoteSearchAllHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSString *)note
{
    SearchAllHistory *history = [UserDatabaseHelper getSearchAllHistoryByRowId:self.rowId];
    return history.note;
}

- (NSInteger)noteState
{
    SearchAllHistory *history = [UserDatabaseHelper getSearchAllHistoryByRowId:self.rowId];
    return history.noteState;
}

- (void)updateNote
{
    [UserDatabaseHelper updateSearchAllHistoryNote:self.rowId note:self.noteTextView.text state:self.segmentedControl.selectedSegmentIndex];
}

@end
