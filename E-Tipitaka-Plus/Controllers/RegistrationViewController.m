//
//  RegistrationViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "RegistrationViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface RegistrationViewController ()<UIWebViewDelegate>

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    self.title = NSLocalizedString(@"Registration", nil);
    [self.spinner startAnimating];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/signup/", USER_DATA_HOST]]];
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [SVProgressHUD showErrorWithStatus:@"Connection Error"];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.spinner stopAnimating];
    self.spinner.hidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
