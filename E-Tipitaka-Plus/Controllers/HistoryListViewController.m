//
//  HistoryListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <HHPanningTableViewCell/HHPanningTableViewCell.h>
#import "HistoryListViewController.h"
#import "HistoryTableViewCell.h"
#import "UserDatabaseHelper.h"
#import "BookDatabaseHelper.h"
#import "PageInfo.h"
#import "NoteHistoryViewController.h"
#import "UIView+Snapshot.h"
#import "ActionSheetPicker.h"
#import "UITableView+LongPressMove.h"

@interface HistoryListViewController ()<HHPanningTableViewCellDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate, LongPressMoveDelegate>


@property (nonatomic, readonly) BOOL canBeMoved;

@end

@implementation HistoryListViewController

@synthesize searchBar = _searchBar;
@synthesize showAllSwitch = _showAllSwitch;
@synthesize sortingOrderSegmentedControl = _sortingOrderSegmentedControl;
@synthesize sortingTypeSegmentedControl = _sortingTypeSegmentedControl;
@synthesize canBeMoved = _canBeMoved;


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:RefreshHistoryListNotification object:nil];
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (void)changeCode:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        self.code = [notification.object integerValue];
        [self refresh];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        [self refresh];
        self.code = ((PageInfo *)notification.object).code;
    }
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"History", nil), self.dataModel.shortTitle];
}

- (BOOL)canBeMoved
{
    return self.sortingTypeSegmentedControl.selectedSegmentIndex == 3;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModePanningCenterView];
    
    UILongPressGestureRecognizer *recognizer1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    recognizer1.minimumPressDuration = 1.5;
    recognizer1.delegate = self;
    [self.tableView addGestureRecognizer:recognizer1];
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"History", nil), self.dataModel.shortTitle];
    
    self.tableView.longPressMoveDelegate = self;
    [self.tableView enableLongPressMove:2];
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return !self.canBeMoved;
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.histories exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [self calculatePriority];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.selectedHistory = [self.histories objectAtIndex:indexPath.row];
        self.selectedIndexPath = indexPath;
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Add note", nil), NSLocalizedString(@"Add tag", nil), nil];
        [actionSheet showInView:self.view];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSString *)confirmDeleteMessage
{
    return NSLocalizedString(@"Do you want to delete this search history?", nil);
}

- (void)refresh:(NSNotification *)notification
{
    [self refresh];
}

- (void)refresh
{
    if (self.showAllSwitch.isOn) {
        self.histories = [[UserDatabaseHelper queryHistories:self.searchBar.text] mutableCopy];
    } else {
        self.histories = [[UserDatabaseHelper queryHistories:self.searchBar.text withCode:self.code] mutableCopy];
    }
    [self sort:nil];
}


- (IBAction)clear:(id)sender
{
    UIAlertController * alert = [UIAlertController
                    alertControllerWithTitle:NSLocalizedString(@"Deletion confirmation", nil)
                                     message:NSLocalizedString(@"Do you want to remove all histories?", nil)
                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* deleteButton = [UIAlertAction
                        actionWithTitle:NSLocalizedString(@"Delete", nil)
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        [UserDatabaseHelper clearHistories];
        [self refresh];
    }];

    UIAlertAction* cancelButton = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                      style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {}];
    [alert addAction:deleteButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)sort:(id)sender
{
    self.histories = [[self.histories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        History *history1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        History *history2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [history1.created compare:history2.created];
    }] mutableCopy];
    
    self.histories = [[self.histories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        History *history1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        History *history2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [history1.keywords compare:history2.keywords];
            case 1:
                return [[NSNumber numberWithInteger:history1.state] compare:[NSNumber numberWithInteger:history2.state]];
            case 2:
                return [history1.created compare:history2.created];
            case 3:
                return [[NSNumber numberWithInteger:history1.priority] compare:[NSNumber numberWithInteger:history2.priority]];
            default:
                return [history1.created compare:history2.created];
        }
    }] mutableCopy];
    [self.tableView reloadData];
}

- (IBAction)toggleShowAll:(id)sender
{
    [self refresh];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Note"]) {
        NoteHistoryViewController *controller = segue.destinationViewController;
        controller.rowId = self.selectedHistory.rowId;
    }
}

- (void)chooseTags
{
    NSArray *names = [UserDatabaseHelper queryTagNames];
    if (!names.count) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No tag found", nil) message:NSLocalizedString(@"Please add a tag", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"Select a tag", nil)
                                            rows:names
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %d, value: %@",
                                                 picker, selectedIndex, selectedValue);
                                           [UserDatabaseHelper addTagItems:@"history" rowId:self.selectedHistory.rowId ofTag:selectedValue withCode:self.code];
                                           [self.tableView beginUpdates];
                                           [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                                           [self.tableView endUpdates];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self.view];
}

- (void)calculatePriority
{
    // calculate new priority
    if (self.sortingOrderSegmentedControl.selectedSegmentIndex == 0) {
        // desc
        NSUInteger start = self.histories.count;
        for (History *h in self.histories) {
            if (h.priority != start) {
                h.priority = start;
                [UserDatabaseHelper updateHistoryAtColumn:@"priority" setValue:[NSNumber numberWithUnsignedInteger:start] byRowId:h.rowId];
            }
            start -= 1;
        }
    } else {
        // asc
        NSInteger start = 0;
        for (History *h in self.histories) {
            if (h.priority != start) {
                h.priority = start;
                [UserDatabaseHelper updateHistoryAtColumn:@"priority" setValue:[NSNumber numberWithUnsignedInteger:start] byRowId:h.rowId];
            }
            start += 1;
        }
    }
}

- (void)deleteSelectedHistory
{
    [UserDatabaseHelper deleteHistoryByRowId:self.activeHistory.rowId];
    [self refresh];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}

#pragma mark - HHPanningTableViewCellDelegate
- (BOOL)panningTableViewCell:(HHPanningTableViewCell *)cell shouldReceivePanningTouch:(UITouch*)touch
{
    self.activeCell = cell;
    return YES;
}

#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self performSegueWithIdentifier:@"Show Note" sender:self.selectedIndexPath];
    } else if (buttonIndex == 1) {
        [self chooseTags];
    }
}

@end
