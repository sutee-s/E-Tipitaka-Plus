//
//  ServerConnectionTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Unirest/UNIRest.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "ServerConnectionTableViewController.h"
#import "AccountHelper.h"
#import "SyncDatabaseManager.h"

@interface ServerConnectionTableViewController ()

@end

@implementation ServerConnectionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Connect to server", nil);
    if ([AccountHelper currentUsername]) {
        self.statusLabel.text = [NSString stringWithFormat:@"Logged in : %@", [AccountHelper currentUsername]];
    } else {
        self.statusLabel.text = @"No account";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        [self login];
        return nil;
    } else if (indexPath.row == 2) {
        [self logout];
        return nil;
    }
    return indexPath;
}

- (void)logout
{
    [AccountHelper deleteCurrentAccount];
    self.statusLabel.text = @"No account";
    [SVProgressHUD showSuccessWithStatus:@"Logout successful"];
}

- (void)remoteLogin:(NSString *)username password:(NSString *)password
{
    NSDictionary *headers = @{@"accept": @"application/json"};
    NSDictionary *parameters = @{@"username": username, @"password": password};
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showProgress:0 status:@"Logging in.."];
    
    [[UNIRest post:^(UNISimpleRequest *request) {
        [request setUrl:[NSString stringWithFormat:@"%@/rest-auth/login/", USER_DATA_HOST]];
        [request setHeaders:headers];
        [request setParameters:parameters];
    }] asJsonAsync:^(UNIHTTPJsonResponse* response, NSError *error) {
        NSInteger code = response.code;
        UNIJsonNode *body = response.body;
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            if (code == 200) {
                [SVProgressHUD showSuccessWithStatus:@"Login successful"];
                [AccountHelper updateCurrentAccount:username withToken:body.JSONObject[@"key"]];
                self.statusLabel.text = [NSString stringWithFormat:@"Logged in : %@", username];
                if (AUTO_SYNC) {
                    [[SyncDatabaseManager sharedInstance] syncWithProgressHUD:nil];
                } else {
                    [[SyncDatabaseManager sharedInstance] syncManuallyWithProgressHUD:nil inViewController:self];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:body.JSONObject[@"non_field_errors"][0]];
            }
        });
    }];
}

- (void)login
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Login", nil)
                                          message:NSLocalizedString(@"Enter username and password", nil)
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Username", @"Login");
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Password", @"Password");
         textField.secureTextEntry = YES;
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];

    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField *login = alertController.textFields.firstObject;
                                   UITextField *password = alertController.textFields.lastObject;
                                   [self remoteLogin:login.text password:password.text];
                               }];
    okAction.enabled = NO;
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *login = alertController.textFields.firstObject;
        UITextField *password = alertController.textFields[1];
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = login.text.length > 0 && password.text.length > 0;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
