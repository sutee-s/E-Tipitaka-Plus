//
//  HighlightColorTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "HighlightColorTableViewController.h"
#import "HighlightColorEditTableViewController.h"
#import "HighlightColorList.h"
#import "UserDatabaseHelper.h"

@interface HighlightColorTableViewController ()<HighlightColorListDelegate, UIActionSheetDelegate, HighlightColorEditTableViewControllerDelegate>

@property (nonatomic, strong) HighlightColorList *items;
@property (nonatomic, strong) HighlightColor *selectedColor;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation HighlightColorTableViewController

@synthesize items = _items;

- (HighlightColorList *)items
{
    if (!_items) {
        _items = [[HighlightColorList alloc] init];
    }
    return _items;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Highlight color configuration", nil);
    self.items.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)add:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Add color template", nil) message:NSLocalizedString(@"Enter template name", nil) preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"Template", nil), [self.items size] + 1];
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = alert.textFields[0];
        NSLog(@"%@", textField.text);
        HighlightColor *color = [HighlightColor new];
        color.name = textField.text;
        color.inlineColors = [@[DEFAULT_HIGHLIGHT_COLOR_1,DEFAULT_HIGHLIGHT_COLOR_2,DEFAULT_HIGHLIGHT_COLOR_3,DEFAULT_HIGHLIGHT_COLOR_4,DEFAULT_HIGHLIGHT_COLOR_5] mutableCopy];
        color.footerColors = [@[DEFAULT_HIGHLIGHT_COLOR_6,DEFAULT_HIGHLIGHT_COLOR_7,DEFAULT_HIGHLIGHT_COLOR_8,DEFAULT_HIGHLIGHT_COLOR_9,DEFAULT_HIGHLIGHT_COLOR_10] mutableCopy];
        color.noteColors = [@[DEFAULT_HIGHLIGHT_COLOR_11,DEFAULT_HIGHLIGHT_COLOR_12,DEFAULT_HIGHLIGHT_COLOR_13,DEFAULT_HIGHLIGHT_COLOR_14,DEFAULT_HIGHLIGHT_COLOR_15] mutableCopy];
        [self.items addHighlightColor:color];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    [self presentViewController:alert animated:YES completion:^{
    }];
}

- (void)apply
{
    [HighlightColorList setCurrentHighlightColor:self.selectedColor.rowId];
    [self reload];
}

- (void)rename
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Add color template", nil) message:NSLocalizedString(@"Enter template name", nil) preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = self.selectedColor.name;
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = alert.textFields[0];
        if (![self.selectedColor.name isEqualToString:textField.text]) {
            self.selectedColor.name = textField.text;
            [self.items updateHighlightColor:self.selectedColor];
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.15 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:^{
        }];
    });
}

- (void)delete
{
    if (self.selectedColor.rowId == 1) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"You cannot delete standard template.", nil) preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.15 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:^{
            }];
        });
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:self.selectedColor.name message:NSLocalizedString(@"Do you want to delete this template?", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if ([HighlightColorList getCurrentHighlightColor].rowId == self.selectedColor.rowId) {                
                [HighlightColorList setCurrentHighlightColor:1];
            }
            [self.items removeHighlightColor:self.selectedColor];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.15 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:^{
            }];
        });
    }
}

- (void)reload
{
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:UpdateCurrentPageNotification object:nil];
}

#pragma mark - HighlightColorEditTableViewControllerDelegate

- (void)HighlightColorEditTableViewController:(HighlightColorEditTableViewController *)controler didChangeColor:(HighlightColor *)color
{
    [self.items updateHighlightColor:color];
}

#pragma mark - HighlightColorListDelegate

- (void)highlightColorListDidChange:(HighlightColorList *)colorList
{
    [self reload];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items size];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HighlightColorCell" forIndexPath:indexPath];
    
    HighlightColor *color = [self.items highlightColorAtIndex:indexPath.row];
    cell.textLabel.text = color.name;

    cell.accessoryType = UITableViewCellAccessoryNone;
    
    NSInteger selectedId = [HighlightColorList getCurrentHighlightColor] ? [HighlightColorList getCurrentHighlightColor].rowId : -1;
    [HighlightColorList getCurrentHighlightColor];
    if (color.rowId == selectedId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Delete", nil) otherButtonTitles:NSLocalizedString(@"Apply", nil), NSLocalizedString(@"Edit", nil), NSLocalizedString(@"Rename", nil), nil];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [actionSheet showInView:self.view];
    });
    
    self.selectedIndexPath = indexPath;
    self.selectedColor = [self.items highlightColorAtIndex:indexPath.row];

    return nil;
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self apply];
    } else if (buttonIndex == 2) {
        [self performSegueWithIdentifier:@"Edit" sender:self];
    } else if (buttonIndex == 3) {
        [self rename];
    } else if (buttonIndex == 0) {
        [self delete];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Edit"]) {
        HighlightColorEditTableViewController *controller = segue.destinationViewController;
        controller.color = self.selectedColor;
        controller.delegate = self;
    }
}


@end
