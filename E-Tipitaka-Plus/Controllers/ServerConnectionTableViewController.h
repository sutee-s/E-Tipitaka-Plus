//
//  ServerConnectionTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerConnectionTableViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UILabel *statusLabel;

@end
