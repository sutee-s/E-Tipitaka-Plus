//
//  ReadingStateDetailTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 12/23/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ReadingStateDetailTableViewController.h"
#import "UserDatabaseHelper.h"
#import "ReadingState.h" 
#import "NSString+Thai.h"


@interface ReadingStateDetailTableViewController ()

@property (nonatomic, strong) NSArray *items;

@end

@implementation ReadingStateDetailTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    self.navigationItem.title = self.customTitle;
    if (self.state == ReadingStateTypeUnread) {
        NSMutableArray *items = [NSMutableArray new];
        for (int i=0; i<self.totalPages; ++i) {
            NSInteger page = i+1;
            [items addObject:[NSNumber numberWithInteger:page]];
        }
        for (ReadingState *readingState in [UserDatabaseHelper queryReadingStates:ReadingStateTypeRead code:self.code volume:self.volume]) {
            [items removeObject:[NSNumber numberWithInteger:readingState.page]];
        }
        for (ReadingState *readingState in [UserDatabaseHelper queryReadingStates:ReadingStateTypeRead code:self.code volume:self.volume]) {
            [items removeObject:[NSNumber numberWithInteger:readingState.page]];
        }
        self.items = items;
    } else {
        self.items = [UserDatabaseHelper queryReadingStates:self.state code:self.code volume:self.volume];
    }
    
}

- (NSNumber *)findPageAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *page = nil;
    if (self.state == ReadingStateTypeUnread) {
        page = self.items[indexPath.row];
    } else {
        ReadingState *readingState = self.items[indexPath.row];
        page = [NSNumber numberWithInteger:readingState.page];
    }
    return page;
}

- (NSString *)countStringAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *countString = @"";
    if (self.state != ReadingStateTypeUnread) {
        ReadingState *readingState = self.items[indexPath.row];
        countString = [NSString stringWithFormat:@"(%@ %@)", [NSNumber numberWithInteger:(readingState.count ? readingState.count : 1)], NSLocalizedString(@"time", nil)];
    }
    return countString;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeAndPageNotification object:@{@"volume":[NSNumber numberWithInteger:self.volume], @"page":[self findPageAtIndexPath:indexPath]}];
    return indexPath;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"ReadingStateDetailCell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
 
    NSNumber *page = [self findPageAtIndexPath:indexPath];
    cell.textLabel.text = [[NSString stringWithFormat:@"%@ %@ %@ %@ %@",
                           NSLocalizedString(@"Volume", nil),
                           [NSNumber numberWithInteger:self.volume],
                           NSLocalizedString(@"Page", nil),
                           page, [self countStringAtIndexPath:indexPath]
                           ] stringByReplacingThaiNumeric];
    
    if ([[UserDatabaseHelper queryHighlightsInVolume:self.volume page:page.integerValue code:self.code] count]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}



@end
