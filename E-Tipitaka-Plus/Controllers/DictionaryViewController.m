//
//  DictionaryViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "DictionaryViewController.h"
#import "DictDatabaseHelper.h"
#import "Lexicon.h"
#import "UserDatabaseHelper.h"
#import "DictionaryDetailViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <CoreText/CoreText.h>
#import "UIView+FindSubView.h"

#define HEAD_SIZE           (self.dictionaryType != DictionaryTypeEnglish ? 26 : 18)
#define HEAD_FONT           (self.dictionaryType != DictionaryTypeEnglish ? @"THSarabunNew-Bold" : @"Helvetica Bold")

#define TRANSLATION_SIZE    (self.dictionaryType != DictionaryTypeEnglish ? 22 : 16)
#define TRANSLATION_FONT    (self.dictionaryType != DictionaryTypeEnglish ? @"THSarabunNew" : @"Helvetica")

#define CELL_MIN_HEIGHT     65
#define CELL_PADDING        10


@interface DictionaryViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate>
{
  BOOL _bookmarkLoaded;
}

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSIndexPath *selectedIndex;
@property (nonatomic, strong) Lexicon* selectedLexicon;

@end

@implementation DictionaryViewController
{
  CGFloat _normalHeight;
  CGFloat _maxHeight;
  dispatch_block_t _reloadTableBlock;
}

@synthesize items = _items;
@synthesize selectedIndex = _selectedIndex;
@synthesize searchText = _searchText;
@synthesize romanToolbar1 = _romanToolbar1;
@synthesize romanToolbar2 = _romanToolbar2;
@synthesize selectedLexicon = _selectedLexicon;

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  _maxHeight = self.tableView.frame.size.height - self.searchBar.frame.size.height - self.romanToolbar1.frame.size.height - self.romanToolbar2.frame.size.height;
  _normalHeight = CELL_MIN_HEIGHT + 2*CELL_PADDING;
  
  UITextField *textField = (UITextField*)[self.searchBar findSubview:@"UITextField" resursion:YES];
  
  [textField setTextColor:[UIColor redColor]];
  [textField setFont:[UIFont fontWithName:TRANSLATION_FONT size:TRANSLATION_SIZE]];
  
  if (self.dictionaryType == DictionaryTypePali) {
    self.searchBar.placeholder = NSLocalizedString(@"Enter Pali", nil);
    self.title = NSLocalizedString(@"Pali-Thai dictionary", nil);
    self.infoBarButton.enabled = YES;
  } else if (self.dictionaryType == DictionaryTypeThai) {
    self.searchBar.placeholder = NSLocalizedString(@"Enter Thai", nil);
    self.title = NSLocalizedString(@"Thai dictionary", nil);
    self.infoBarButton.enabled = NO;
    self.infoBarButton.tintColor = [UIColor clearColor];
  } else if (self.dictionaryType == DictionaryTypeEnglish) {
    self.searchBar.placeholder = NSLocalizedString(@"Enter Pali Roman Script", nil);
    self.title = NSLocalizedString(@"Pali-English dictionary", nil);
    self.infoBarButton.enabled = NO;
    self.infoBarButton.tintColor = [UIColor clearColor];
  }
  
  self.searchBar.showsBookmarkButton = YES;
  
  if (self.searchText.length) {
    self.searchBar.text = self.searchText;
    [self lookupDictionary:self.searchText];
  } else {
    [self.searchBar becomeFirstResponder];
  }
  
  UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
  recognizer.minimumPressDuration = 1.0;
  recognizer.delegate = self;
  [self.tableView addGestureRecognizer:recognizer];
  
  [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
  CGPoint p = [gestureRecognizer locationInView:self.tableView];
  
  NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
  if (indexPath == nil) {
    NSLog(@"long press on table view but not on a row");
  } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
    self.selectedLexicon = [self.items objectAtIndex:indexPath.row];
    NSArray *items = [UserDatabaseHelper querySavedLexicon:self.selectedLexicon.type];
    if ([items containsObject:self.selectedLexicon]) {
      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:self.selectedLexicon.head message:NSLocalizedString(@"Do you want to delete this lexicon?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
      [alertView show];
    } else {
      [self performSegueWithIdentifier:@"Show detail" sender:indexPath];
    }
  } else {
    NSLog(@"gestureRecognizer.state = %d", gestureRecognizer.state);
  }
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width-100, self.view.frame.size.height*0.75);
  } else {
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.height-100, self.view.frame.size.width*0.85);
  }
  
  if (self.dictionaryType != DictionaryTypeEnglish) {
    self.romanToolbar1.hidden = YES;
    self.romanToolbar2.hidden = YES;
    NSInteger toolBarSize = UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad ? 44 : 88;
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y-toolBarSize, self.tableView.frame.size.width, self.tableView.frame.size.height+toolBarSize);
  }
  
  if (self.selectedIndex) {
    [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndex] withRowAnimation:UITableViewRowAnimationNone];
  }
  
}

- (NSString *)prepareSearchText:(NSString *)searchText
{
  if (self.dictionaryType != DictionaryTypePali) {
    return searchText;
  }
  searchText = [searchText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%C", (unichar)3597] withString:[NSString stringWithFormat:@"%C", (unichar)63247]];
  searchText = [searchText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%C", (unichar)3661] withString:[NSString stringWithFormat:@"%C", (unichar)63249]];
  searchText = [searchText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%C", (unichar)3600] withString:[NSString stringWithFormat:@"%C", (unichar)63232]];
  return searchText;
}


- (void)lookupDictionary:(NSString *)searchText
{
  _bookmarkLoaded = NO;
  searchText = [self prepareSearchText:searchText];
  
  NSString *newStringText = @"";
  for (NSUInteger i = 0; i < [searchText length]; i++) {
    unichar c = [searchText characterAtIndex:i];
    newStringText = [newStringText stringByAppendingFormat:@"%C", (unichar)(c == 3597 ? 63247 : c)];
  }
  
  __block NSString *safeSearchText = newStringText;
  
  if (_reloadTableBlock != nil) {
    dispatch_block_cancel(_reloadTableBlock);
  }
  self.items = @[];
  
  if (safeSearchText.length != 0) {
    _reloadTableBlock = dispatch_block_create(0, ^{
      while (self.items.count == 0 && safeSearchText.length > 0) {
        if (self.dictionaryType == DictionaryTypePali) {
          self.items = [DictDatabaseHelper queryDictionary:safeSearchText];
        } else if (self.dictionaryType == DictionaryTypeThai) {
          self.items = [DictDatabaseHelper queryThaiDictionary:safeSearchText];
        } else if (self.dictionaryType == DictionaryTypeEnglish) {
          self.items = [DictDatabaseHelper queryEnglishDictionary:safeSearchText];
        }
        safeSearchText = [safeSearchText substringToIndex:safeSearchText.length-1];
      }
      [self.tableView reloadData];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), _reloadTableBlock);
  } else {
    [self.tableView reloadData];
  }
}


#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
-(CGFloat)translationHeightForIndex:(NSInteger)index
{
  CGSize maximumSize = CGSizeMake(self.view.frame.size.width-24, CGFLOAT_MAX);
  
  NSString *text = NSLocalizedString(@"Not found this lexicon in the dictionary", nil);
  if (index < self.items.count) {
    Lexicon *lexicon = [self.items objectAtIndex:index];
    text = lexicon.translation;
  }
  
  CGSize translationSize;
  if ([text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
    translationSize = [text boundingRectWithSize:maximumSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:TRANSLATION_FONT size:TRANSLATION_SIZE]} context:nil].size;
  } else {
    translationSize = [text sizeWithFont:[UIFont fontWithName:TRANSLATION_FONT size:TRANSLATION_SIZE] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
  }
  
  return translationSize.height * 1.15;
  
}
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
-(CGFloat)headHeightForIndex:(NSInteger)index
{
  CGSize maximumSize = CGSizeMake(self.view.frame.size.width-24, 10000);
  NSString *text = self.searchBar.text;
  if (index < self.items.count) {
    Lexicon *lexicon = [self.items objectAtIndex:index];
    text = lexicon.head;
  }
  
  CGSize headSize;
  if ([text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
    headSize = [text boundingRectWithSize:maximumSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:HEAD_FONT size:HEAD_SIZE]} context:nil].size;
  } else {
    headSize = [text sizeWithFont:[UIFont fontWithName:HEAD_FONT size:HEAD_SIZE] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
  }
  return headSize.height;
}

#pragma GCC diagnostic warning "-Wdeprecated-declarations"

-(CGFloat)cellHeightForIndexPath:(NSIndexPath *)indexPath
{
  return [self headHeightForIndex:indexPath.row] + [self translationHeightForIndex:indexPath.row];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.searchBar.text.length || _bookmarkLoaded ? (self.items.count ? self.items.count : 1) : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Dict Cell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  if (!cell) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
  }

  cell.textLabel.backgroundColor = [UIColor clearColor];
  cell.detailTextLabel.backgroundColor = [UIColor clearColor];
  
  Lexicon *lexicon = self.items.count ? [self.items objectAtIndex:indexPath.row] : [[Lexicon alloc] initWithHead:self.searchBar.text andTranslation:NSLocalizedString(@"Not found this lexicon in the dictionary", nil)];
  cell.textLabel.font = [UIFont fontWithName:HEAD_FONT size:HEAD_SIZE];
  cell.textLabel.text = lexicon.head;
  
  cell.detailTextLabel.font = [UIFont fontWithName:TRANSLATION_FONT size:TRANSLATION_SIZE];
  cell.detailTextLabel.attributedText = lexicon.attributedTranslation;
  
  if ([self.selectedIndex isEqual:indexPath]) {
    CGFloat expandedHeight = [self cellHeightForIndexPath:indexPath] + 2*CELL_PADDING;
    cell.detailTextLabel.lineBreakMode = expandedHeight < _maxHeight ? NSLineBreakByWordWrapping : NSLineBreakByTruncatingTail;
    cell.detailTextLabel.numberOfLines = expandedHeight < _maxHeight ? 0 : 1;
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.7 alpha:0.15];
  } else {
    cell.detailTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.detailTextLabel.numberOfLines = 1;
    cell.contentView.backgroundColor = [UIColor whiteColor];
  }
  
  
  cell.accessoryType = ![self.selectedIndex isEqual:indexPath] ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
  
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  CGFloat expandedHeight = [self cellHeightForIndexPath:indexPath] + 2*CELL_PADDING;
  if ([self.selectedIndex isEqual:indexPath] && expandedHeight > _normalHeight && expandedHeight < _maxHeight) {
    return expandedHeight;
  }
  return _normalHeight;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if ([self.selectedIndex isEqual:indexPath]) {
    self.selectedIndex = nil;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    return;
  }
  
  if (self.selectedIndex) {
    NSIndexPath *previousIndex = self.selectedIndex;
    self.selectedIndex = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    [tableView reloadRowsAtIndexPaths:@[previousIndex] withRowAnimation:UITableViewRowAnimationFade];
  }
  
  self.selectedIndex = indexPath;
  [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
  
  CGFloat expandedHeight = [self cellHeightForIndexPath:indexPath] + 2*CELL_PADDING;
  if (expandedHeight > _maxHeight) {
    [self performSegueWithIdentifier:@"Show detail" sender:indexPath];
  }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [self.searchBar resignFirstResponder];
  
  if (!self.items.count) {
    return nil;
  }
  
  Lexicon *lexicon = [self.items objectAtIndex:indexPath.row];
  
  self.searchBar.text = lexicon.head;
  
  return indexPath;
}

- (IBAction)info:(id)sender
{
  [self performSegueWithIdentifier:@"Show info" sender:sender];
}

- (IBAction)save:(id)sender
{
  NSString *text = [self prepareSearchText:self.searchBar.text];
  NSArray *items;
  if (self.dictionaryType == DictionaryTypePali) {
    items = [DictDatabaseHelper queryDictionary:text];
  } else if (self.dictionaryType == DictionaryTypeThai) {
    items = [DictDatabaseHelper queryThaiDictionary:text];
  } else if (self.dictionaryType == DictionaryTypeEnglish) {
    items = [DictDatabaseHelper queryEnglishDictionary:text];
  }
  if (items.count) {
    [UserDatabaseHelper saveLexicon:items[0] type:self.dictionaryType];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", nil)];
  }
}

- (IBAction)done:(id)sender
{
  [self.searchBar resignFirstResponder];
  [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)roman:(id)sender
{
  NSInteger tag = [sender tag];
  self.searchBar.text = [NSString stringWithFormat:@"%@%@", self.searchBar.text, ROMAN_KEYS[tag]];
  [self lookupDictionary:self.searchBar.text];
}

- (void)loadLexicon
{
  self.selectedIndex = nil;
  self.items = [UserDatabaseHelper querySavedLexicon:self.dictionaryType];
  if (self.items.count) {
    _bookmarkLoaded = YES;
    [self.tableView reloadData];
  } else {
    _bookmarkLoaded = NO;
    self.searchBar.text = nil;
    [self.tableView reloadData];
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"Show detail"]) {
    NSIndexPath *indexPath = sender;
    DictionaryDetailViewController *controller = segue.destinationViewController;
    controller.lexicon = [self.items objectAtIndex:indexPath.row];
    controller.dictionaryType = self.dictionaryType;
  }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
  [self.searchBar resignFirstResponder];
}

#pragma mark - UISearcBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
  [self lookupDictionary:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
  [self lookupDictionary:searchBar.text];
  [searchBar resignFirstResponder];
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
  [self loadLexicon];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if (buttonIndex == 1) {
    [UserDatabaseHelper deleteLexicon:self.selectedLexicon type:self.selectedLexicon.type];
    [self loadLexicon];
  }
}

@end
