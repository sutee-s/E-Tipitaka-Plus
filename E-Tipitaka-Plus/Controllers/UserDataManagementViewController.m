//
//  UserDataManagementViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 10/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "UserDataManagementViewController.h"
#import "UserDatabaseHelper.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <SSZipArchive/SSZipArchive.h>
#import <MessageUI/MessageUI.h>
#import <OCTotallyLazy/OCTotallyLazy.h>
#import <AFNetworking/AFNetworking.h>
#import "AccountHelper.h"

#define kForeignItemActionSheet     1
#define kNativeItemActionSheet      2

@interface UserDataManagementViewController ()<UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) NSArray *files;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation UserDataManagementViewController

@synthesize files = _files;
@synthesize selectedIndexPath = _selectedIndexPath;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = NSLocalizedString(@"Local data", nil);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)create:(id)sender
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:EXPORT_PATH]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:EXPORT_PATH withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Creating", nil)];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd-HH_mm_ss"];
        [UserDatabaseHelper exportUserDatabase:[NSURL fileURLWithPath:[EXPORT_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.json", [dateFormatter stringFromDate:[NSDate date]]]]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self.tableView reloadData];
        });
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{    
    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
        NSProgress *progress = (NSProgress *)object;
        NSLog(@"Progress is %f", progress.fractionCompleted);
        [SVProgressHUD showProgress:progress.fractionCompleted];
    }
}

- (void)upload:(NSString *)filename
{
    if (![AccountHelper currentToken]) {
        return;
    }
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                              URLString:[NSString stringWithFormat:@"%@/upload/", USER_DATA_HOST]
                                                                                             parameters:@{@"title": filename}
                                                                              constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSURL *filePath = [NSURL fileURLWithPath:[EXPORT_PATH stringByAppendingPathComponent:filename]];
        NSLog(@"%@", filePath);
        [formData appendPartWithFileURL:filePath name:@"file" fileName:filename mimeType:@"text/json" error:nil];
    } error:nil];
    
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    NSProgress *progress;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:&progress
                  completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                      [SVProgressHUD dismiss];
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Upload data error", nil)];
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                          [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Upload data successful", nil)];
                      }
                  }];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showProgress:0.0f];

    [progress addObserver:self
               forKeyPath:@"fractionCompleted"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
    [uploadTask resume];
}

- (void)export:(NSString *)filename
{
    NSString *srcPath = [EXPORT_PATH stringByAppendingPathComponent:filename];
    NSString *zipPath = [TEMP_PATH stringByAppendingPathComponent:filename];

    if ([[NSFileManager defaultManager] fileExistsAtPath:zipPath]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:zipPath error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    }
    
    BOOL ret = [SSZipArchive createZipFileAtPath:[zipPath stringByAppendingPathExtension:@"etz"] withFilesAtPaths:@[srcPath]];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:@"E-Tipitaka Document"];
    NSString *attachFile = ret ? zipPath : srcPath;
    [mc addAttachmentData:[NSData dataWithContentsOfFile:attachFile] mimeType:@"application/E-Tipitaka" fileName:[attachFile lastPathComponent]];
    [mc setMessageBody:[attachFile lastPathComponent] isHTML:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if (mc) {
            [self presentViewController:mc animated:YES completion:nil];
        }        
    });
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MFMailComposeResultSent) {
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Sent", nil)];
    } else if (result == MFMailComposeResultFailed) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Fail", nil)];
    } else if (result == MFMailComposeResultCancelled) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Cancelled", nil)];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"", nil)];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];        
        [UserDatabaseHelper importUserDatabase:[NSURL fileURLWithPath:[EXPORT_PATH stringByAppendingPathComponent:self.files[self.selectedIndexPath.row]]]];
    } else if (buttonIndex == 1 && actionSheet.tag == kNativeItemActionSheet) {
        [self export:self.files[self.selectedIndexPath.row]];
    } else if (buttonIndex == 2 && actionSheet.tag == kNativeItemActionSheet) {
        [self upload:self.files[self.selectedIndexPath.row]];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    NSString *filename = self.files[self.selectedIndexPath.row];
    if ([filename hasSuffix:@".json"]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:self.files[indexPath.row]
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Import data", nil), NSLocalizedString(@"Export data via email", nil), NSLocalizedString(@"Upload to server", nil), nil];
        actionSheet.tag = kNativeItemActionSheet;
        [actionSheet showInView:self.view];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:self.files[indexPath.row]
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Import data", nil), nil];
        actionSheet.tag = kForeignItemActionSheet;
        [actionSheet showInView:self.view];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.files = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:EXPORT_PATH error:nil] filter:^BOOL(NSString *filename) {
        return [filename.pathExtension isEqualToString:@"json"] || [filename.pathExtension isEqualToString:@"etz"] || [filename.pathExtension isEqualToString:@"js"];
    }];
    self.files = [self.files sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        return [obj2 compare:obj1];
    }];
    return self.files.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"File Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self.files[indexPath.row] lastPathComponent];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [SVProgressHUD showWithStatus:@"Deleting"];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:[EXPORT_PATH stringByAppendingPathComponent:self.files[indexPath.row]] error:&error];
            if (error) {
                NSLog(@"%@", error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            });
        });
    }
}

@end
