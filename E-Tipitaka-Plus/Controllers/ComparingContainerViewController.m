//
//  ComparingContainerViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 28/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ComparingContainerViewController.h"
#import "ReadingPageViewController.h"
#import "ReadingContainerViewController.h"
#import "ComparingViewController.h"
#import "BookDatabaseHelper.h"
#import "SharedDataModel.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface ComparingContainerViewController ()<ComparingViewControllerDelegate>

@property (nonatomic, readonly) ReadingPageViewController *leftReadingPageViewController;
@property (nonatomic, readonly) ReadingPageViewController *rightReadingPageViewController;
@property (nonatomic, readonly) ComparingViewController *leftComparingViewController;
@property (nonatomic, readonly) ComparingViewController *rightComparingViewController;

@end

@implementation ComparingContainerViewController

@synthesize leftPageInfo = _leftPageInfo;
@synthesize rightPageInfo = _rightPageInfo;
@synthesize switchOn = _switchOn;
@synthesize keywords = _keywords;
@synthesize searchType = _searchType;

- (ComparingViewController *)leftComparingViewController
{
    return (ComparingViewController *)((UINavigationController *)self.childViewControllers[0]).topViewController;
}

- (ComparingViewController *)rightComparingViewController
{
    return (ComparingViewController *)((UINavigationController *)self.childViewControllers[1]).topViewController;
}

- (ReadingPageViewController *)leftReadingPageViewController
{
    return self.leftComparingViewController.childViewControllers.count ? self.leftComparingViewController.childViewControllers[0] : nil;
}

- (ReadingPageViewController *)rightReadingPageViewController
{
    return self.rightComparingViewController.childViewControllers.count ? self.rightComparingViewController.childViewControllers[0] : nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.leftComparingViewController.shortNoteSwitch.on = self.switchOn;
    self.leftComparingViewController.side = ComparingSideLeft;
    self.leftComparingViewController.delegate = self;
    
    self.rightComparingViewController.shortNoteSwitch.on = self.switchOn;
    self.rightComparingViewController.side = ComparingSideRight;
    self.rightComparingViewController.delegate = self;
    
    self.leftReadingPageViewController.keywords = self.keywords;
    self.leftReadingPageViewController.searchType = self.searchType;
    
    [self.leftReadingPageViewController open:self.leftPageInfo keywords:self.keywords];
    
    [self.rightReadingPageViewController open:self.rightPageInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

#pragma mark - ComparingViewControllerDelegate

- (void)comparingViewController:(ComparingViewController *)controller shouldCompare:(ComparingSide)side
{
    if (self.leftComparingViewController.code == LanguageCodeThaiFiveBooks || self.rightComparingViewController.code == LanguageCodeThaiFiveBooks) {
        return;
    }
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Loading..."];
    ComparingViewController *comparingViewController = ComparingSideLeft == side ? self.leftComparingViewController : self.rightComparingViewController;
    ReadingPageViewController *readingPageViewController = ComparingSideLeft == side ? self.rightReadingPageViewController : self.leftReadingPageViewController;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSArray *items = [comparingViewController.dataModel queryItemsInVolume:comparingViewController.volume page:comparingViewController.page];
        if (items.count) {
            NSInteger item = [items[0] integerValue];
            
            NSArray *result1 = [comparingViewController.dataModel convertToPivot:comparingViewController.volume page:comparingViewController.page item:item];
            NSNumber *rVolume = result1[0];
            NSNumber *rItem = result1[1];
            NSNumber *rSubItem = result1[2];
            
            NSArray *result2 = [[SharedDataModel sharedDataModel:readingPageViewController.code] convertFromPivot:rVolume.integerValue item:rItem.integerValue subItem:rSubItem.integerValue];
            NSNumber *volume = result2[0];
            NSNumber *page = result2[1];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [readingPageViewController open:readingPageViewController.code volume:volume.integerValue page:page.integerValue];
                [SVProgressHUD dismiss];
            });            
        }
    });
}

@end
