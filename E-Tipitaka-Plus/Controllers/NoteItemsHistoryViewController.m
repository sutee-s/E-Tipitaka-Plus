//
//  NoteItemsHistoryViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/9/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>

#import "UserDatabaseHelper.h"
#import "NoteItemsHistoryViewController.h"
#import "NoteItem.h"

@interface NoteItemsHistoryViewController ()

@end

@implementation NoteItemsHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.noteTextView.layer.borderWidth = 1;
    self.noteTextView.layer.borderColor = [[UIColor blackColor] CGColor];    
    NSLog(@"%d:%d", self.rowId, self.index);
    NSMutableDictionary *noteItems = [UserDatabaseHelper getHistoryNoteItemsByRowId:self.rowId];
    if ([noteItems objectForKey:[NSNumber numberWithInteger:self.index]]) {
        NoteItem *noteItem = [noteItems objectForKey:[NSNumber numberWithInteger:self.index]];
        self.noteTextView.text = noteItem.note;
        self.segmentedControl.selectedSegmentIndex = noteItem.state;
    }
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)save:(id)sender
{
    [UserDatabaseHelper updateHistoryNoteItems:self.index note:self.noteTextView.text state:self.segmentedControl.selectedSegmentIndex rowId:self.rowId];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", nil)];
}

@end
