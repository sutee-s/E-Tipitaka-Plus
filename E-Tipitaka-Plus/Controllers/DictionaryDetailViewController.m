//
//  DictionaryDetailViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/10/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "DictionaryDetailViewController.h"
#import "UIWebView+Javascript.h"
#import "HighlightDict.h"
#import "UserDatabaseHelper.h"
#import "NSString+Selection.h"

@interface DictionaryDetailViewController ()<UIWebViewDelegate, UIGestureRecognizerDelegate>

@end

@implementation DictionaryDetailViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.webView.delegate = self;
  self.webView.scrollView.bounces = YES;
  self.webView.scalesPageToFit = YES;
  
  UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
  tapRecognizer.numberOfTapsRequired = 1;
  tapRecognizer.delegate = self;
  [self.webView addGestureRecognizer:tapRecognizer];
  
  self.title = self.lexicon.head;
   
  NSString *html = [self createHtmlContent];
  
  [self.webView loadHTMLString:html baseURL:[NSURL URLWithString:@"https://www.etipitaka.com"]];
  
}

- (NSString *)createHtmlContent
{
  NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dict_detail" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
  NSString *script = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"script" ofType:@"js"] encoding:NSUTF8StringEncoding error:nil];
  
  NSString *font = self.dictionaryType == DictionaryTypeEnglish ? @"Helvetica" : @"THSarabunNew";
  NSString *html = [NSString stringWithFormat: template, font, HIGHLIGHT_DICT_COLOR, script, [self.lexicon.translation stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
  return [html stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"]; // escape single quote
}

- (void)handleGesture:(UIGestureRecognizer *)sender {}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)addHighlight:(id)sender
{
  HighlightDict *highlight = [[HighlightDict alloc] init];
  highlight.type = self.dictionaryType;
  highlight.wid = self.lexicon.rowId;
  highlight.selection = [self.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
  
  NSArray *result = [[self.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCharOffsets();"] objectFromJSONString];
  highlight.range = NSMakeRange([result[0] integerValue], [result[1] integerValue] - [result[0] integerValue]);
  
  NSString *position = [self.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCountPosition();"];
  highlight.position = position.integerValue;
  
  highlight.color = HIGHLIGHT_DICT_COLOR;
  highlight.note = @"";
  
  [UserDatabaseHelper saveHighlightDict:highlight];
  
  [self update];
}

- (void)update
{
  NSString *html = [self createHtmlContent];
  
  html = [html stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
  html = [html stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
[self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"updateHtml('%@');", [html JSONString]]];
  
  NSArray *highlights = [UserDatabaseHelper queryHighlightDicts:self.lexicon.type rowId:self.lexicon.rowId];
  for (HighlightDict *highlight in highlights) {
    NSString *selection = [highlight.selection formatSelection];
    
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"addHighlightDict('%@', %@, %@);", selection, [NSNumber numberWithInteger:highlight.position], [NSNumber numberWithInteger:highlight.rowId]]];
  }
}

- (void)handleClickAtHighlight:(NSInteger)rowId
{
  HighlightDict *highlight = [UserDatabaseHelper getHighlightDictByRowId:rowId];
  
  UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@", highlight.selection] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
  
  [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
    [UserDatabaseHelper deleteHighlightDict:rowId];
    [self update];
  }]];
  
  [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
  
  [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
  CGRect rect = self.view.frame;
  rect.origin.x = self.view.frame.size.width / 20;
  rect.origin.y = self.view.frame.size.height / 20;
  actionSheet.popoverPresentationController.sourceView = self.view;
  actionSheet.popoverPresentationController.sourceRect = rect;
  [self presentViewController:actionSheet animated:YES completion:nil];
  
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] hasPrefix:@"js-call://highlight_click"]) {
      [self handleClickAtHighlight:[request.URL.pathComponents.lastObject integerValue]];
        return NO;
    }
    return YES;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
  [webView.window makeKeyAndVisible];
  [webView becomeFirstResponder];
  
  [webView loadJavascriptFileFromResourceBundle:@"rangy-core.js"];
  [webView loadJavascriptFileFromResourceBundle:@"rangy-textrange.js"];
  [webView loadJavascriptFileFromResourceBundle:@"rangy-cssclassapplier.js"];
  [webView loadJavascriptFileFromResourceBundle:@"rangy-serializer.js"];
  [webView loadJavascriptFileFromResourceBundle:@"jquery-2.1.1.min.js"];
  [webView stringByEvaluatingJavaScriptFromString:@"rangy.init();"];
  
  [self update];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
  NSString* selection = [self.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
  NSArray *result = [[self.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCharOffsets();"] objectFromJSONString];
  NSRange range = NSMakeRange([result[0] integerValue], [result[1] integerValue] - [result[0] integerValue]);
  
  BOOL overlap = [UserDatabaseHelper checkOverlapHighlightDicts:self.lexicon.type wid:self.lexicon.rowId range:range];
  
  if ([[selection stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 || overlap) {
    return action != @selector(addHighlight:) && action == @selector(copy:);
  }
  
  return action == @selector(addHighlight:) || action == @selector(copy:);
}

@end
