//
//  MultiComparingViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 3/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReadingContainerViewController.h"
#import "PageInfo.h"

@class MultiComparingViewController;

@protocol MultiComparingViewControllerDelegate <NSObject>

- (void)multiComparingViewControllerViewDidLoad:(MultiComparingViewController *)controller;
- (void)multiComparingViewControllerViewWillClose:(MultiComparingViewController *)controller;
- (void)multiComparingViewControllerView:(MultiComparingViewController *)controller withCompare:(PageInfo *)pageInfo;
- (void)multiComparingViewControllerViewConvertToPdf:(MultiComparingViewController *)controller showInButton:(UIBarButtonItem *)button;

@end

@interface MultiComparingViewController : ReadingContainerViewController


@property (nonatomic, strong) PageInfo *pageInfo;
@property (nonatomic, weak) id<MultiComparingViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic, weak) UIBarButtonItem *pdfButton;

@end
