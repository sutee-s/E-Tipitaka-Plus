//
//  ReadingStateDetailTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 12/23/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"

@interface ReadingStateDetailTableViewController : UITableViewController

@property (nonatomic, strong) NSString *customTitle;
@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) ReadingStateType state;
@property (nonatomic, assign) NSInteger totalPages;

@end
