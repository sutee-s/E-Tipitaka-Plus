//
//  SearchViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+MMDrawerController.h"
#import "ETViewController.h"
#import "AutoCompleteSearchBar.h"
#import "History.h"

@class SearchViewController;

@protocol SearchViewControllerDelegate <NSObject>

@optional
- (void)searchViewController:(SearchViewController *)controller didSearch:(NSString *)keywords saveHistory:(History *)history;

@end

@interface SearchViewController : ETViewController

@property (weak, nonatomic) IBOutlet AutoCompleteSearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<SearchViewControllerDelegate> delegate;
@property (nonatomic, assign) NSInteger number;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, strong) History *history;
@property (nonatomic, assign) BOOL comparingMode;

@end
