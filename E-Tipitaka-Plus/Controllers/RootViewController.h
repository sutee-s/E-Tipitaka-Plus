//
//  RootViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 23/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMDrawerController+Storyboard.h>


@interface RootViewController : MMDrawerController

@end
