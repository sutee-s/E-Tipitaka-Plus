//
//  DictionaryDetailViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/10/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lexicon.h"
#import "MyWebView.h"

@interface DictionaryDetailViewController : UIViewController

@property (nonatomic, weak) IBOutlet MyWebView *webView;
@property (nonatomic, strong) Lexicon *lexicon;
@property (nonatomic, assign) DictionaryType dictionaryType;

@end
