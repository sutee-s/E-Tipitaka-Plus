//
//  BaseBookmarkListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "BaseBookmarkListViewController.h"
#import "Bookmark.h"
#import "NoteViewCell.h"
#import "NSString+Thai.h"
#import "UserDatabaseHelper.h"
#import "Constants.h"

@implementation BaseBookmarkListViewController

@synthesize tableView = _tableView;
@synthesize bookmarks = _bookmarks;


#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    Bookmark *bookmark = [self.bookmarks objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeAndPageNotification object:@{@"volume":[NSNumber numberWithInt:bookmark.volume], @"page":[NSNumber numberWithInt:bookmark.page], @"bookmark":[NSNumber numberWithBool:YES]}];
    return indexPath;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bookmarks.count;
}

- (NoteViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Bookmark Cell";
    
    NoteViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[NoteViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Bookmark *bookmark = [self.bookmarks objectAtIndex:indexPath.row];
  
  cell.noteLabel.attributedText = [[NSAttributedString alloc] initWithString:bookmark.snippet attributes:@{NSFontAttributeName: [UIFont fontWithName:@"THSarabunNew" size:NOTE_FONT_SIZE]}];
  
    cell.volumeLabel.text = [[NSString stringWithFormat:HEADER_TEMPLATE_3, bookmark.volume, bookmark.page + [self.dataModel startPageOfVolume:bookmark.volume] - 1] stringByReplacingThaiNumeric];
    cell.infoLabel.text = @"";
    
    cell.accessoryView = bookmark.starred ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_star"]] : nil;
    
    NSArray *tagNames = [UserDatabaseHelper queryTagNamesFrom:@"note" byRowId:bookmark.rowId withCode:bookmark.code];
    if (tagNames.count > 0) {
        cell.tagLabel.text = [NSString stringWithFormat:@"[%@]", [tagNames componentsJoinedByString:@"|"]];
    } else {
        cell.tagLabel.text = @"";
    }
    
    return cell;
}

@end
