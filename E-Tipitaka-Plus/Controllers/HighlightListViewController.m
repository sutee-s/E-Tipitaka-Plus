//
//  HighlightListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "HighlightListViewController.h"
#import "Highlight.h"
#import "UserDatabaseHelper.h"
#import "PageInfo.h"
#import "NSString+Thai.h"
#import "NoteViewCell.h"
#import "ActionSheetPicker.h"

@interface HighlightListViewController ()<UIGestureRecognizerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation HighlightListViewController

@synthesize tableView = _tableView;
@synthesize searchBar = _searchBar;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:RefreshHistoryListNotification object:nil];
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (void)changeCode:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        self.code = [notification.object intValue];
        [self refresh];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        [self refresh];
        self.code = ((PageInfo *)notification.object).code;
    }
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Highlight_", nil), self.dataModel.shortTitle];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.searchBar.delegate = self;
    self.items = [[UserDatabaseHelper queryHighlightsOfCode:self.code] mutableCopy];
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    recognizer.minimumPressDuration = 1.5;
    recognizer.delegate = self;
    [self.tableView addGestureRecognizer:recognizer];

}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.selectedIndexPath = indexPath;
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Add tag", nil), nil];
        [actionSheet showInView:self.view];
    }
}

- (void)chooseTags
{
    NSArray *names = [UserDatabaseHelper queryTagNames];
    if (!names.count) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No tag found", nil) message:NSLocalizedString(@"Please add a tag", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"Select a tag", nil)
                                            rows:names
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           Highlight *highlight = self.items[self.selectedIndexPath.row];
                                           [UserDatabaseHelper addTagItems:@"highlight" rowId:highlight.rowId ofTag:selectedValue withCode:self.code];
                                           [self.tableView beginUpdates];
                                           [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                                           [self.tableView endUpdates];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self.view];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)refresh:(NSNotification *)notification
{
    [self refresh];
}

- (void)refresh
{
    if (self.searchBar.text.length) {
        self.items = [[UserDatabaseHelper queryHighlightsByKeyword:self.searchBar.text code:self.code] mutableCopy];
    } else {
        self.items = [[UserDatabaseHelper queryHighlightsOfCode:self.code] mutableCopy];
    }
    [self sort:nil];
}

- (IBAction)sort:(id)sender
{
    self.items = [[self.items sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Highlight *highlight1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        Highlight *highlight2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [[NSNumber numberWithInteger:highlight1.rowId] compare:[NSNumber numberWithInteger:highlight2.rowId]];
    }] mutableCopy];

    self.items = [[self.items sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Highlight *highlight1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        Highlight *highlight2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [highlight1.selection compare:highlight2.selection];
            case 1:
                if (highlight1.volume == highlight2.volume) {
                    return [[NSNumber numberWithInteger:highlight1.page] compare:[NSNumber numberWithInteger:highlight2.page]];
                }
                return [[NSNumber numberWithInteger:highlight1.volume] compare:[NSNumber numberWithInteger:highlight2.volume]];
            case 2:
                return [[NSNumber numberWithInteger:highlight1.rowId] compare:[NSNumber numberWithInteger:highlight2.rowId]];
            default:
                return [[NSNumber numberWithInteger:highlight1.rowId] compare:[NSNumber numberWithInteger:highlight2.rowId]];
        }
    }] mutableCopy];
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Highlight *highlight = self.items[indexPath.row];
        [UserDatabaseHelper deleteHighlight:highlight.rowId];
        [self refresh];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}

#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self chooseTags];
    } 
}


@end
