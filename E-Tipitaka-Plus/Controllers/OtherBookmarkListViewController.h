//
//  OtherBookmarkListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/4/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETViewController.h"
#import "OtherUser.h"

@interface OtherBookmarkListViewController : ETViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingOrderSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingTypeSegmentedControl;

@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) OtherUser *user;

@end
