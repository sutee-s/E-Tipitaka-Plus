//
//  ReadingContainerViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETViewController.h"
#import "ReadingPageViewController.h"

@interface ReadingContainerViewController : ETViewController

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *jumpToButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *compareToButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *fontColorButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *dictButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pdfButton;
@property (weak, nonatomic) IBOutlet UISwitch *shortNoteSwitch;
@property (weak, nonatomic) IBOutlet UILabel *readingStateView;;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *noteButton;

@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, readonly) NSInteger page;
@property (nonatomic, assign) NSUInteger number;

@property (nonatomic, readonly) ReadingPageViewController *readingPageViewController;

- (void)changeCode:(NSNotification *)notification;
- (void)semiModalDidHide:(NSNotification *)notification;
- (void)semiModalDidShow:(NSNotification *)notification;
- (void)setupLeftMenuButton;
- (void)setupRightMenuButton;
- (void)compareWithPageInfo:(PageInfo *)pageInfo;
- (IBAction)showModalViewController:(id)sender;
- (void)doSync:(NSNotification *)notification;

@end
