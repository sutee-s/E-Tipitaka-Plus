//
//  ComparingContainerViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 28/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageInfo.h"

@interface ComparingContainerViewController : UIViewController

@property (nonatomic, strong) PageInfo *leftPageInfo;
@property (nonatomic, strong) PageInfo *rightPageInfo;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, assign) SearchType searchType;
@property (nonatomic, assign) BOOL switchOn;

@end
