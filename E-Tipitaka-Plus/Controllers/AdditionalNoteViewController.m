//
//  AdditionalNoteViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "AdditionalNoteViewController.h"
#import "UserDatabaseHelper.h"
#import "NSString+Thai.h"
#import "UIColor+HexHTMLString.h"
#import "PageInfo.h"
#import "NSAttributedString+RichTextEditor.h"
#import "UITextView+ApplyAttributes.h"
#import "HighlightColorList.h"
#import "Constants.h"


@interface AdditionalNoteViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, WKNavigationDelegate, UITextViewDelegate>
{
    BOOL _editing;
    BOOL _starred;
}

@property (nonatomic, strong) UIBarButtonItem *editItem;
@property (nonatomic, strong) UIBarButtonItem *doneItem;
@property (nonatomic, strong) UIBarButtonItem *starItem;
@property (nonatomic, strong) UIBarButtonItem *closeItem;
@property (nonatomic, strong) UIBarButtonItem *darkStarItem;
@property (nonatomic, strong) UIBarButtonItem *refItem;
@property (nonatomic, strong) UIBarButtonItem *fixedSpaceItem;
@property (nonatomic, strong) UIBarButtonItem *flexibleSpaceItem;
@property (nonatomic, strong) UIBarButtonItem *titleItem;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UITextView *textView;

@property (nonatomic, strong) NSArray *bookArray;
@property (nonatomic, strong) NSArray *volumeArray;
@property (nonatomic, strong) NSArray *pageArray;
@property (nonatomic, strong) NSArray *itemArray;

@property (nonatomic, strong) UIPickerView *pickerFrame;
@property (nonatomic, strong) HighlightColor *highlightColor;

@end

@implementation AdditionalNoteViewController

@synthesize volume = _volume;
@synthesize page = _page;

@synthesize editItem = _editItem;
@synthesize doneItem = _doneItem;
@synthesize starItem = _starItem;
@synthesize closeItem = _closeItem;
@synthesize refItem = _refItem;
@synthesize titleItem = _titleItem;

@synthesize darkStarItem = _darkStarItem;
@synthesize fixedSpaceItem = _fixedSpaceItem;

@synthesize webView = _webView;
@synthesize textView = _textView;

@synthesize delegate = _delegate;


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
        self.standalone = YES;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIBarButtonItem *)titleItem
{
  if (!_titleItem) {
    _titleItem = [[UIBarButtonItem alloc] initWithTitle:self.title style:UIBarButtonItemStylePlain target:nil action:nil];
    _titleItem.enabled = NO;
    [_titleItem setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateDisabled];
  }
  return _titleItem;
}

- (UIBarButtonItem *)flexibleSpaceItem
{
  if (!_flexibleSpaceItem) {
    _flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
  }
  return _flexibleSpaceItem;
}

- (UIBarButtonItem *)fixedSpaceItem
{
    if (!_fixedSpaceItem) {
        _fixedSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        _fixedSpaceItem.width = 10.0f;
    }
    return _fixedSpaceItem;
}

- (UIBarButtonItem *)closeItem
{
    if (!_closeItem) {
        _closeItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(close:)];
    }
    return _closeItem;
}

- (UIBarButtonItem *)refItem
{
    if (!_refItem) {
        _refItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Reference", nil) style:UIBarButtonItemStylePlain target:self action:@selector(insertReference:)];
    }
    return _refItem;
}

- (UIBarButtonItem *)editItem
{
    if (!_editItem) {
        _editItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(edit:)];
    }
    return _editItem;
}

- (UIBarButtonItem *)doneItem{
    if (!_doneItem) {
        _doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    }
    return _doneItem;
}

- (UIBarButtonItem *)starItem
{
    if (!_starItem) {
        _starItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"star"] style:UIBarButtonItemStylePlain target:self action:@selector(addStar:)];
    }
    return _starItem;
}

- (UIBarButtonItem *)darkStarItem
{
    if (!_darkStarItem) {
        _darkStarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dark_star"] style:UIBarButtonItemStylePlain target:self action:@selector(removeStar:)];
    }
    return _darkStarItem;
}

- (void)keyboardChangeFrame:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    NSValue* value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
      
    self.textView.frame = CGRectMake(
      0,
      self.highlightToolBar.frame.size.height+self.highlightToolBar.frame.origin.y,
      self.view.frame.size.width,
      self.view.frame.size.height-self.highlightToolBar.frame.size.height-keyboardFrame.size.height-self.highlightToolBar.frame.origin.y);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
  self.title = [[NSString stringWithFormat:NSLocalizedString(@"volume %1$li page %2$li", nil), self.volume, self.page] stringByReplacingThaiNumeric];;
  
    self.highlightColor = [HighlightColorList getCurrentHighlightColor];
  
    self.webView = [[WKWebView alloc] init];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    self.webView.scrollView.bounces = NO;
    self.webView.navigationDelegate = self;
    [self.view addSubview:self.webView];
    
    self.textView = [[UITextView alloc] init];
    self.textView.font = [UIFont systemFontOfSize:20];
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview:self.textView];
    
    self.bookArray = REF_BOOKS;
    self.volumeArray = @[];
    self.pageArray = @[];
    self.itemArray = @[];

  [self updateColorView];
}


-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
  if (action == NSSelectorFromString(@"addHighlight:") || action == NSSelectorFromString(@"clear:")) {
      return !self.textView.selectedTextRange.isEmpty;
  }
  return NO;
}

- (void)buildMenuWithBuilder:(id<UIMenuBuilder>)builder API_AVAILABLE(ios(13.0))  {
  if (@available(iOS 16.0, *)) {
    [builder removeMenuForIdentifier:UIMenuLookup];
    [builder removeMenuForIdentifier:UIMenuShare];
    [builder removeMenuForIdentifier:UIMenuSpeech];
    [builder removeMenuForIdentifier:UIMenuStandardEdit];
    [builder removeMenuForIdentifier:UIMenuReplace];
  }
  [super buildMenuWithBuilder:builder];
}

- (IBAction)selectColor:(id)sender
{
    [self updateColorView];
}

- (void)updateColorView
{
  NSString *colorCode = self.highlightColor.noteColors[self.colorControl.selectedSegmentIndex];
  self.colorView.backgroundColor = [UIColor pxColorWithHexValue:colorCode];
}

- (void)addHighlight:(id)sender
{
  NSString *colorCode = self.highlightColor.noteColors[self.colorControl.selectedSegmentIndex];
  [self.textView applyAttrubutesToSelectedRange:[UIColor pxColorWithHexValue:colorCode] forKey:NSBackgroundColorAttributeName];
}

- (void)clear:(id)sender
{
  [self.textView applyAttrubutesToSelectedRange:[UIColor whiteColor] forKey:NSBackgroundColorAttributeName];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resizeViews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self resizeViews];
    
    [self setUpEdit];
    if (!self.firstEdit) {
        [self setUpDone];
    }
}

- (void)resizeViews
{
    self.webView.frame = CGRectMake
    (0,
     self.highlightToolBar.frame.size.height+self.highlightToolBar.frame.origin.y,
     self.view.frame.size.width,
     self.view.frame.size.height-self.highlightToolBar.frame.size.height-self.highlightToolBar.frame.origin.y);
  
    self.textView.frame = CGRectMake
    (0,
     self.highlightToolBar.frame.size.height+self.highlightToolBar.frame.origin.y,
     self.view.frame.size.width,
     self.view.frame.size.height-self.highlightToolBar.frame.size.height-self.highlightToolBar.frame.origin.y);
}

- (NSString *)note
{
    Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:self.volume page:self.page withCode:self.code];
    return bookmark.note;
}

- (NSString *)timestamp {
    Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:self.volume page:self.page withCode:self.code];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocalizedDateFormatFromTemplate:@"dd/MM/yyyy HH:mm:ss"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"th"]];
    return bookmark ? [dateFormatter stringFromDate:bookmark.created] : @"";
}

- (BOOL)starred
{
    Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:self.volume page:self.page withCode:self.code];
    return bookmark.starred;
}

- (void)save:(NSString*)text starred:(BOOL)starred
{
    [UserDatabaseHelper saveBookmark:text volume:self.volume page:self.page starred:starred withCode:self.code];
}

- (void)markStar:(BOOL)value
{
    Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:self.volume page:self.page withCode:self.code];
    [UserDatabaseHelper updateBookmarkAtColumn:@"important" setValue:[NSNumber numberWithBool:value] byRowId:bookmark.rowId];
}

- (void)setUpEdit
{
    self.webView.hidden = YES;
    [self.view bringSubviewToFront:self.textView];
    self.textView.hidden = NO;
    
    _starred = [self starred];
    self.toolBar.items = @[self.closeItem, (_starred) ? self.darkStarItem : self.starItem, self.flexibleSpaceItem, self.titleItem, self.flexibleSpaceItem, self.refItem, self.doneItem];
    [self.textView becomeFirstResponder];
  
  
    NSString *note = [self note];
    
    
    if (note.length > 0) {
      self.textView.attributedText = [[NSAttributedString alloc] initWithData:[note dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
      
      [self.textView applyAttributes:[UIFont fontWithName:@"THSarabunNew" size:NOTE_FONT_SIZE] forKey:NSFontAttributeName atRange:NSMakeRange(0, self.textView.text.length)];
      
      [self.textView setSelectedRange:NSMakeRange(0, 0)];
      [self.textView scrollRangeToVisible:NSMakeRange(0, 0)];
    } else {
      [self.textView applyAttributeToTypingAttribute:[UIFont fontWithName:@"THSarabunNew" size:NOTE_FONT_SIZE] forKey:NSFontAttributeName];
    }
    
    _editing = YES;
  
}

- (void)setUpDone
{
    self.webView.hidden = NO;
    [self.view bringSubviewToFront:self.webView];
    self.textView.hidden = YES;
  
    BOOL starred = [self starred];
    
    _starred = starred;
    self.toolBar.items = @[self.closeItem, (starred) ? self.darkStarItem : self.starItem, self.flexibleSpaceItem, self.titleItem, self.flexibleSpaceItem, self.editItem];
    [self.textView resignFirstResponder];
  
  NSString *content = self.textView.text.length > 0 ? [self.textView.attributedText htmlString] : @"";
    
    NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"note_no_title" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
    content = [content stringByAppendingFormat:@"<br/>(%@ %@)", NSLocalizedString(@"Saved at :", nil), [self timestamp]];
    content = [NSString stringWithFormat:template, NOTE_FONT_SIZE, content];

    content = [self formatReference:content];
  
    [self.webView loadHTMLString:content baseURL:[[NSBundle mainBundle] bundleURL]];
  
    _editing = NO;
}

- (NSString *)formatReference:(NSString *)content
{
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:REF_PATTERN options:NSRegularExpressionCaseInsensitive error:&error];
    return [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@"<a href='ref://etipitaka.com/$1/$2/$3/$4'>$1/$2/$3/$4</a>"];
}

- (void)processRefRequest:(NSString *)ref
{
    NSArray *tokens = [ref componentsSeparatedByString:@"/"];
    NSString *book = [tokens objectAtIndex:1];
    NSInteger volume = [[tokens objectAtIndex:2] integerValueFromThaiNumeric];
    NSInteger page = [[tokens objectAtIndex:3] integerValueFromThaiNumeric];
    
    LanguageCode code = LanguageCodeThaiSiam;
    if ([book isEqualToString:REF_THAI]) {
        code = LanguageCodeThaiSiam;
    } else if ([book isEqualToString:REF_PALI]) {
        code = LanguageCodePaliSiam;
    } else if ([book isEqualToString:REF_THAIMM]) {
        code = LanguageCodeThaiMahaMakut;
    } else if ([book isEqualToString:REF_THAIMC]) {
        code = LanguageCodeThaiMahaChula;
    }
    NSLog(@"%d/%ld/%ld", code, volume, page);
    
    [self close:nil];
    
    PageInfo *pageInfo = [[PageInfo alloc] initWithCode:code volume:volume page:page];
    if (self.standalone) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabasAndVolumeAndPageNotification object:pageInfo];
    } else {
        [self.delegate additionalNoteViewController:self changePage:pageInfo];
    }
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
  if ([navigationAction.request.URL.scheme isEqualToString:@"ref"]) {
    [self processRefRequest:navigationAction.request.URL.path];
    decisionHandler(WKNavigationActionPolicyCancel);
  } else {
    decisionHandler(WKNavigationActionPolicyAllow);
  }
}

- (IBAction)insertReference:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Reference", nil) message:@"\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.modalInPresentation = YES;
    
    self.pickerFrame = [[UIPickerView alloc] initWithFrame:CGRectMake(17, 52, 270, 150)];
    
    CGFloat labelWidth = self.pickerFrame.frame.size.width / 4;
    
    for (int i=0; i<4; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(labelWidth * i, 0, labelWidth, 20)];
        switch (i) {
            case 0:
                label.text = @"";
                break;
            case 1:
                label.text = NSLocalizedString(@"Volume", nil);
                break;
            case 2:
                label.text = NSLocalizedString(@"Page", nil);
                break;
            case 3:
                label.text = NSLocalizedString(@"Item", nil);
                break;
            default:
                break;
        }
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:12];
        [self.pickerFrame addSubview:label];
    }
    
    self.pickerFrame.delegate = self;
    self.pickerFrame.dataSource = self;
    [alert.view addSubview:self.pickerFrame];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.textView insertText:[NSString stringWithFormat:@" %@ ", [self reference]]];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    alert.popoverPresentationController.sourceView = self.toolBar;
    alert.popoverPresentationController.sourceRect = self.toolBar.frame;
    alert.preferredContentSize = CGSizeMake(350, 400);
    
    [self presentViewController:alert animated:YES completion:^{
        if (self.code < 5) {
            [self.pickerFrame selectRow:self.code-1 inComponent:0 animated:YES];
        } else {
            [self.pickerFrame selectRow:0 inComponent:0 animated:YES];
        }
        [self refreshPickerData];
        [self.pickerFrame selectRow:self.volume-1 inComponent:1 animated:YES];
        [self.pickerFrame selectRow:self.page-1 inComponent:2 animated:YES];
    }];
}

- (NSString *)reference
{
    NSString *book = [self.bookArray objectAtIndex:[self.pickerFrame selectedRowInComponent:0]];
    NSString *volume = [self.volumeArray objectAtIndex:[self.pickerFrame selectedRowInComponent:1]];
    NSString *page = [self.pageArray objectAtIndex:[self.pickerFrame selectedRowInComponent:2]];
    NSString *item = [self.itemArray objectAtIndex:[self.pickerFrame selectedRowInComponent:3]];
    
    return [NSString stringWithFormat:@"%@/%@/%@/%@", book,volume,page,item];
}

- (void)refreshPickerData
{
    NSInteger bookComponent = [self.pickerFrame selectedRowInComponent:0];
    self.volumeArray = bookComponent == 2 ? [self createThaiNumbers:1 end:91] : [self createThaiNumbers:1 end:45]; // 2 = thaimm
    
    NSInteger volumeComponent = [self.pickerFrame selectedRowInComponent:1];
    self.pageArray = [self createThaiNumbers:1 end:[self.dataModel totalPagesOfVolume:volumeComponent+1]];
    
    NSInteger pageComponent = [self.pickerFrame selectedRowInComponent:2];
    NSArray *items = [self.dataModel queryItemsInVolume:volumeComponent+1 page:pageComponent+1];
    self.itemArray = [self createThaiNumbers:[items.firstObject integerValue] end:[items.lastObject integerValue]];
    
    [self.pickerFrame reloadAllComponents];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *tView = (UILabel *)view;
    if (!tView) {
        tView = [[UILabel alloc] init];
        tView.font = [UIFont systemFontOfSize:12];
        tView.textAlignment = NSTextAlignmentCenter;
    }
    
    tView.text = [self fetchTitleForRow:row forComponent:component];
    
    return tView;
}



- (NSArray *) createThaiNumbers:(NSInteger)start end:(NSInteger)end
{
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSInteger i = start; i<=end; i++) {
        NSString *number = [NSString stringWithFormat:@"%ld", i];
        [array addObject:[number stringByReplacingThaiNumeric]];
    }
    
    return array;
}

- (NSString *)fetchTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [self.bookArray objectAtIndex:row];
        case 1:
            return [self.volumeArray objectAtIndex:row];
        case 2:
            return [self.pageArray objectAtIndex:row];
        case 3:
        default:
            return [self.itemArray objectAtIndex:row];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 4;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return self.bookArray.count;
        case 1:
            return self.volumeArray.count;
        case 2:
            return self.pageArray.count;
        case 3:
        default:
            return self.itemArray.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self fetchTitleForRow:row forComponent:component];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        switch (row) {
            case 0:
                self.code = LanguageCodeThaiSiam;
                break;
            case 1:
                self.code = LanguageCodePaliSiam;
                break;
            case 2:
                self.code = LanguageCodeThaiMahaMakut;
                break;
            case 3:
                self.code = LanguageCodeThaiMahaChula;
                break;
            default:
                break;
        }
    }
    [self refreshPickerData];
}

- (void)close
{
  [self.textView resignFirstResponder];
  __weak AdditionalNoteViewController *weakSelf = self;
  [self dismissViewControllerAnimated:YES completion:^{
      if (weakSelf.delegate) {
          [weakSelf.delegate additionalNoteViewControllerDidDismiss:weakSelf];
      }
  }];
}

- (IBAction)close:(id)sender
{
  if (!_editing) {
    [self close];
  } else {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Caution before closing", nil) message:NSLocalizedString(@"Do you want to save the note?", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Save", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      NSString *content = self.textView.text.length > 0 ? [self.textView.attributedText htmlString] : @"";
      [self save:content starred:_starred];
      [self close];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
      [self close];
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
  }
}

- (IBAction)edit:(id)sender
{
    [self setUpEdit];
}

- (IBAction)done:(id)sender
{
  NSString *content = self.textView.text.length > 0 ? [self.textView.attributedText htmlString] : @"";
  [self save:content starred:_starred];
  [self setUpDone];
}

- (IBAction)addStar:(id)sender
{
  [self markStar:YES];
  NSMutableArray *items = [NSMutableArray arrayWithArray:@[self.closeItem, self.darkStarItem, self.flexibleSpaceItem, self.titleItem, self.flexibleSpaceItem]];
  if (_editing) {
    [items addObject:self.refItem];
  }
  [items addObject:_editing ? self.doneItem : self.editItem];
  self.toolBar.items = items;
  _starred = YES;
}

- (IBAction)removeStar:(id)sender
{
  [self markStar:NO];
  NSMutableArray *items = [NSMutableArray arrayWithArray:@[self.closeItem, self.starItem, self.flexibleSpaceItem, self.titleItem, self.flexibleSpaceItem]];
  if (_editing) {
    [items addObject:self.refItem];
  }
  [items addObject:_editing ? self.doneItem : self.editItem];
  self.toolBar.items = items;
  _starred = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
