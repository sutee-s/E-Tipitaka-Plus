//
//  HighlightNoteViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/8/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "HighlightNoteViewController.h"
#import "UIWebView+Javascript.h"
#import "UserDatabaseHelper.h"
#import "HighlightNote.h"

@interface HighlightNoteViewController ()<UIWebViewDelegate, UIActionSheetDelegate>
{
    BOOL _loadedJSFiles;
}

@property (nonatomic, strong) HighlightNote *selectedHighlightNote;

@end

@implementation HighlightNoteViewController

@synthesize highlight = _highlight;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
    self.webView.scalesPageToFit = YES;
    
    [self loadHighlight:self.highlight];
    
    UIMenuItem *highlightMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Highlight", nil) action:@selector(addHighlight:)];
    [UIMenuController sharedMenuController].menuItems = @[highlightMenuItem];
}

- (void)addHighlight:(id)sender
{
    NSString *selection = [self.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
    NSArray *result = [[self.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCharOffsets();"] objectFromJSONString];
    NSRange range = NSMakeRange([result[0] integerValue], [result[1] integerValue] - [result[0] integerValue]);
    HighlightNote *highlightNote = [HighlightNote new];
    highlightNote.range = range;
    highlightNote.selection = selection;
    highlightNote.highlight = self.highlight;
    [UserDatabaseHelper addHighlightNote:highlightNote];
    [self update];
    [self.delegate highlightNoteViewController:self didChangeHighlight:self.highlight];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(addHighlight:)) {
        NSString *selection = [self.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
        NSArray *result = [[self.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCharOffsets();"] objectFromJSONString];
        NSRange range = NSMakeRange([result[0] integerValue], [result[1] integerValue] - [result[0] integerValue]);
        BOOL overlap = [UserDatabaseHelper checkOverlapHighlightNotesInRange:range ofHighlight:self.highlight];
        NSLog(@"%@ %@", selection, result);
        return !overlap;
    }
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadHighlight:(Highlight *)highlight
{
    NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"highlight_note" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:[NSString stringWithFormat:template, [self createHtmlContent]] baseURL:nil];
}

- (void)update
{
    NSString *html = [self createHtmlContent];
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"updateMain('%@');", [html JSONString]]];
}

- (NSString *)createHtmlContent
{
    return [UserDatabaseHelper createHtmlHighlightNotes:self.highlight withBlock:^NSString *(NSString *selection, NSInteger rowId) {
        return [NSString stringWithFormat:@"<a href=\"js-call://highlightnote_click/%@\" style=\"text-decoration:none;display:inline;\"><span id=\"hn%@\" class=\"highlightnote\">%@</span></a>",[NSNumber numberWithInteger:rowId], [NSNumber numberWithInteger:rowId], selection];
    }];
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)showActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:self.selectedHighlightNote.selection delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Delete", nil) otherButtonTitles:nil];
    [actionSheet showInView:self.view];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] hasPrefix:@"js-call://highlightnote_click"]) {
        NSInteger rowId = [request.URL.pathComponents.lastObject integerValue];
        self.selectedHighlightNote = [UserDatabaseHelper getHighlightNoteByRowId:rowId];
        [self showActionSheet];
        return NO;
    } 
    return YES;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [UserDatabaseHelper deleteHighlightNote:self.selectedHighlightNote];
        [self update];
        [self.delegate highlightNoteViewController:self didChangeHighlight:self.highlight];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (!_loadedJSFiles) {
        [webView loadJavascriptFileFromResourceBundle:@"rangy-core.js"];
        [webView loadJavascriptFileFromResourceBundle:@"rangy-textrange.js"];
        [webView loadJavascriptFileFromResourceBundle:@"rangy-cssclassapplier.js"];
        [webView loadJavascriptFileFromResourceBundle:@"rangy-serializer.js"];
        [webView loadJavascriptFileFromResourceBundle:@"jquery-2.1.1.min.js"];
        [webView stringByEvaluatingJavaScriptFromString:@"rangy.init();"];
        _loadedJSFiles = YES;
    }
    [webView.window makeKeyAndVisible];
    [webView becomeFirstResponder];
}

@end
