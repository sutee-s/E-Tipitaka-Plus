//
//  OtherBookmarkListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/4/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherBookmarkListViewController.h"
#import "NoteViewCell.h"
#import "Bookmark.h"
#import "UserDatabaseHelper.h"
#import "NSString+Thai.h"
#import "UIViewController+MMDrawerController.h"

@interface OtherBookmarkListViewController ()<UIGestureRecognizerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation OtherBookmarkListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    recognizer.minimumPressDuration = 1;
    recognizer.delegate = self;
    [self.tableView addGestureRecognizer:recognizer];
    
    [self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.selectedIndexPath = indexPath;        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Import bookmark", nil), nil];
        [actionSheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        Bookmark *bookmark = [self.bookmarks objectAtIndex:self.selectedIndexPath.row];
        [UserDatabaseHelper saveBookmark:bookmark.note volume:bookmark.volume page:bookmark.page starred:bookmark.starred withCode:bookmark.code];
    }
}

- (void)refresh
{
    self.bookmarks = [[UserDatabaseHelper queryBookmarks:self.searchBar.text withCode:self.code ofUser:self.user] mutableCopy];
    [self sort:nil];
}

- (IBAction)sort:(id)sender
{
    self.bookmarks = [[self.bookmarks sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Bookmark *bookmark1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        Bookmark *bookmark2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [bookmark1.created compare:bookmark2.created];
    }] mutableCopy];
    
    self.bookmarks = [[self.bookmarks sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Bookmark *bookmark1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        Bookmark *bookmark2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        
        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [bookmark1.note compare:bookmark2.note];
            case 1:
                return [[NSNumber numberWithInteger:bookmark1.volume] compare:[NSNumber numberWithInteger:bookmark2.volume]] == NSOrderedSame ? [[NSNumber numberWithInteger:bookmark1.page] compare:[NSNumber numberWithInteger:bookmark2.page]] : [[NSNumber numberWithInteger:bookmark1.volume] compare:[NSNumber numberWithInteger:bookmark2.volume]];
            case 2:
                return [bookmark1.created compare:bookmark2.created];
            case 3:
            default:
                return [[NSNumber numberWithBool:bookmark1.starred] compare:[NSNumber numberWithBool:bookmark2.starred]];
        }
    }] mutableCopy];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bookmarks.count;
}

- (NoteViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Bookmark Cell";
    
    NoteViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[NoteViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Bookmark *bookmark = [self.bookmarks objectAtIndex:indexPath.row];
    
    cell.noteLabel.text = bookmark.note;
    
    cell.volumeLabel.text = [[NSString stringWithFormat:HEADER_TEMPLATE_3, bookmark.volume, bookmark.page + [self.dataModel startPageOfVolume:bookmark.volume] - 1] stringByReplacingThaiNumeric];
    cell.infoLabel.text = [self.dataModel volumeTitle:bookmark.volume];
    
    cell.accessoryView = bookmark.starred ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_star"]] : nil;
    
    cell.tagLabel.text = @"";
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mm_drawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabaseNotification object:[NSNumber numberWithInt:self.code]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            Bookmark *bookmark = [self.bookmarks objectAtIndex:indexPath.row];
            [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeAndPageNotification object:@{@"volume":[NSNumber numberWithInteger:bookmark.volume], @"page":[NSNumber numberWithInteger:bookmark.page], @"bookmark":[NSNumber numberWithBool:YES]}];
        });
    }];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}

@end
