//
//  SearchViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "SearchViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "NSString+Thai.h"
#import "UserDatabaseHelper.h"
#import "PageInfo.h"
#import "NoteItemsHistoryViewController.h"
#import "NoteItem.h"

#define kActionSheetLongPress        10001
#define kActionSheetChooseSearchType 10002

@interface SearchViewController ()<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIScrollViewDelegate, AutoCompleteSearchBarDataSource, AutoCompleteSearchBarDelegate> {
    int _counts[3];
    BOOL _marked;
}

@property (nonatomic, strong) NSArray *results;
@property (nonatomic, strong) NSArray *names;

@property (nonatomic, strong) NSArray *skimmedItems;
@property (nonatomic, strong) NSArray *readItems;
@property (nonatomic, strong) NSArray *markedItems;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, assign) SearchType searchType;
@property (nonatomic, strong) NSMutableDictionary *noteItems;

@end

@implementation SearchViewController

@synthesize results = _results;
@synthesize names = _names;
@synthesize keywords = _keywords;
@synthesize selectedIndexPath = _selectedIndexPath;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetHistory:) name:ChangeVolumeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadHistory:) name:LoadHistoryNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveSkimming:) name:DidChangeVolumeAndPageNotification object:nil];        
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (void)refresh
{
    self.noteItems = [UserDatabaseHelper getHistoryNoteItemsByRowId:self.history.rowId];    
    [self.tableView reloadData];
}

- (void)changeCode:(NSNotification *)notification
{
    LanguageCode code =  LanguageCodeThaiSiam;
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        code = [notification.object intValue];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        code = ((PageInfo *)notification.object).code;
    }

    if (code != self.code) {
        self.code = code;
        self.history = nil;
        self.skimmedItems = nil;
        self.readItems = nil;
        self.markedItems = nil;
        self.keywords = nil;
        self.results = nil;
        self.searchBar.text = nil;
        self.searchBar.placeholder = self.dataModel.placeholder;
        self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Search", nil), self.dataModel.shortTitle];
        [self refresh];
    }
}

- (void)resetHistory:(NSNotification *)notification
{
    self.history = nil;
}

- (void)doLoadHistory:(History *)history
{
    self.skimmedItems = [UserDatabaseHelper getHistorySkimmedItemsByRowId:history.rowId];
    self.readItems = [UserDatabaseHelper getHistoryReadItemsByRowId:history.rowId];
    self.markedItems = [UserDatabaseHelper getHistoryMarkedItemsByRowId:history.rowId];
    self.keywords = [history.keywords stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.searchType = history.type == 1 ? SearchTypeAll : SearchTypeBuddhawaj;
    self.results = [UserDatabaseHelper historyItemsById:history.rowId];
    [self processResults:self.results];
}

- (void)loadHistory:(NSNotification *)notification
{
    [self.tabBarController setSelectedViewController:self.tabBarController.viewControllers[1]];
    self.history = notification.object;
    [self doLoadHistory:self.history];
}

- (void)saveSkimming:(NSNotification *)notification
{
    if (self.history) {
        NSInteger volume = [[notification.object objectForKey:@"volume"] integerValue];
        NSInteger page = [[notification.object objectForKey:@"page"] integerValue];        
        [UserDatabaseHelper updateHistorySkimmedItem:self.history.rowId volume:volume page:page];
        self.skimmedItems = [UserDatabaseHelper getHistorySkimmedItemsByRowId:self.history.rowId];
        [self refresh];
    }
}

- (void)saveReading:(NSInteger)volume page:(NSInteger)page
{
    if (self.history) {
        [UserDatabaseHelper updateHistoryReadItem:self.history.rowId volume:volume page:page];
        self.readItems = [UserDatabaseHelper getHistoryReadItemsByRowId:self.history.rowId];
        [self refresh];
    }
}

- (void)saveMarking:(NSInteger)volume page:(NSInteger)page
{
    if (self.history) {
        [UserDatabaseHelper updateHistoryMarkedItem:self.history.rowId volume:volume page:page];
        self.markedItems = [UserDatabaseHelper getHistoryMarkedItemsByRowId:self.history.rowId];
        [self refresh];
    }
}

- (void)removeMarking:(NSInteger)volume page:(NSInteger)page
{
    if (self.history) {
        [UserDatabaseHelper deleteHistoryMarkedItem:self.history.rowId volume:volume page:page];
        self.markedItems = [UserDatabaseHelper getHistoryMarkedItemsByRowId:self.history.rowId];
        [self refresh];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Search", nil), self.dataModel.shortTitle];
    self.searchBar.placeholder = self.dataModel.placeholder;
    self.searchBar.autoCompleteDataSource = self;
    self.searchBar.autoCompleteDelegate = self;
    if (self.history) {
        [self doLoadHistory:self.history];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.history) {
        self.searchBar.text = self.keywords;
    }
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)processResults:(NSArray *)results
{
    _counts[0] = _counts[1] = _counts[2] = 0;
    
    for (int i=0; i<results.count; ++i) {
        if ([results[i][@"volume"] intValue] <= [self.dataModel totalVolumesInSection:1]) {
            _counts[0] += 1;
        } else if ([results[i][@"volume"] intValue] <= [self.dataModel totalVolumesInSection:1] + [self.dataModel totalVolumesInSection:2]) {
            _counts[1] += 1;
        } else {
            _counts[2] += 1;
        }
    }
    [self refresh];
    if (results.count > 0) {
        if (self.dataModel.isTipitaka) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        } else {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }
}

- (NSDictionary *)resultByIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return self.results[indexPath.row];
    } else if (indexPath.section == 2) {
        return self.results[indexPath.row + _counts[0]];
    } else if (indexPath.section == 3) {
        return self.results[indexPath.row + _counts[0] + _counts[1]];
    }
    return nil;
}

- (void)doSearch:(NSString *)text
{
    self.keywords = text;
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Searching", nil)];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if (self.dataModel.isBuddhawajana) {
            self.results = [self.dataModel search:self.keywords type:self.searchType];
        } else {
            self.results = [self.dataModel search:self.keywords];
        }
        
        [UserDatabaseHelper saveSearchResults:self.results ofKeywords:self.keywords ofType:self.searchType withCode:self.dataModel.code totalSearch:NO];
        
        if (self.dataModel.isBuddhawajana) {
            self.history = [UserDatabaseHelper getHistory:self.keywords ofType:self.searchType withCode:self.dataModel.code];
        } else {
            self.history = [UserDatabaseHelper getHistory:self.keywords withCode:self.dataModel.code];
        }
        
        if ([self.delegate respondsToSelector:@selector(searchViewController:didSearch:saveHistory:)]) {
            [self.delegate searchViewController:self didSearch:self.keywords saveHistory:self.history];
        }
        
        self.skimmedItems = [UserDatabaseHelper getHistorySkimmedItemsByRowId:self.history.rowId];
        self.readItems = [UserDatabaseHelper getHistoryReadItemsByRowId:self.history.rowId];
        self.markedItems = [UserDatabaseHelper getHistoryMarkedItemsByRowId:self.history.rowId];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Complete", nil)];
            [self processResults:self.results];
        });
    });
    
}

- (void)chooseSearchType
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose search type", nil) delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"All", nil), NSLocalizedString(@"Only Buddhawaj", nil), nil];
    actionSheet.tag = kActionSheetChooseSearchType;
    [actionSheet showInView:self.view];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Note"]) {
        NoteItemsHistoryViewController *controller = (NoteItemsHistoryViewController *)segue.destinationViewController;
        controller.rowId = self.history.rowId;
        controller.index = [self.results indexOfObject:[self resultByIndexPath:self.selectedIndexPath]];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.keywords.length == 0) {
        return 0;
    } else if (section == 0) {
        if (self.dataModel.isTipitaka) {
            return self.results.count > 0 ? 3 : 1;
        } else {
            return self.results.count > 0 ? 0 : 1;
        }
    } else if (section < 4) {
        return _counts[section-1];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.keywords.length == 0) { return 0; }

    if (self.dataModel.isTipitaka) {
        return self.results.count > 0 ? 4 : 1;
    } else {
        return self.results.count > 0 ? 2 : 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [[NSString stringWithFormat:NSLocalizedString(@"Search results: %1$i", nil), _counts[0]+_counts[1]+_counts[2]] stringByReplacingThaiNumeric];
    } else if (section == 1) {
        return self.dataModel.isTipitaka ? NSLocalizedString(@"Section 1", nil) : nil;
    } else if (section == 2) {
        return NSLocalizedString(@"Section 2", nil);
    } else if (section == 3) {
        return NSLocalizedString(@"Section 3", nil);
    }
    return nil;
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath && indexPath.section > 0 && gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        NSInteger index = [self.results indexOfObject:[self resultByIndexPath:indexPath]];
        _marked = ![self.markedItems containsObject:[NSNumber numberWithInteger:index]];
        self.selectedIndexPath = indexPath;
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Compare", nil), _marked ? NSLocalizedString(@"Starred", nil) : NSLocalizedString(@"Unstarred", nil), NSLocalizedString(@"Take note", nil), nil];
        actionSheet.tag = kActionSheetLongPress;
        if (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            [actionSheet showFromRect:CGRectMake(p.x, p.y, 1, 1) inView:self.tableView animated:YES];
        } else {
            [actionSheet showInView:self.view];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [self.results indexOfObject:[self resultByIndexPath:indexPath]];
    NoteItem *noteItem = [[NoteItem alloc] initWithNote:@"" state:0];
    if ([self.noteItems objectForKey:[NSNumber numberWithInteger:index]]) {
        noteItem = [self.noteItems objectForKey:[NSNumber numberWithInteger:index]];
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *starView = nil;
    if (indexPath.section > 0) {
        NSInteger index = [self.results indexOfObject:[self resultByIndexPath:indexPath]];
        if ([self.markedItems containsObject:[NSNumber numberWithInteger:index]]) {
            starView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_star"]];
        }
        if ([self.skimmedItems containsObject:[NSNumber numberWithInteger:index]]) {
            [cell setBackgroundColor:[UIColor colorWithRed:.67 green:.85 blue:.91 alpha:1]];
        } else if ([self.readItems containsObject:[NSNumber numberWithInteger:index]]) {
            [cell setBackgroundColor:[UIColor colorWithRed:0.52 green:.80 blue:.99 alpha:1]];
        }
    }
    
    UIButton *stateView = nil;
    if (noteItem.state > 0) {
        stateView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        if (noteItem.state == 1) {
            [stateView setTitle:@"✕" forState:UIControlStateNormal];
            [stateView setBackgroundColor:[UIColor redColor]];
        } else if (noteItem.state == 2) {
            [stateView setTitle:@"✔︎" forState:UIControlStateNormal];
            [stateView setBackgroundColor:[UIColor colorWithRed:0.196 green:0.8 blue:0.196 alpha:1]];
        }
        [stateView.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [stateView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    cell.accessoryView = nil;
    if (stateView && starView) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
        starView.frame = CGRectMake(0, 0, 20, 20);
        stateView.frame = CGRectMake(30, 0, 20, 20);
        [view addSubview:starView];
        [view addSubview:stateView];
        cell.accessoryView = view;
    } else if (stateView) {
        cell.accessoryView = stateView;
    } else if (starView) {
        cell.accessoryView = starView;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Search Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    [cell addGestureRecognizer:lpgr];
    
    NSInteger index = [self.results indexOfObject:[self resultByIndexPath:indexPath]];
    
    NoteItem *noteItem = [[NoteItem alloc] initWithNote:@"" state:0];
    if ([self.noteItems objectForKey:[NSNumber numberWithInteger:index]]) {
        noteItem = [self.noteItems objectForKey:[NSNumber numberWithInteger:index]];
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
    if (indexPath.section > 0) {
        NSDictionary *result = [self resultByIndexPath:indexPath];
        NSString *title = [[NSString stringWithFormat:NSLocalizedString(@"Volume %1$i Page %2$i", nil), [result[@"volume"] intValue], [result[@"page"] intValue]] stringByReplacingThaiNumeric];
        cell.textLabel.text = noteItem.note.length ? [NSString stringWithFormat:@"%@ (%@)", title, noteItem.note] : title;
        cell.detailTextLabel.text = [self.dataModel volumeTitle:[result[@"volume"] intValue]];
    } else if (self.results.count > 0) {
        cell.textLabel.text = @[NSLocalizedString(@"Section 1", nil), NSLocalizedString(@"Section 2", nil), NSLocalizedString(@"Section 3", nil)][indexPath.row];
        cell.detailTextLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"Found %1$i pages", nil), _counts[indexPath.row]] stringByReplacingThaiNumeric];
    } else {
        cell.textLabel.text = self.keywords.length > 0 ? [NSString stringWithFormat:NSLocalizedString(@"Not found %1$@", nil), self.keywords] : nil;
        cell.detailTextLabel.text = self.keywords.length > 0 ? self.dataModel.title : nil;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.results.count == 0) { return nil; }
    
    if (indexPath.section == 0 && _counts[indexPath.row] > 0) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.row+1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } else if (indexPath.section > 0) {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[self resultByIndexPath:indexPath]];
        if (self.keywords) {
            [dict setObject:self.keywords forKey:@"keywords"];
        }

        [self saveReading:[[dict objectForKey:@"volume"] integerValue] page:[[dict objectForKey:@"page"] integerValue]];
        
        NSInteger volume = [[dict objectForKey:@"volume"] integerValue];
        NSInteger page = [[dict objectForKey:@"page"] integerValue] - [self.dataModel startPageOfVolume:volume] + 1;
        [dict setObject:[NSNumber numberWithInteger:page] forKey:@"page"];
        [dict setObject:[NSNumber numberWithInt:self.searchType] forKey:@"type"];
        [dict setObject:[NSNumber numberWithInteger:self.number] forKey:@"number"];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeAndPageNotification object:dict];
        
    }
    return indexPath;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    if (self.dataModel.isBuddhawajana) {
        [self chooseSearchType];
    } else {
        self.searchType = SearchTypeAll;
        NSString *searchText = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self doSearch:searchText];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.searchBar reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar hideAutoCompleteView];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == kActionSheetLongPress && buttonIndex == 1) {
        NSDictionary *dict = [self resultByIndexPath:self.selectedIndexPath];
        if (_marked) {
            [self saveMarking:[[dict objectForKey:@"volume"] integerValue] page:[[dict objectForKey:@"page"] integerValue]];
        } else {
            [self removeMarking:[[dict objectForKey:@"volume"] integerValue] page:[[dict objectForKey:@"page"] integerValue]];
        }
    } else if (actionSheet.tag == kActionSheetLongPress && buttonIndex == 0) {
        if (self.comparingMode) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Caution", nil)
                                                                                     message:NSLocalizedString(@"Can not use this function in comparing view", nil)
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:NSLocalizedString(@"Close", nil)
                                                               style:UIAlertActionStyleDefault
                                                             handler:nil];
            [alertController addAction:actionOk];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self presentViewController:alertController animated:YES completion:nil];
            });
            
        } else {
            [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[self resultByIndexPath:self.selectedIndexPath]];
            if (self.keywords) {
                [dict setObject:self.keywords forKey:@"keywords"];
            }
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"compare"];
            [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeAndPageThenCompareNotification object:dict];
            [self saveReading:[[dict objectForKey:@"volume"] integerValue] page:[[dict objectForKey:@"page"] integerValue]];
        }
    } else if (actionSheet.tag == kActionSheetLongPress && buttonIndex == 2) {
        [self performSegueWithIdentifier:@"Show Note" sender:self];
    } else if (actionSheet.tag == kActionSheetChooseSearchType && buttonIndex == 0) {
        self.searchType = SearchTypeAll;
        [self doSearch:self.searchBar.text];
    } else if (actionSheet.tag == kActionSheetChooseSearchType && buttonIndex == 1) {
        self.searchType = SearchTypeBuddhawaj;
        [self doSearch:self.searchBar.text];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - AutoCompleteSearchBarDataSource

- (NSString *)autoCompleteSearchBarStringCode
{
    return self.dataModel.codeString;
}

#pragma mark - AutoCompleteSearchBarDelegate

- (void)autoCompleteSearchBar:(AutoCompleteSearchBar *)searchBar didSelectSuggest:(NSString *)suggest
{
    [self doSearch:suggest];
}

@end
