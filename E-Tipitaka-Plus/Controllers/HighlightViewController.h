//
//  HighlightViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 6/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Types.h"

@class SQRichTextEditor;
@class HighlightViewController;

@protocol HighlightViewControllerDelegate <NSObject>

- (void)highlightViewController:(HighlightViewController *)controller didSaveWithSelection:(NSString *)selection withPosition:(NSInteger)position withNote:(NSString *)note withHtml:(NSString *)html inRange:(NSRange)range useType:(HighlightType)type withColor:(NSInteger)color;

@end

@interface HighlightViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIView *textView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, weak) IBOutlet UISegmentedControl *colorControl;
@property (nonatomic, weak) IBOutlet UIView *colorView;
@property (nonatomic, strong) NSString *selection;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, assign) NSRange range;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *html;
@property (nonatomic, assign) HighlightType type;
@property (nonatomic, assign) NSInteger color;
@property (nonatomic, weak) id<HighlightViewControllerDelegate>delegate;

@end
