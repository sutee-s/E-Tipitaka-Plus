//
//  ETViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETViewController.h"

@interface ETViewController ()

@end

@implementation ETViewController

@synthesize code = _code;
@synthesize dataModel = _dataModel;

- (void)setCode:(LanguageCode)code
{
    if (_code == code && self.dataModel) { return; }
    _dataModel = [SharedDataModel sharedDataModel:code];
    _code = code;
}

@end
