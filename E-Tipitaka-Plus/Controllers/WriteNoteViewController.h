//
//  WriteNoteViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "AdditionalNoteViewController.h"

@interface WriteNoteViewController : AdditionalNoteViewController

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, assign) BOOL main;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *snippet;

@end
