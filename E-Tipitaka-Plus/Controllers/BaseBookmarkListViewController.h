//
//  BaseBookmarkListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "ETViewController.h"

@interface BaseBookmarkListViewController : ETViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) NSMutableArray *bookmarks;

@end
