//
//  ServerDataTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "ServerDataTableViewController.h"
#import "AccountHelper.h"
#import <Unirest/UNIRest.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <AFNetworking/AFNetworking.h>
#import "UserDatabaseHelper.h"
#import "UserData.h"

#define kConfirmDeletingAlert       1000
#define kConfirmDownloadingAlert    1001
#define kConfirmImportingAlert      1002

@interface ServerDataTableViewController ()<UIActionSheetDelegate, UIAlertViewDelegate>

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSMutableArray *iosItems;
@property (nonatomic, strong) NSMutableArray *androidItems;
@property (nonatomic, strong) NSMutableArray *pcItems;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) NSOperationQueue *deletingQueue;

@end

@implementation ServerDataTableViewController

@synthesize iosItems = _iosItems;
@synthesize androidItems = _androidItems;
@synthesize pcItems = _pcItems;
@synthesize deletingQueue = _deletingQueue;

- (NSOperationQueue *)deletingQueue
{
    if (_deletingQueue) {
        _deletingQueue = [[NSOperationQueue alloc] init];
        _deletingQueue.maxConcurrentOperationCount = 1;
    }
    return _deletingQueue;
}

- (NSMutableArray *)iosItems
{
    if (!_iosItems) {
        _iosItems = [[NSMutableArray alloc] init];
    }
    return _iosItems;
}

- (NSMutableArray *)androidItems
{
    if (!_androidItems) {
        _androidItems = [[NSMutableArray alloc] init];
    }
    return _androidItems;
}

- (NSMutableArray *)pcItems
{
    if (!_pcItems) {
        _pcItems = [[NSMutableArray alloc] init];
    }
    return _pcItems;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.isLoading = NO;
    self.navigationItem.title = NSLocalizedString(@"Server data", nil);
    if ([AccountHelper currentUsername]) {
        self.accountLabel.text = [NSString stringWithFormat:@"Logged in : %@", [AccountHelper currentUsername]];
        [self refresh:nil];
    } else {
        self.accountLabel.text = @"No account";
    }
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)parseItems:(NSArray *)items
{
    if (![AccountHelper currentUsername]) {
        return;
    }
    
    for (NSDictionary *item in items) {
        NSString *platform = item[@"fields"][@"platform"];
        NSString *path = item[@"fields"][@"file"];
        NSNumber *gid = item[@"pk"];        
        [UserDatabaseHelper saveUserData:[AccountHelper currentUsername] path:path platform:platform gid:gid];
    }
    
    [self reloadData];
}

- (void)deleteItems:(NSArray *)items
{
    for (NSDictionary *item in items) {
        NSNumber *gid = item[@"pk"];
        [UserDatabaseHelper deleteUserDataByGid:gid.integerValue];
    }

    [self reloadData];
}

- (void)reloadData
{
    if (![AccountHelper currentUsername]) {
        return;
    }
    
    self.iosItems = [UserDatabaseHelper getUserDataItems:[AccountHelper currentUsername] platform:@"ios"];
    self.androidItems = [UserDatabaseHelper getUserDataItems:[AccountHelper currentUsername] platform:@"android"];
    self.pcItems = [UserDatabaseHelper getUserDataItems:[AccountHelper currentUsername] platform:@"pc"];
    
    [self.tableView reloadData];
}

- (void)checkDeletedInServer
{
    if (![AccountHelper currentToken] || self.isLoading) {
        return;
    }
    
    NSURL *baseURL = [NSURL URLWithString:USER_DATA_HOST];
    
    self.isLoading = YES;
    self.refreshButtonItem.enabled = NO;
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    [manager GET:@"user_data_list/?deleted=true" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject) {
            NSData *jsonData = [responseObject[@"items"] dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error;
            NSArray *items = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            [self deleteItems:items];
        }
        self.isLoading = NO;
        self.refreshButtonItem.enabled = YES;
        [self checkDeletingItem];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        self.isLoading = NO;
        self.refreshButtonItem.enabled = YES;
        NSLog(@"%@", error);
    }];
    
}


- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)refresh:(id)sender
{
    if (![AccountHelper currentToken] || self.isLoading) {
        return;
    }
        
    NSURL *baseURL = [NSURL URLWithString:USER_DATA_HOST];
    
    self.isLoading = YES;
    self.refreshButtonItem.enabled = NO;
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    [manager GET:@"user_data_list/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject) {
            NSData *jsonData = [responseObject[@"items"] dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error;
            NSArray *items = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            [self parseItems:items];
        }
        self.isLoading = NO;
        self.refreshButtonItem.enabled = YES;
        [self checkDeletedInServer];
        [self checkDeletingItem];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        self.isLoading = NO;
        self.refreshButtonItem.enabled = YES;
        NSLog(@"%@", error);
    }];
    
}

- (NSArray *)selectItemsInSection:(NSIndexPath *)indexPath
{
    NSArray *items = @[];
    if (indexPath.section == 0) {
        items = self.iosItems;
    } else if (indexPath.section == 1) {
        items = self.androidItems;
    } else if (indexPath.section == 2) {
        items = self.pcItems;
    }
    return items;
}

- (UserData *)selectUserData:(NSIndexPath *)indexPath
{
    NSArray *items = [self selectItemsInSection:indexPath];
    return items[indexPath.row];
}

- (void)confirmDelete
{
    UserData *userData = [self selectUserData:self.selectedIndexPath];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:userData.path
                                                    message:NSLocalizedString(@"Do you want to delete this item?", nil)
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    alert.tag = kConfirmDeletingAlert;
    [alert show];
}

- (void)confirmDownload
{
   UserData *userData = [self selectUserData:self.selectedIndexPath];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:userData.path
                                                    message:NSLocalizedString(@"Do you want to download this item?", nil)
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    alert.tag = kConfirmDownloadingAlert;
    [alert show];
}

- (void)confirmImport
{
    UserData *userData = [self selectUserData:self.selectedIndexPath];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:userData.path
                                                    message:NSLocalizedString(@"Do you want to import this item?", nil)
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    alert.tag = kConfirmImportingAlert;
    [alert show];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
        NSProgress *progress = (NSProgress *)object;
        NSLog(@"Progress is %f", progress.fractionCompleted);
        [SVProgressHUD showProgress:progress.fractionCompleted];
    }
}

- (void)import
{
    if (![AccountHelper currentToken]) {
        return;
    }
    
    UserData *userData = [self selectUserData:self.selectedIndexPath];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user_data/%@/", USER_DATA_HOST, userData.gid]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSURL *tempDir = [documentsDirectoryURL URLByAppendingPathComponent:@"temp"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:tempDir.path]) {
            [[NSFileManager defaultManager] createDirectoryAtURL:tempDir withIntermediateDirectories:YES attributes:nil error:nil];
        }        
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"temp/%@", [response suggestedFilename]]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        [UserDatabaseHelper importUserDatabase:filePath];
    }];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    
    [downloadTask resume];
}

- (void)download
{
    if (![AccountHelper currentToken]) {
        return;
    }
    
    UserData *userData = [self selectUserData:self.selectedIndexPath];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user_data/%@/", USER_DATA_HOST, userData.gid]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    NSProgress *progress;
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:&progress destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"export/%@", [response suggestedFilename]]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        [SVProgressHUD dismiss];
    }];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showProgress:0.0f];

    [progress addObserver:self
               forKeyPath:@"fractionCompleted"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
    [downloadTask resume];
}

- (void)delete
{
    UserData *userData = [self selectUserData:self.selectedIndexPath];
    [UserDatabaseHelper updateUserDataAtColumn:@"deleting" setValue:[NSNumber numberWithBool:YES] byGid:userData.gid.integerValue];
    [self checkDeletingItem];
    [self reloadData];
}

- (void)deleteUserData:(UserData *)userData
{
    if (![AccountHelper currentToken]) {
        return;
    }
    
    NSURL *baseURL = [NSURL URLWithString:USER_DATA_HOST];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    [manager DELETE:[NSString stringWithFormat:@"user_data/%@/", userData.gid] parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [UserDatabaseHelper deleteUserDataByGid:userData.gid.integerValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadData];
            [self checkDeletingItem];
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];

}

- (void)checkDeletingItem
{
    if (![AccountHelper currentUsername]) {
        return;
    }
    
    NSArray *items = [UserDatabaseHelper getDeletingUserDataItems:[AccountHelper currentUsername]];
    for (UserData *userData in items) {
        [self deleteUserData:userData];
        break;
    }
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kConfirmDownloadingAlert && buttonIndex == 1) {
        [self download];
    } else if (alertView.tag == kConfirmDeletingAlert & buttonIndex == 1) {
        [self delete];
    } else if (alertView.tag == kConfirmImportingAlert && buttonIndex == 1) {
        [self import];
    }
}

#pragma mark - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self confirmDelete];
    } else if (buttonIndex == 1) {
        [self confirmDownload];
    } else if (buttonIndex == 2) {
        [self confirmImport];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    UserData *userData = [self selectUserData:indexPath];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:userData.path delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Delete", nil) otherButtonTitles:NSLocalizedString(@"Download data", nil), NSLocalizedString(@"Import data", nil), nil];
    [actionSheet showInView:self.view];

}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserData *userData = [self selectUserData:indexPath];
    if (userData.deleting) {
        return nil;
    }
    return indexPath;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.iosItems.count;
    } else if (section == 1) {
        return self.androidItems.count;
    } else if (section == 2) {
        return self.pcItems.count;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"iOS";
    } else if (section == 1) {
        return @"Android";
    } else if (section == 2) {
        return @"PC";
    }
    return nil;
}    

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"UserDataCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSArray *items = [self selectItemsInSection:indexPath];
    
    UserData *userData = items[indexPath.row];
    cell.textLabel.text = userData.path;
    
    if (userData.deleting) {
        cell.textLabel.textColor = [UIColor lightGrayColor];
    } else {
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    return cell;
}



@end
