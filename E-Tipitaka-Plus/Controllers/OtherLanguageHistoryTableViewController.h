//
//  OtherLanguageHistoryTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherLanguageTableViewController.h"

@interface OtherLanguageHistoryTableViewController : OtherLanguageTableViewController

@end
