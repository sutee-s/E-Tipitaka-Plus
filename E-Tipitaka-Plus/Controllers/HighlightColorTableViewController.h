//
//  HighlightColorTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightColorTableViewController : UITableViewController

@end
