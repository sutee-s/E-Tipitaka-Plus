//
//  HighlightColorEditTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 18/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "HighlightColorEditTableViewController.h"
#import "UIColor+HexHTMLString.h"

@interface HighlightColorEditTableViewController ()<UIColorPickerViewControllerDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation HighlightColorEditTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.color.name;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)pickColorForHighlight
{
  UIColorPickerViewController *picker = [[UIColorPickerViewController alloc] init];
  picker.supportsAlpha = NO;
  picker.delegate = self;
  picker.popoverPresentationController.sourceView = self.tableView;
  picker.popoverPresentationController.sourceRect = [self.tableView rectForRowAtIndexPath:self.selectedIndexPath];
  picker.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
  if (self.selectedIndexPath.section == 0) {
    picker.selectedColor = [UIColor pxColorWithHexValue:self.color.inlineColors[self.selectedIndexPath.row]];
  } else if (self.selectedIndexPath.section == 1) {
    picker.selectedColor = [UIColor pxColorWithHexValue:self.color.footerColors[self.selectedIndexPath.row]];
  } else if (self.selectedIndexPath.section == 2) {
    picker.selectedColor = [UIColor pxColorWithHexValue:self.color.noteColors[self.selectedIndexPath.row]];
  }
  [self presentViewController:picker animated:YES completion:nil];
}

- (void)colorPickerViewControllerDidFinish:(UIColorPickerViewController *)viewController
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)colorPickerViewControllerDidSelectColor:(UIColorPickerViewController *)viewController
{
  UIColor *color = viewController.selectedColor;
  if (self.selectedIndexPath.section == 0) {
      [self.color.inlineColors replaceObjectAtIndex:self.selectedIndexPath.row withObject:color.hexHTMLString];
  } else if (self.selectedIndexPath.section == 1) {
      [self.color.footerColors replaceObjectAtIndex:self.selectedIndexPath.row withObject:color.hexHTMLString];
  } else if (self.selectedIndexPath.section == 2) {
      [self.color.noteColors replaceObjectAtIndex:self.selectedIndexPath.row withObject:color.hexHTMLString];
  }
  [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
  [self.delegate HighlightColorEditTableViewController:self didChangeColor:self.color];
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Description", nil);
    } else if (section == 1) {
      return NSLocalizedString(@"Footnote", nil);
    }
    return NSLocalizedString(@"Note", nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HighlightColorEditCell" forIndexPath:indexPath];
    
    if (indexPath.section == 0) { // inline colors
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 1", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.inlineColors[0]];
                break;
            case 1:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 2", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.inlineColors[1]];
                break;
            case 2:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 3", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.inlineColors[2]];
                break;
            case 3:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 4", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.inlineColors[3]];
                break;
            case 4:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 5", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.inlineColors[4]];
                break;
            default:
                break;
        }
    } else if (indexPath.section == 1) { // footer colors
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 1", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.footerColors[0]];
                break;
            case 1:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 2", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.footerColors[1]];
                break;
            case 2:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 3", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.footerColors[2]];
                break;
            case 3:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 4", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.footerColors[3]];
                break;
            case 4:
                cell.textLabel.text = NSLocalizedString(@"Highlight Color 5", nil);
                cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.footerColors[4]];
                break;
            default:
                break;
        }
    } else if (indexPath.section == 2) { // note colors
      switch (indexPath.row) {
          case 0:
              cell.textLabel.text = NSLocalizedString(@"Highlight Color 1", nil);
              cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.noteColors[0]];
              break;
          case 1:
              cell.textLabel.text = NSLocalizedString(@"Highlight Color 2", nil);
              cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.noteColors[1]];
              break;
          case 2:
              cell.textLabel.text = NSLocalizedString(@"Highlight Color 3", nil);
              cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.noteColors[2]];
              break;
          case 3:
              cell.textLabel.text = NSLocalizedString(@"Highlight Color 4", nil);
              cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.noteColors[3]];
              break;
          case 4:
              cell.textLabel.text = NSLocalizedString(@"Highlight Color 5", nil);
              cell.backgroundColor = [UIColor pxColorWithHexValue:self.color.noteColors[4]];
              break;
          default:
              break;
      }
  }
    
    return cell;
}

#pragma mark - Table view data delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    [self pickColorForHighlight];
    return nil;
}

#pragma mark - Private

- (void)ms_dismissViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
