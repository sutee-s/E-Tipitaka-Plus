//
//  TagBookmarkListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "TagBookmarkListViewController.h"
#import "UserDatabaseHelper.h"
#import "Bookmark.h"
#import "UIView+Snapshot.h"
#import "UITableView+LongPressMove.h"

#define kConfirmDeletingAlert       1000

@interface TagBookmarkListViewController()<UIAlertViewDelegate, UIGestureRecognizerDelegate, LongPressMoveDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation TagBookmarkListViewController

@synthesize tag = _tag;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.code = self.tag.code;
    self.navigationItem.title = self.tag.name;
    self.tableView.longPressMoveDelegate = self;
    [self.tableView enableLongPressMove:1];
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Bookmark", nil);
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.tag.note exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [UserDatabaseHelper updateTagItems:@"note" items:self.tag.note ofTag:self.tag.name withCode:self.tag.code];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        self.selectedIndexPath = indexPath;
        [self confirmDelete];
    }
}


- (void)reloadData
{
    self.bookmarks = [[UserDatabaseHelper queryBookmarksInRowIds:self.tag.note withCode:self.tag.code] mutableCopy];
    [self.tableView reloadData];
}

- (void)confirmDelete
{
    Bookmark *bookmark = [self.bookmarks objectAtIndex:self.selectedIndexPath.row];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:bookmark.note
                                                    message:NSLocalizedString(@"Do you want to remove the item from this tag?", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"No", nil)
                                          otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
    alert.tag = kConfirmDeletingAlert;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kConfirmDeletingAlert && buttonIndex == 1) {
        Bookmark *bookmark = [self.bookmarks objectAtIndex:self.selectedIndexPath.row];
        [self.bookmarks removeObjectAtIndex:self.selectedIndexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tag.note removeObject:[NSNumber numberWithInteger:bookmark.rowId]];
        [UserDatabaseHelper updateTagItems:@"note" items:self.tag.note ofTag:self.tag.name withCode:self.tag.code];
    }
}

@end
