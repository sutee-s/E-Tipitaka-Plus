//
//  BookListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "ETViewController.h"

@interface BookListViewController : ETViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
