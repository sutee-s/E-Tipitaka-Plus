//
//  WriteNoteListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WriteNoteListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingOrderSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingTypeSegmentedControl;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@end
