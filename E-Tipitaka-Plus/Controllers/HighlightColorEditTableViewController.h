//
//  HighlightColorEditTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 18/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HighlightColor.h"

@class HighlightColorEditTableViewController;

@protocol HighlightColorEditTableViewControllerDelegate <NSObject>

- (void)HighlightColorEditTableViewController:(HighlightColorEditTableViewController *)controler didChangeColor:(HighlightColor *)color;

@end

@interface HighlightColorEditTableViewController : UITableViewController

@property (nonatomic, strong) HighlightColor *color;
@property (nonatomic, weak) id<HighlightColorEditTableViewControllerDelegate> delegate;

@end
