//
//  WriteNoteListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "WriteNoteListViewController.h"
#import "WriteNoteViewController.h"
#import "WriteNote.h"
#import "UserDatabaseHelper.h"
#import "Constants.h"
#import "NSString+Thai.h"

@interface WriteNoteListViewController ()<AdditionalNoteViewControllerDelegate, UIScrollViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray *notes;
@property (nonatomic, strong) WriteNoteViewController *writeNoteViewController;

@end

@implementation WriteNoteListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)refresh
{
    self.notes = [[UserDatabaseHelper queryNotes:self.searchBar.text] mutableCopy];
    [self sort:nil];
}

- (IBAction)sort:(id)sender
{
    self.notes = [[self.notes sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        WriteNote *note1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        WriteNote *note2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        return [note1.created compare:note2.created];
    }] mutableCopy];
    
    self.notes = [[self.notes sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        WriteNote *note1 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj2 : obj1;
        WriteNote *note2 = self.sortingOrderSegmentedControl.selectedSegmentIndex == 0 ? obj1 : obj2;
        switch (self.sortingTypeSegmentedControl.selectedSegmentIndex) {
            case 0:
                return [note1.plainNote compare:note2.plainNote];
            case 1:
                return [note1.created compare:note2.created];
            case 2:
            default:
                return [[NSNumber numberWithBool:note1.starred] compare:[NSNumber numberWithBool:note2.starred]];
        }
    }] mutableCopy];
    
    NSInteger mainPosition = -1;
    for (NSInteger i = 0; i < self.notes.count; i++) {
        WriteNote *note = [self.notes objectAtIndex:i];
        if (note.main) {
            mainPosition = i;
            break;
        }
    }
    
    if (mainPosition > 0) {
        WriteNote *mainNote = [self.notes objectAtIndex:mainPosition];
        [self.notes removeObjectAtIndex:mainPosition];
        [self.notes insertObject:mainNote atIndex:0];
    }
    
    [self.tableView reloadData];
}

- (void)showEditDialog:(WriteNote *)note
{
    self.writeNoteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WriteNoteViewController"];
    self.writeNoteViewController.delegate = self;
    self.writeNoteViewController.rowId = note.rowId;
    self.writeNoteViewController.code = LanguageCodeThaiSiam;
    self.writeNoteViewController.volume = 1;
    self.writeNoteViewController.page = 1;
    self.writeNoteViewController.standalone = NO;
    self.writeNoteViewController.snippet = note.snippet;
    
    if (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.writeNoteViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    } else {
        self.writeNoteViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    
    [self presentViewController:self.writeNoteViewController animated:YES completion:^{
    }];
}

- (void)additionalNoteViewControllerDidDismiss:(AdditionalNoteViewController *)controller
{
    [self refresh];
}

- (void)additionalNoteViewController:(AdditionalNoteViewController *)controller changePage:(PageInfo *)pageInfo
{
    [self.mm_drawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabasAndVolumeAndPageNotification object:pageInfo];
    }];    
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        WriteNote *note = [self.notes objectAtIndex:indexPath.row];
        [UserDatabaseHelper deleteWriteNote:note.rowId];
        [self refresh];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Note Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    WriteNote *note = [self.notes objectAtIndex:indexPath.row];
  
  cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:note.snippet attributes:@{NSFontAttributeName: [UIFont fontWithName:@"THSarabunNew" size:NOTE_FONT_SIZE]}];

  cell.accessoryView = note.starred ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_star"]] : nil;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showEditDialog:[self.notes objectAtIndex:indexPath.row]];
    return indexPath;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refresh];
}
@end
