//
//  NoteHistoryViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/9/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "History.h"

@interface NoteHistoryViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextView *noteTextView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, assign) NSInteger rowId;

- (NSString *)note;
- (NSInteger)noteState;
- (void)updateNote;

@end
