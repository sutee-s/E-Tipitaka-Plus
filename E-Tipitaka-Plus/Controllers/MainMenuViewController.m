//
//  MainMenuViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 24/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "MainMenuViewController.h"
#import "PageInfo.h"
#import "DictDatabaseHelper.h"
#import "SyncDatabaseManager.h"
#import "BookDatabaseHelper.h"
#import "DownloadDatabaseHelper.h"
#import "SharedDataModel.h"

@interface MainMenuViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong, readonly) NSArray *bookMenuTable;

@end

@implementation MainMenuViewController

@synthesize selectedIndexPath = _selectedIndexPath;
@synthesize bookMenuTable = _bookMenuTable;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSelectedIndex:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSelectedIndex:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSelectedIndex:) name:StartDatabaseNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    self.title = NSLocalizedString(@"Main menu", nil);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:self.selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)changeSelectedIndex:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        int code = [notification.object intValue];
        NSInteger position = [BOOK_POSITION_INDEXES indexOfObject:[NSNumber numberWithInt:code]];
        self.selectedIndexPath = [NSIndexPath indexPathForItem:position inSection:0];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        int code = ((PageInfo *)notification.object).code;
        NSInteger position = [BOOK_POSITION_INDEXES indexOfObject:[NSNumber numberWithInt:code]];
        self.selectedIndexPath = [NSIndexPath indexPathForItem:position inSection:0];
    }
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Books", nil);
    } else if (section == 1) {
        return NSLocalizedString(@"Dictionary", nil);
    } else if (section == 2) {
        return NSLocalizedString(@"Tools", nil);
    } else if (section == 3) {
        return NSLocalizedString(@"Version", nil);
    }
    return @"";
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.bookMenuTable.count;
    } else if (section == 1) {
        return 3;
    } else if (section == 2) {
        return 5;
    } else if (section == 3) {
        return 1;
    }
    return 0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Book Cell";
    
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"Book Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (NSArray *)bookMenuTable {
    if (!_bookMenuTable) {
        _bookMenuTable = [NSArray arrayWithObjects:
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Thai Royal Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiSiam], @"code", nil],
                      
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Pali Siam Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodePaliSiam], @"code", nil],

                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"New Pali Siam Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodePaliSiamNew], @"code", nil],

                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Buddhawajana Tipitaka", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiWatna], @"code", nil],
                      
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Thai Maha Makut Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiMahaMakut], @"code", nil],
                      
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Thai Maha Chula Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiMahaChula], @"code", nil],

                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Thai Maha Chula 2 Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiMahaChula2], @"code", nil],

                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Thai Supreme Menu", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiSupreme], @"code", nil],
                      
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Buddhawajana Pocket Book", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiPocketBook], @"code", nil],
                      
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Five books from Buddha speech", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiFiveBooks], @"code", nil],
                      
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Ariyavinaya", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeThaiVinaya], @"code", nil],
                          
                      [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Roman Script", nil), @"title",
                       [NSNumber numberWithInt:LanguageCodeRomanScript], @"code", nil],
                      
                      nil];
    }
    return _bookMenuTable;
}

-(void) askForDownloadingDatabase:(LanguageCode)code
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Database not found", nil) message:NSLocalizedString(@"Do you want to re-download this database?", nil) preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *codeString = [SharedDataModel sharedDataModel:code].codeString;
        [[[DownloadDatabaseHelper sharedInstance] updateDatabaseWithCode:codeString] continueWithSuccessBlock:^id(BFTask *task) {
            [[DownloadDatabaseHelper sharedInstance] forceDismissHUD];                        
            return nil;
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.accessoryView = nil;

    if (indexPath.section == 0) {
        cell.textLabel.text = self.bookMenuTable[indexPath.row][@"title"];
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"Pali-Thai dictionary", nil);
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"Thai dictionary", nil);
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        cell.textLabel.text = NSLocalizedString(@"Pali-English dictionary", nil);
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"User data management", nil);
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"See others data", nil);
    } else if (indexPath.section == 2 && indexPath.row == 2) {
        cell.textLabel.text = NSLocalizedString(@"Highlight color configuration", nil);
    } else if (indexPath.section == 2 && indexPath.row == 3) {
        cell.textLabel.text = NSLocalizedString(@"Connect to server", nil);
    } else if (indexPath.section == 2 && indexPath.row == 4) {
        UIButton *stateView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        if ([SyncDatabaseManager sharedInstance].syncCompleted) {
            [stateView setTitle:@"✔︎" forState:UIControlStateNormal];
            [stateView setBackgroundColor:[UIColor colorWithRed:0.196 green:0.8 blue:0.196 alpha:1]];
        } else {
            [stateView setTitle:@"✕" forState:UIControlStateNormal];
            [stateView setBackgroundColor:[UIColor redColor]];
        }
        cell.accessoryView = stateView;
        cell.textLabel.text = NSLocalizedString(@"Sync user data", nil);
    } else if (indexPath.section == 3 && indexPath.row == 0) {
        NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", appVersionString, appBuildString];
    }
}

#pragma mark - UITableViewDelegate 

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        LanguageCode code = [self.bookMenuTable[indexPath.row][@"code"] intValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabaseNotification object:[NSNumber numberWithInt:code]];
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OpenPaliDictionaryNotification object:[NSNumber numberWithInt:DictionaryTypePali]];
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OpenThaiDictionaryNotification object:[NSNumber numberWithInt:DictionaryTypeThai]];
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OpenEnglishDictionaryNotification object:[NSNumber numberWithInt:DictionaryTypeEnglish]];
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        [self performSegueWithIdentifier:@"User Data Management" sender:self];
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        [self performSegueWithIdentifier:@"Others Data" sender:self.self];
    } else if (indexPath.section == 2 && indexPath.row == 2) {
        [self performSegueWithIdentifier:@"Highlight Color" sender:self];
    } else if (indexPath.section == 2 && indexPath.row == 3) {
        [self performSegueWithIdentifier:@"Connect to Server" sender:self];
    }
    self.selectedIndexPath = indexPath;
    
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3 && indexPath.row == 0) {
        return nil;
    } else if (indexPath.section == 2 && indexPath.row == 4) {
        if (AUTO_SYNC) {
            [[SyncDatabaseManager sharedInstance] syncWithProgressHUD:^(BOOL success) {
                [self.tableView reloadData];
            }];
        } else {
            [[SyncDatabaseManager sharedInstance] syncManuallyWithProgressHUD:^(BOOL success) {
                [self.tableView reloadData];
            } inViewController:self];
        }
        return nil;
    } else if (indexPath.section == 0) {
        LanguageCode code = [self.bookMenuTable[indexPath.row][@"code"] intValue];
        if (![BookDatabaseHelper checkDatabaseAvailability:code]) {
            [self askForDownloadingDatabase:code];
            return nil;
        }
    }
    return indexPath;
}
@end
