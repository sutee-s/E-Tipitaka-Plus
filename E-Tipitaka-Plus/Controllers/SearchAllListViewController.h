//
//  SearchAllListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "BaseHistoryListViewController.h"
#import "SearchAllHistory.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchAllListViewController : BaseHistoryListViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingOrderSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingTypeSegmentedControl;

@property (nonatomic, strong) SearchAllHistory *activeSearchAllHistory;
@property (nonatomic, strong) SearchAllHistory *selectedSearchAllHistory;

@end

NS_ASSUME_NONNULL_END
