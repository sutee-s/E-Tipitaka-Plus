//
//  AdditionalNoteViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "ETViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "PageInfo.h"

@class AdditionalNoteViewController;

@protocol AdditionalNoteViewControllerDelegate <NSObject>

- (void)additionalNoteViewControllerDidDismiss:(AdditionalNoteViewController *)controller;
- (void)additionalNoteViewController:(AdditionalNoteViewController *)controller changePage:(PageInfo *)pageInfo;

@end

@interface AdditionalNoteViewController : ETViewController

@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL standalone;
@property (nonatomic, assign) BOOL firstEdit;

@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;
@property (nonatomic, weak) IBOutlet UIView *highlightToolBar;
@property (nonatomic, weak) IBOutlet UIView *colorView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *colorControl;

@property (nonatomic, weak) IBOutlet id<AdditionalNoteViewControllerDelegate> delegate;

- (NSString *)note;
- (BOOL)starred;
- (void)save:(NSString*)text starred:(BOOL)starred;
- (void)markStar:(BOOL)value;


@end
