//
//  HighlightNoteViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/8/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Highlight.h"

@class HighlightNoteViewController;

@protocol HighlightNoteViewControllerDelegate <NSObject>

- (void)highlightNoteViewController:(HighlightNoteViewController *)controller didChangeHighlight:(Highlight *)highlight;

@end

@interface HighlightNoteViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, strong) Highlight *highlight;
@property (nonatomic, weak) id<HighlightNoteViewControllerDelegate> delegate;


@end
