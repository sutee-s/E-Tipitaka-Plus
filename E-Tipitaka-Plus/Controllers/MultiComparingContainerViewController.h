//
//  MultiComparingContainerViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageInfo.h"

@interface MultiComparingContainerViewController : UIViewController

@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, assign) SearchType searchType;
@property (nonatomic, assign) BOOL switchOn;
@property (nonatomic, strong) PageInfo *firstPageInfo;
@property (nonatomic, strong) PageInfo *secondPageInfo;

@end
