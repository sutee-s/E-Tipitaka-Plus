//
//  ReadingStateViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 12/23/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "ReadingStateViewController.h"
#import "PageInfo.h"
#import "UserDatabaseHelper.h"
#import "ReadingState.h"
#import "NSString+Thai.h"
#import "ReadingStateDetailTableViewController.h"

@interface ReadingStateViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ReadingStateViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:StartDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
        self.code = LanguageCodeThaiSiam;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeCode:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSNumber class]]) {
        self.code = [notification.object intValue];
        [self reloadData];
    } else if ([notification.object isKindOfClass:[PageInfo class]]) {
        [self reloadData];
        self.code = ((PageInfo *)notification.object).code;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Reading state", nil), self.dataModel.shortTitle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = [NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Reading state", nil), self.dataModel.shortTitle];
    [self.tableView reloadData];
}

- (void)reloadData
{
    [self.tableView reloadData];
}

- (IBAction)changeType:(id)sender
{
    [self reloadData];
}

- (NSInteger)volumeFromIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = 1;
    if (indexPath.section == 1) {
        index += [self.dataModel totalVolumesInSection:1];
    } else if (indexPath.section == 2) {
        index += [self.dataModel totalVolumesInSection:1] + [self.dataModel totalVolumesInSection:2];
    }
    return index + indexPath.row;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataModel totalVolumesInSection:section+1];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataModel.totalSections;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSInteger totalPages = 0;
    NSInteger start = 0, end = 0;
    NSString *prefix = @"";
    if (!self.dataModel.isTipitaka) {
        start = 1;
        end = [self.dataModel totalVolumes];
    } else {
        switch (section) {
            case 0:
                start = 1;
                end = [self.dataModel totalVolumesInSection:1];
                prefix = NSLocalizedString(@"Section 1 (short)", nil);
                break;
            case 1:
                start = [self.dataModel totalVolumesInSection:1] + 1;
                end = start + [self.dataModel totalVolumesInSection:2];
                prefix = NSLocalizedString(@"Section 2 (short)", nil);
                break;
            case 2:
                start = [self.dataModel totalVolumesInSection:1] + [self.dataModel totalVolumesInSection:2] + 1;
                end = start + [self.dataModel totalVolumesInSection:3];
                prefix = NSLocalizedString(@"Section 3 (short)", nil);
                break;
            default:
                break;
        }
    }
    for (NSInteger i=start; i<=end; ++i) {
        totalPages += [self.dataModel totalPagesOfVolume:i];
    }
    
    NSInteger count = 0;
    NSString *stateString = @"";
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        count = [UserDatabaseHelper countReadingStates:ReadingStateTypeRead code:self.dataModel.code fromVolume:start toVolume:end];
        stateString = NSLocalizedString(@"Read", nil);
    } else if (self.segmentedControl.selectedSegmentIndex == 1) {
        count = [UserDatabaseHelper countReadingStates:ReadingStateTypeSkimmed code:self.dataModel.code fromVolume:start toVolume:end];
        stateString = NSLocalizedString(@"Skimmed", nil);
    } else if (self.segmentedControl.selectedSegmentIndex == 2) {
        count = totalPages;
        count -= [UserDatabaseHelper countReadingStates:ReadingStateTypeRead code:self.dataModel.code fromVolume:start toVolume:end];
        count -= [UserDatabaseHelper countReadingStates:ReadingStateTypeSkimmed code:self.dataModel.code];
        stateString = NSLocalizedString(@"Unread", nil);
    }
    
    NSString *title = [NSString stringWithFormat:@"%@%@ %@/%@ %@",
            prefix,
            stateString,
            [NSNumber numberWithInteger:count],
            [NSNumber numberWithInteger:totalPages],
            NSLocalizedString(@"page", nil)];
    
    return [title stringByReplacingThaiNumeric];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"ReadingStateCell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSInteger volume = [self volumeFromIndexPath:indexPath];
    
    NSInteger count = 0;
    NSString *stateString = @"";
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        count = [UserDatabaseHelper countReadingStates:ReadingStateTypeRead code:self.dataModel.code volume:volume];
        stateString = NSLocalizedString(@"Read", nil);
    } else if (self.segmentedControl.selectedSegmentIndex == 1) {
        count = [UserDatabaseHelper countReadingStates:ReadingStateTypeSkimmed code:self.dataModel.code volume:volume];
        stateString = NSLocalizedString(@"Skimmed", nil);
    } else if (self.segmentedControl.selectedSegmentIndex == 2) {
        count = [self.dataModel totalPagesOfVolume:volume] - [UserDatabaseHelper countReadingStatesWithCode:self.dataModel.code andVolume:volume];
        stateString = NSLocalizedString(@"Unread", nil);
    }
    
    NSString* text = [NSString stringWithFormat:@"%@ %@ %@ %@%@%@ %@",
                      NSLocalizedString(@"Volume", nil),
                      [NSNumber numberWithInteger:volume],
                      stateString,
                      [NSNumber numberWithInteger:count],
                      NSLocalizedString(@"/", nil),
                      [NSNumber numberWithInteger:[self.dataModel totalPagesOfVolume:volume]],
                      NSLocalizedString(@"page", nil)
                      ];
    
    cell.textLabel.text = [text stringByReplacingThaiNumeric];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Show Detail" sender:indexPath];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Show Detail"]) {
        ReadingStateDetailTableViewController *controller = segue.destinationViewController;
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            controller.customTitle = [NSString stringWithFormat:@"%@ - %@", self.dataModel.shortTitle, NSLocalizedString(@"Read", nil)];
            controller.state = ReadingStateTypeRead;
        } else if (self.segmentedControl.selectedSegmentIndex == 1) {
            controller.customTitle = [NSString stringWithFormat:@"%@ - %@", self.dataModel.shortTitle, NSLocalizedString(@"Skimmed", nil)];
            controller.state = ReadingStateTypeSkimmed;
        } else if (self.segmentedControl.selectedSegmentIndex == 2) {
            controller.customTitle = [NSString stringWithFormat:@"%@ - %@", self.dataModel.shortTitle, NSLocalizedString(@"Unread", nil)];
            controller.state = ReadingStateTypeUnread;
        }
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        NSInteger volume = [self.dataModel convertVolume:indexPath.row+1 inSection:indexPath.section+1];
        controller.code = self.dataModel.code;
        controller.volume = volume;
        controller.totalPages = [self.dataModel totalPagesOfVolume:volume];
    }
}


@end
