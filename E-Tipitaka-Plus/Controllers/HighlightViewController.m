//
//  HighlightViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 6/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "HighlightViewController.h"
#import <KNSemiModalViewController/UIViewController+KNSemiModal.h>
#import "HighlightColorList.h"
#import "UIColor+HexHTMLString.h"
#import "ZSSRichTextEditor.h"
#import "E_Tipitaka_Plus-Swift.h"


@interface HighlightViewController ()

@property (nonatomic, assign) NSInteger keyboardHeight;
@property (nonatomic, assign) NSInteger originViewHeight;
@property (nonatomic, strong) HighlightColor *highlightColor;
@property (nonatomic, strong) ZSSRichTextEditor *textEditor;

@end

@implementation HighlightViewController

@synthesize textView = _textView;
@synthesize titleLabel = _titleLabel;
@synthesize selection = _selection;
@synthesize position = _position;
@synthesize note = _note;
@synthesize type = _type;
@synthesize segmentedControl = _segmentedControl;
@synthesize delegate = _delegate;
@synthesize textEditor = _textEditor;


- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    NSValue* value = info[UIKeyboardFrameEndUserInfoKey];
    CGRect rawFrame = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    self.keyboardHeight = keyboardFrame.size.height;
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.originViewHeight-self.keyboardHeight);
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height+self.keyboardHeight);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textEditor = [[ZSSRichTextEditor alloc] init];
    self.textEditor.enabledToolbarItems = @[ZSSRichTextEditorToolbarNone];
    self.textEditor.alwaysShowToolbar = YES;
    self.highlightColor = [HighlightColorList getCurrentHighlightColor];
    self.titleLabel.text = [self.selection stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    [self addChildViewController:self.textEditor];
    [self.textView addSubview:self.textEditor.view];
    self.textEditor.view.frame = self.textView.bounds;
    self.textEditor.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.textEditor didMoveToParentViewController:self];
    
    
    [self.textEditor setHTML:self.html];

    self.segmentedControl.selectedSegmentIndex = self.type == HighlightTypeDescription ? 0 : 1;
    self.colorControl.selectedSegmentIndex = self.color - 1;
    [self updateColorView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.originViewHeight = self.view.frame.size.height;
    [self.textView becomeFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)updateColorView
{
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        NSString *colorCode = self.highlightColor.inlineColors[self.colorControl.selectedSegmentIndex];
        self.colorView.backgroundColor = [UIColor pxColorWithHexValue:colorCode];
    } else if (self.segmentedControl.selectedSegmentIndex ==  1) {
        NSString *colorCode = self.highlightColor.footerColors[self.colorControl.selectedSegmentIndex];
        self.colorView.backgroundColor = [UIColor pxColorWithHexValue:colorCode];
    }
}

- (IBAction)selectColor:(id)sender
{
    [self updateColorView];
}

- (IBAction)save:(id)sender
{
    [self.textView resignFirstResponder];
    [self.textEditor getHTML:^(NSString * html, NSError * _Nullable error) {
        [self.textEditor getText:^(NSString * text, NSError * _Nullable error) {
        NSString *newHTML = [MySoup stripTagsWithHtml:html];
        [self.delegate highlightViewController:self didSaveWithSelection:self.selection withPosition:self.position withNote:text withHtml: newHTML inRange:self.range useType:self.segmentedControl.selectedSegmentIndex == 0 ? HighlightTypeDescription : HighlightTypeFootnote withColor:self.colorControl.selectedSegmentIndex+1];
        }];
    }];
}

- (IBAction)close:(id)sender
{
    [self.textView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
