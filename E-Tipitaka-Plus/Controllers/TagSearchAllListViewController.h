//
//  TagSearchAllListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 10/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "SearchAllListViewController.h"
#import "Tag.h"

NS_ASSUME_NONNULL_BEGIN

@interface TagSearchAllListViewController : SearchAllListViewController

@property (nonatomic, strong) Tag* tag;

@end

NS_ASSUME_NONNULL_END
