//
//  TagHighlightViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "TagHighlightViewController.h"
#import "UserDatabaseHelper.h"
#import "Highlight.h"
#import <BlocksKit/BlocksKit.h>
#import "UIView+Snapshot.h"
#import "UITableView+LongPressMove.h"

#define kConfirmDeletingAlert       1000

@interface TagHighlightViewController()<UIAlertViewDelegate, UIGestureRecognizerDelegate, LongPressMoveDelegate>

@property (nonatomic, strong) Highlight *selectedHighlight;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation TagHighlightViewController

@synthesize tag = _tag;
@synthesize selectedHighlight = _selectedHighlight;
@synthesize selectedIndexPath = _selectedIndexPath;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.code = self.tag.code;
    self.navigationItem.title = self.tag.name;
    self.tableView.longPressMoveDelegate = self;    
    [self.tableView enableLongPressMove:1];
    
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadData];
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.tag.highlight exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [UserDatabaseHelper updateTagItems:@"highlight" items:self.tag.highlight ofTag:self.tag.name withCode:self.tag.code];
}

- (void)reloadData
{
    self.items = [UserDatabaseHelper queryHighlights:self.tag.highlight withCode:self.tag.code];
    [self.tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Highlight_", nil);
}

- (void)confirmDelete
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.selectedHighlight.selection
                                                    message:NSLocalizedString(@"Do you want to remove the item from this tag?", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"No", nil)
                                          otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
    alert.tag = kConfirmDeletingAlert;
    [alert show];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        self.selectedIndexPath = indexPath;
        self.selectedHighlight = [self.items objectAtIndex:indexPath.row];
        [self confirmDelete];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kConfirmDeletingAlert && buttonIndex == 1) {
        [self.items removeObjectAtIndex:self.selectedIndexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tag.highlight removeObject:[NSNumber numberWithInteger:self.selectedHighlight.rowId]];
        [UserDatabaseHelper updateTagItems:@"highlight" items:self.tag.highlight ofTag:self.tag.name withCode:self.tag.code];
    }
}

@end
