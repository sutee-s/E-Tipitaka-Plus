//
//  Tag.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "Tag.h"

@implementation Tag

@synthesize name = _name;
@synthesize priority = _priority;
@synthesize history = _history;
@synthesize highlight = _highlight;
@synthesize searchAllhistory = _searchAllhistory;
@synthesize note = _note;
@synthesize code = _code;

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.name = name;
    }
    return self;
}

- (NSDictionary *)dictionaryValue
{
    return @{@"name":self.name,
             @"priority":self.priority,
             @"history":[self.history componentsJoinedByString:@","],
             @"note":[self.note componentsJoinedByString:@","],
             @"highlight":[self.highlight componentsJoinedByString:@","],
             @"search_all":[self.searchAllhistory componentsJoinedByString:@","],
             @"code":[NSNumber numberWithInt:self.code]};
}

@end
