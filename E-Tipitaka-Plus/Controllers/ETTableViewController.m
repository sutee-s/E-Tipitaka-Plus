//
//  ETTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "ETTableViewController.h"

@interface ETTableViewController ()

@end

@implementation ETTableViewController

@synthesize code = _code;
@synthesize dataModel = _dataModel;

- (void)setCode:(LanguageCode)code
{
    if (_code == code && self.dataModel) { return; }
    _dataModel = [SharedDataModel sharedDataModel:code];
    _code = code;
}


@end
