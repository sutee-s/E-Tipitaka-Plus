//
//  OtherLanguageHistoryTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherLanguageHistoryTableViewController.h"
#import "OtherHistoryListViewController.h"

@interface OtherLanguageHistoryTableViewController ()

@end

@implementation OtherLanguageHistoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"History List" sender:indexPath];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"History List"]) {
        OtherHistoryListViewController *controller = segue.destinationViewController;
        controller.selectedCode = self.selectedCode;
        controller.user = self.user;
    }
}

@end
