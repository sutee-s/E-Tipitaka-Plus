//
//  MyDocument.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 26/3/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "MyDocument.h"

@implementation MyDocument

@synthesize jsonString = _jsonString;

- (NSArray *)items
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

- (void)importData
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self items] options:NSJSONWritingPrettyPrinted error:&error];    
    if (error) {
        NSLog(@"%@", error);
    }
    self.jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError *__autoreleasing *)outError
{
    if ([contents length] > 0) {
        self.jsonString = [[NSString alloc] initWithBytes:[contents bytes] length:[contents length] encoding:NSUTF8StringEncoding];
    } else {
        self.jsonString = @"";
    }
    return YES;
}

- (id)contentsForType:(NSString *)typeName error:(NSError *__autoreleasing *)outError
{
    if (self.jsonString.length == 0) {
        self.jsonString = @"";
    }
    return [NSData dataWithBytes:[self.jsonString UTF8String] length:[self.jsonString length]];
}

@end
