//
//  SyncDatabaseManager.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 24/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncDatabaseManager : NSObject

@property (nonatomic, assign) BOOL syncCompleted;

+ (instancetype)sharedInstance;

- (void)sync:(void (^)(BOOL success, BOOL firstTime))block;
- (void)syncWithProgressHUD:(void (^)(BOOL success))block;
- (void)syncManuallyWithProgressHUD:(void (^)(BOOL success))block inViewController:(UIViewController *)controller;

- (NSProgress *)pushAll:(void (^)(BOOL success))block;
- (NSProgress *)push:(NSString *)dbPath onSuccess:(void (^)(BOOL success))block;
- (NSProgress *)pushDatabases:(NSArray *)dbPaths successBlock:(void (^)(BOOL))block;
- (void)delayPush:(NSString *)dbPath;
- (void)cancelAllUploadTasks;

- (BOOL)enabled;
- (void)checkSyncCompleted;
- (void)database:(NSString *)dbName edited:(BOOL)edited;
- (BOOL)databaseIsEdited:(NSString *)dbName;

@end
