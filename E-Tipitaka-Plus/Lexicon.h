//
//  Lexicon.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Lexicon : NSObject

@property (nonatomic, strong) NSString *head;
@property (nonatomic, strong) NSString *translation;
@property (nonatomic, assign) DictionaryType type;
@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, readonly) NSAttributedString *attributedTranslation;

- (id)initWithHead:(NSString *)head andTranslation:(NSString *)translation;
- (NSDictionary *)dictionaryValue;

@end
