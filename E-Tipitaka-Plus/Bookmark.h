//
//  Bookmark.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bookmark : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL starred;
@property (nonatomic, assign) NSInteger rank;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *plainNote;
@property (nonatomic, strong) NSString *snippet;
@property (nonatomic, assign) LanguageCode code;

- (NSDictionary *)dictionaryValue;

@end
