//
//  SearchAllHistory.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchAllHistory : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, assign) NSInteger state;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, assign) NSUInteger noteState;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger priority;
@property (nonatomic, strong) NSDate *created;

- (NSDictionary *)dictionaryValue;

@end

NS_ASSUME_NONNULL_END
