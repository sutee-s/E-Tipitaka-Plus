//
//  ETThaiDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 13/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETThaiDataModel.h"


@interface ETThaiDataModel()

@end

@implementation ETThaiDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiSiam;
}

- (NSString *)codeString
{
    return @"thai";
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Thai Royal", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Thai Royal", nil);
}

- (NSString *)abbrTitle
{
    return NSLocalizedString(@"thai", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 45;
}

@end
