//
//  Models.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#ifndef E_Tipitaka_Plus_Models_h
#define E_Tipitaka_Plus_Models_h

#import "ETDataModel.h"
#import "ETThaiDataModel.h"
#import "ETPaliDataModel.h"
#import "ETThaiMahaMakutDataModel.h"
#import "ETThaiMahaChulaDataModel.h"
#import "ETThaiMahaChula2DataModel.h"
#import "ETThaiFiveBooksDataModel.h"
#import "ETThaiWatnaDataModel.h"
#import "ETThaiPocketBookDataModel.h"
#import "ETRomanScriptDataModel.h"
#import "ETPaliMahaChulaDataModel.h"
#import "ETThaiSupremeDataModel.h"
#import "ETThaiVinayaDataModel.h"
#import "ETPaliNewDataModel.h"
#import "SharedDataModel.h"

#endif
