//
//  ETPaliDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 13/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETPaliDataModel.h"

@implementation ETPaliDataModel

- (LanguageCode)code
{
    return LanguageCodePaliSiam;
}

- (NSString *)codeString
{
    return @"pali";
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Pali Siam", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Pali Siam", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Pali word", nil);
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 45;
}

@end
