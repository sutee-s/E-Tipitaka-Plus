//
//  SharedDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "SharedDataModel.h"

@implementation SharedDataModel

+ (ETDataModel *)sharedDataModel:(LanguageCode)code
{
    switch (code) {
        case LanguageCodeThaiSiam:
            return [SharedDataModel thaiSiamDataModel];
        case LanguageCodePaliSiam:
            return [SharedDataModel paliSiamDataModel];
        case LanguageCodeThaiMahaMakut:
            return [SharedDataModel thaiMahaMakutDataModel];
        case LanguageCodeThaiMahaChula:
            return [SharedDataModel thaiMahaChulaDataModel];
        case LanguageCodeThaiMahaChula2:
            return [SharedDataModel thaiMahaChula2DataModel];
        case LanguageCodeThaiFiveBooks:
            return [SharedDataModel thaiFiveBooksDataModel];
        case LanguageCodeThaiWatna:
            return [SharedDataModel thaiWatnaDataModel];
        case LanguageCodeThaiPocketBook:
            return [SharedDataModel thaiPocketBookDataModel];
        case LanguageCodeRomanScript:
            return [SharedDataModel romanScriptDataModel];
        case LanguageCodePaliMahaChula:
            return [SharedDataModel paliMahaChulaDataModel];
        case LanguageCodeThaiSupreme:
            return [SharedDataModel thaiSupremeDataModel];
        case LanguageCodePaliSiamNew:
            return [SharedDataModel paliSiamNewDataModel];
        case LanguageCodeThaiVinaya:
            return [SharedDataModel thaiVinayaDataModel];
        default:
            break;
    }
    return nil;
}

+ (ETThaiDataModel *)thaiSiamDataModel
{
    static ETThaiDataModel *__sharedThaiSiamDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiSiamDataModel = [[ETThaiDataModel alloc] init];
    });
    return __sharedThaiSiamDataModel;
}

+ (ETPaliMahaChulaDataModel *)paliMahaChulaDataModel
{
    static ETPaliMahaChulaDataModel *__sharedPaliMahaChulaDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedPaliMahaChulaDataModel = [[ETPaliMahaChulaDataModel alloc] init];
    });
    return __sharedPaliMahaChulaDataModel;
}


+ (ETThaiWatnaDataModel *)thaiWatnaDataModel
{
    static ETThaiWatnaDataModel *__sharedThaiWatnaDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiWatnaDataModel = [[ETThaiWatnaDataModel alloc] init];
    });
    return __sharedThaiWatnaDataModel;
}

+ (ETThaiPocketBookDataModel *)thaiPocketBookDataModel
{
    static ETThaiPocketBookDataModel *__sharedThaiPocketBookDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiPocketBookDataModel = [[ETThaiPocketBookDataModel alloc] init];
    });
    return __sharedThaiPocketBookDataModel;
}

+ (ETRomanScriptDataModel *)romanScriptDataModel
{
    static ETRomanScriptDataModel *__sharedRomanScriptDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedRomanScriptDataModel = [[ETRomanScriptDataModel alloc] init];
    });
    return __sharedRomanScriptDataModel;
}

+ (ETPaliDataModel *)paliSiamDataModel
{
    static ETPaliDataModel *__sharedPaliSiamDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedPaliSiamDataModel = [[ETPaliDataModel alloc] init];
    });
    return __sharedPaliSiamDataModel;
}

+ (ETPaliNewDataModel *)paliSiamNewDataModel
{
    static ETPaliNewDataModel *__sharedPaliSiamNewDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedPaliSiamNewDataModel = [[ETPaliNewDataModel alloc] init];
    });
    return __sharedPaliSiamNewDataModel;
}

+ (ETThaiMahaMakutDataModel *)thaiMahaMakutDataModel
{
    static ETThaiMahaMakutDataModel *__sharedThaiMMDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiMMDataModel = [[ETThaiMahaMakutDataModel alloc] init];
    });
    return __sharedThaiMMDataModel;
}

+ (ETThaiMahaChulaDataModel *)thaiMahaChulaDataModel
{
    static ETThaiMahaChulaDataModel *__sharedThaiMCDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiMCDataModel = [[ETThaiMahaChulaDataModel alloc] init];
    });
    return __sharedThaiMCDataModel;
}

+ (ETThaiMahaChula2DataModel *)thaiMahaChula2DataModel
{
    static ETThaiMahaChula2DataModel *__sharedThaiMC2DataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiMC2DataModel = [[ETThaiMahaChula2DataModel alloc] init];
    });
    return __sharedThaiMC2DataModel;
}

+ (ETThaiSupremeDataModel *)thaiSupremeDataModel
{
    static ETThaiSupremeDataModel *__sharedThaiSupremeDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiSupremeDataModel = [[ETThaiSupremeDataModel alloc] init];
    });
    return __sharedThaiSupremeDataModel;
}

+ (ETThaiVinayaDataModel *)thaiVinayaDataModel
{
    static ETThaiVinayaDataModel *__sharedThaiVinayaDataModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiVinayaDataModel = [[ETThaiVinayaDataModel alloc] init];
    });
    return __sharedThaiVinayaDataModel;
}

+ (ETThaiFiveBooksDataModel *)thaiFiveBooksDataModel
{
    static ETThaiFiveBooksDataModel *__sharedThaiFiveBooksModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedThaiFiveBooksModel = [[ETThaiFiveBooksDataModel alloc] init];
    });
    return __sharedThaiFiveBooksModel;
}

@end
