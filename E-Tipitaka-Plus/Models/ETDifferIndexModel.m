//
//  ETDifferIndexModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 18/4/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "ETDifferIndexModel.h"

@implementation ETDifferIndexModel

- (NSDictionary *)itemMappingTable
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSArray *)queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume isDefault:(BOOL)isDefault
{
    if (isDefault) {
        NSMutableArray *pages = [[NSMutableArray alloc] init];
        NSDictionary *dict = [self itemMappingTable];
        for (int sub = 1;;++sub) {
            NSString *key = [NSString stringWithFormat:@"v%d-%d-i%d", volume, sub, item];
            if (dict[key]) {
                [pages addObject:dict[key]];
            } else {
                break;
            }
        }
        return pages;
    }
    return [super queryPagesOfItem:item inVolume:volume isDefault:isDefault];
}

- (NSInteger)getSubItemInVolume:(NSInteger)volume page:(NSInteger)page item:(NSInteger *)item
{
    NSArray *pair = [self itemMappingTable][[NSString stringWithFormat:@"v%d-p%d", volume, page]];
    *item = [pair[0] integerValue];
    return [pair[1] integerValue];
}

@end
