//
//  ETBasicDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 13/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETBasicDataModel.h"
#import "BookDatabaseHelper.h"

@interface ETBasicDataModel()

@property (nonatomic, strong) NSArray *titles;

@end

@implementation ETBasicDataModel

@synthesize titles = _titles;

- (BOOL)hasIndexNumber
{
    return YES;
}

- (NSInteger) totalPagesOfVolume:(NSInteger)volume
{
    return [BookDatabaseHelper totalPagesOfVolume:volume withCode:self.code];
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return [BookDatabaseHelper totalItemsOfVolume:volume withCode:self.code];
}

- (PageResult *) queryPage:(NSInteger)page inVolume:(NSInteger)volume
{
    return [BookDatabaseHelper queryPage:page inVolume:volume withCode:self.code];
}

- (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page
{
    return [BookDatabaseHelper queryItemsInVolume:volume page:page withCode:self.code];
}

- (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume isDefault:(BOOL)isDefault
{
    return [BookDatabaseHelper queryPagesOfItem:item inVolume:volume withCodeString:self.codeString];
}
- (NSArray *) search:(NSString *)keyword
{
    return [BookDatabaseHelper search:keyword withCode:self.code];
}

@end
