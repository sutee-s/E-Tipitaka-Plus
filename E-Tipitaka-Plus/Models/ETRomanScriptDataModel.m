//
//  ETRomanScriptDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 11/12/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "ETRomanScriptDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETRomanScriptDataModel

- (LanguageCode)code
{
    return LanguageCodeRomanScript;
}

- (NSString *)codeString
{
    return @"romanct";
}

- (NSString *)title
{
    return @"Tipitaka Roman Script";
}

- (NSString *)shortTitle
{
    return @"Roman Script";
}

- (NSString *)placeholder
{
    return @"Enter Roman script";
}

- (NSString *)volumeTitle:(NSInteger)volume
{
    NSDictionary *dict = [BookDatabaseHelper romanTitles];
    NSString *kVolume = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:volume]];

    if (!dict[kVolume]) {
        return @"";
    }

    return [dict[kVolume] lastObject];
}

- (NSInteger)totalVolumesInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return 5;
        case 2:
            return 43;
        case 3:
            return 13;
        default:
            return 0;
    }
}

- (BOOL)hasDifferentItemIndex
{
    return YES;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 61;
}

- (NSString *)itemIndexingSystemName
{
    return @"Roman Script";
}

- (NSInteger)totalItemsOfVolume:(NSInteger)volume
{
    return [BookDatabaseHelper totalItemsOfVolume:volume withCodeString:self.codeString];
}

- (NSArray *)convertFromPivot:(NSInteger)volume item:(NSInteger)item subItem:(NSInteger)subItem
{
    NSDictionary *dict1 = [BookDatabaseHelper romanReverseVolumeMappingTable];
    NSString *kVolume = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:volume]];
    NSString *kItem = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:item]];
    NSString *kSubItem = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:subItem]];
    
    NSArray *result = dict1[kVolume][kItem][kSubItem];
    
    if (!result) {
        return nil;
    }
    
    NSNumber *rVolume = result[0];
    NSNumber *rPage = result[1];
    
    return @[rVolume, rPage];
}

- (NSArray *)convertToPivot:(NSInteger)volume page:(NSInteger)page item:(NSInteger)item
{
    NSDictionary *dict1 = [BookDatabaseHelper romanVolumeMappingTable];
    NSString *kVolume = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:volume]];
    NSString *kPage = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:page]];
    
    NSArray *result = dict1[kVolume][kPage];
    
    if (!result) {
        return nil;
    }

    NSNumber *rVolume = result[0][0];
    NSNumber *rItem = result[0][1];
    NSNumber *rSubItem = result[0][2];
    
    return @[rVolume, rItem, rSubItem];
}

@end
