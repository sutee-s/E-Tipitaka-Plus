//
//  SharedDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Models.h"
#import "Types.h"

@interface SharedDataModel : NSObject

+ (ETThaiDataModel *)thaiSiamDataModel;
+ (ETPaliDataModel *)paliSiamDataModel;
+ (ETThaiMahaMakutDataModel *)thaiMahaMakutDataModel;
+ (ETThaiMahaChulaDataModel *)thaiMahaChulaDataModel;
+ (ETThaiMahaChula2DataModel *)thaiMahaChula2DataModel;
+ (ETThaiFiveBooksDataModel *)thaiFiveBooksDataModel;
+ (ETThaiWatnaDataModel *)thaiWatnaDataModel;
+ (ETThaiPocketBookDataModel *)thaiPocketBookDataModel;
+ (ETRomanScriptDataModel *)romanScriptDataModel;
+ (ETPaliMahaChulaDataModel *)paliMahaChulaDataModel;
+ (ETThaiSupremeDataModel *)thaiSupremeDataModel;
+ (ETThaiVinayaDataModel *)thaiVinayaDataModel;
+ (ETPaliNewDataModel *)paliSiamNewDataModel;
+ (ETDataModel *)sharedDataModel:(LanguageCode)code;
@end
