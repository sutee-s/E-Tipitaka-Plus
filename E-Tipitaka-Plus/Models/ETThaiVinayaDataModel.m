//
//  ETThaiVinayaDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/10/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "ETThaiVinayaDataModel.h"
#import "BookDatabaseHelper.h"
#import "NSString+Thai.h"

@implementation ETThaiVinayaDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiVinaya;
}

- (NSString *)codeString
{
    return @"thaivn";
}

- (NSString *)title
{
    return NSLocalizedString(@"Ariyavinaya", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Ariyavinaya", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (BOOL)isBuddhawajana
{
    return YES;
}

- (BOOL)isTipitaka
{
    return NO;
}

- (BOOL)hasIndexNumber
{
    return YES;
}

- (NSInteger)firstVolume
{
    return 1;
}

- (BOOL)hasHtmlContent
{
    return YES;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return YES;
}

- (NSString *)volumeTitle:(NSInteger)volume
{
    return [self.titles objectForKey:[NSString stringWithFormat:@"thaivn_%@", [NSNumber numberWithInteger:volume]]];
}

- (NSInteger)totalSections
{
    return 1;
}

- (NSInteger)totalVolumesInSection:(NSInteger)section
{
    return 11;
}

- (NSInteger) getSubItemInVolume:(NSInteger)volume page:(NSInteger)page item:(NSInteger *)item
{
    return 1;
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return 0;
}

- (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page
{
    if (volume > 9) return @[];
    
    FMDatabase *db = [BookDatabaseHelper database:self.code];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT content FROM main WHERE volume = ? AND page = ?", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page]];
    
    NSMutableArray *items = [NSMutableArray new];
    
    if ([s next]) {
        NSString *content = [s stringForColumnIndex:0];
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:ITEM_4_PATTERN options:NSRegularExpressionCaseInsensitive error:nil];
        NSArray *results = [regex matchesInString:content options:0 range:NSMakeRange(0, content.length)];
        for (NSTextCheckingResult *match in results) {
            NSRange range = [match rangeAtIndex:1];
            NSNumber *item = [[content substringWithRange:range] numberValueFromThaiNumberic];
            [items addObject:item];
        }
    }
    
    [db close];
    
    return items;
}

- (NSInteger) comparingVolume:(NSInteger)volume page:(NSInteger)page
{
    return (volume == 1 ? 9 : volume-1);
}

@end
