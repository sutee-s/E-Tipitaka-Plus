//
//  ETDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 13/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PageResult.h"
#import "Types.h"

@interface ETDataModel : NSObject

@property (nonatomic, readonly) LanguageCode code;
@property (nonatomic, readonly) NSString *codeString;
@property (nonatomic, readonly) NSInteger totalVolumes;
@property (nonatomic, readonly) NSInteger totalSections;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *shortTitle;
@property (nonatomic, readonly) NSString *abbrTitle;
@property (nonatomic, readonly) NSString *placeholder;
@property (nonatomic, readonly) BOOL hasDifferentItemIndex;
@property (nonatomic, readonly) NSString *itemIndexingSystemName;
@property (nonatomic, strong) NSDictionary *titles;
@property (nonatomic, readonly) NSInteger firstVolume;
@property (nonatomic, readonly) BOOL hasHtmlContent;
@property (nonatomic, readonly) BOOL hasIndexNumber;
@property (nonatomic, readonly) BOOL isBuddhawajana;

- (NSInteger) totalPagesOfVolume:(NSInteger)volume;
- (NSInteger) totalItemsOfVolume:(NSInteger)volume;
- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing;
- (PageResult *) queryPage:(NSInteger)page inVolume:(NSInteger)volume;
- (NSArray *) queryPagesInVolume:(NSInteger)volume;
- (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page;
- (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume;
- (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume isDefault:(BOOL)isDefault;
- (NSArray *) search:(NSString *)keyword;
- (NSArray *) search:(NSString *)keyword type:(SearchType)type;
- (NSString *) volumeTitle:(NSInteger)volume;
- (void)saveSearchResults:(NSArray *)results ofKeywords:(NSString *)keywords;
- (NSInteger) getSubItemInVolume:(NSInteger)volume page:(NSInteger)page item:(NSInteger *)item;
- (NSInteger) totalVolumesInSection:(NSInteger)section;
- (NSInteger) convertVolume:(NSInteger)volume item:(NSInteger *)item subItem:(NSInteger)subItem;
- (NSInteger) comparingVolume:(NSInteger)volume page:(NSInteger)page;
- (BOOL) isTipitaka;
- (NSInteger)startPageOfVolume:(NSInteger)volume;
- (BOOL)isValidVolume:(NSInteger)volume;

- (NSArray *)convertToPivot:(NSInteger)volume page:(NSInteger)page item:(NSInteger)item;
- (NSArray *)convertFromPivot:(NSInteger)volume item:(NSInteger)item subItem:(NSInteger)subItem;

- (NSInteger) convertVolume:(NSInteger)volume inSection:(NSInteger)section;
@end
