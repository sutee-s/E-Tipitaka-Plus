//
//  ETThaiFiveBooksDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/6/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "ETThaiFiveBooksDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETThaiFiveBooksDataModel

- (id)init
{
    self = [super init];
    if (self) {
        self.titles = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"fivebooks" ofType:@"plist"]];
    }
    return self;
}

- (LanguageCode)code
{
    return LanguageCodeThaiFiveBooks;
}

- (NSString *)codeString
{
    return @"thaibt";
}

- (NSString *)title
{
    return NSLocalizedString(@"Five books from Buddha speech", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Five books", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (NSArray *) search:(NSString *)keyword
{
    FMDatabase *db = [BookDatabaseHelper database:self.code];
    
    if (![db open]) return nil;
    
    NSMutableArray *args = [[NSMutableArray alloc] init];
    for (NSString *term in [keyword componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]) {
        [args addObject:[NSString stringWithFormat:@"*%@*", [term stringByReplacingOccurrencesOfString:@"+" withString:@" "]]];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    if (args.count > 0) {
        NSString *statement = @"SELECT book,page FROM speech WHERE content GLOB ?";
        for (int i=0; i < args.count-1; ++i) {
            statement = [statement stringByAppendingString:@" AND content GLOB ?"];
        }
        FMResultSet *s = [db executeQuery:statement withArgumentsInArray:args];
        while ([s next]) {
            [results addObject:@{@"volume": [NSNumber numberWithInteger:[s intForColumn:@"book"]], @"page": [NSNumber numberWithInteger:[s intForColumn:@"page"]]}];
        }
    }
    
    [db close];
    
    return results;
}

- (BOOL)isTipitaka
{
    return NO;
}

- (BOOL)hasIndexNumber
{
    return NO;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 5;
}


- (NSInteger)startPageOfVolume:(NSInteger)volume
{
    return volume == 3 ? 818 : 1;
}



- (NSInteger) getSubItemInVolume:(NSInteger)volume page:(NSInteger)page item:(NSInteger *)item
{
    return 1;
}

- (NSInteger) totalPagesOfVolume:(NSInteger)volume
{
    switch (volume) {
        case 1:
            return 466;
        case 2:
            return 817;
        case 3:
            return 1572;
        case 4:
            return 813;
        case 5:
            return 614;
        default:
            return 0;
    }
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return 0;
}

- (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page
{
    return nil;
}

- (NSInteger)totalSections
{
    return 1;
}

- (NSInteger)totalVolumesInSection:(NSInteger)section
{
    return 5;
}


@end
