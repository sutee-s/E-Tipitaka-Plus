//
//  ETPaliDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 13/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETBasicDataModel.h"

@interface ETPaliDataModel : ETBasicDataModel

@end
