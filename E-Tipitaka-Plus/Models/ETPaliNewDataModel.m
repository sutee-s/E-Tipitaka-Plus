//
//  ETPaliNewDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/2/19.
//  Copyright © 2019 Watnapahpong. All rights reserved.
//

#import "ETPaliNewDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETPaliNewDataModel

- (LanguageCode)code
{
    return LanguageCodePaliSiamNew;
}

- (NSString *)codeString
{
    return @"palinew";
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka New Pali Siam", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"New Pali Siam", nil);
}

- (BOOL)hasHtmlContent
{
    return YES;
}

- (BOOL)hasIndexNumber
{
    return YES;
}

- (NSString *) volumeTitle:(NSInteger)volume
{
    return [self.titles objectForKey:[NSString stringWithFormat:@"%@_%d", @"pali", volume]];
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return [BookDatabaseHelper totalItemsOfVolume:volume withCode:LanguageCodePaliSiam];
}

- (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume isDefault:(BOOL)isDefault
{
    return [BookDatabaseHelper queryPagesOfItem:item inVolume:volume withCodeString:@"pali"];
}



@end
