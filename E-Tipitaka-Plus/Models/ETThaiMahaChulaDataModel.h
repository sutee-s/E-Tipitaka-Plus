//
//  ETThaiMahaChulaDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 25/12/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETDifferIndexModel.h"

@interface ETThaiMahaChulaDataModel : ETDifferIndexModel

@property (nonatomic, readonly) NSString *codeStringForResource;

@end
