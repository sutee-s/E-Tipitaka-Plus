//
//  ETThaiVinayaDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/10/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "ETHandbookDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ETThaiVinayaDataModel : ETHandbookDataModel

@end

NS_ASSUME_NONNULL_END
