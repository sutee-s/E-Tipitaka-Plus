//
//  ETThaiMahaMakutDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETThaiMahaMakutDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETThaiMahaMakutDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiMahaMakut;
}

- (NSString *)codeString
{
    return @"thaimm";
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Thai Maha Makut", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Thai Maha Makut", nil);
}

- (NSString *)abbrTitle
{
    return NSLocalizedString(@"thaimm", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (NSInteger)totalVolumesInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return 9;
        case 2:
            return 65;
        case 3:
            return 17;
        default:
            return 0;
    }
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 91;
}

- (NSInteger)totalItemsOfVolume:(NSInteger)volume
{
    return [BookDatabaseHelper totalItemsOfVolume:volume withCodeString:self.codeString];
}

- (NSInteger)convertVolume:(NSInteger)volume item:(NSInteger *)item subItem:(NSInteger)subItem
{
    NSDictionary *table = [BookDatabaseHelper volumeMappingTable];
    NSString *key = [NSString stringWithFormat:@"%d-%d-%d", volume, subItem, table[self.codeString] ? *item : 1];
    return [table[self.codeString][key] integerValue];
}

- (NSInteger)comparingVolume:(NSInteger)volume page:(NSInteger)page
{
    FMDatabase *db = [BookDatabaseHelper database:self.code];
    
    if (![db open]) return volume;
    
    FMResultSet *s = [db executeQuery:@"SELECT volume_orig FROM main WHERE volume = ? AND page = ?", [NSString stringWithFormat:@"%02d", volume], [NSString stringWithFormat:@"%04d", page]];

    NSInteger comparingVolume = volume;
    
    if ([s next]) {
        comparingVolume = [[[s stringForColumnIndex:0] componentsSeparatedByString:@" "][0] intValue];
    }
    
    [db close];
    
    return comparingVolume;
}

- (NSArray *)convertFromPivot:(NSInteger)volume item:(NSInteger)item subItem:(NSInteger)subItem
{
    NSNumber *rVolume = [NSNumber numberWithInteger:[self convertVolume:volume item:&item subItem:subItem]];
    NSInteger page = [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:rVolume.integerValue withCodeString:self.codeString];
    return @[rVolume, [NSNumber numberWithInteger:page]];
}

- (NSArray *)convertToPivot:(NSInteger)volume page:(NSInteger)page item:(NSInteger)item
{
    NSInteger subItem = [self getSubItemInVolume:volume page:page item:&item];
    NSNumber *rVolume = [NSNumber numberWithInteger:[self comparingVolume:volume page:page]];
    return @[rVolume, [NSNumber numberWithInteger:item], [NSNumber numberWithInteger:subItem]];
}

@end
