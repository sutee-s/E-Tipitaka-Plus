//
//  ETPaliMahaChulaDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 11/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "ETPaliMahaChulaDataModel.h"

@implementation ETPaliMahaChulaDataModel

- (LanguageCode)code
{
    return LanguageCodePaliMahaChula;
}

- (NSString *)codeString
{
    return @"palimc";
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Pali Maha Chula", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Pali Siam Maha Chula", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Pali word", nil);
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 45;
}

- (BOOL)hasDifferentItemIndex
{
    return YES;
}

- (NSString *)itemIndexingSystemName
{
    return NSLocalizedString(@"Maha Chula", nil);
}

@end
