//
//  ETHandbookDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/10/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "ETHandbookDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETHandbookDataModel

- (NSArray *)search:(NSString *)keyword type:(SearchType)type
{
    FMDatabase *db = [BookDatabaseHelper database:self.code];
    
    if (![db open]) { return nil; }
    
    NSMutableArray *args = [[NSMutableArray alloc] init];
    for (NSString *term in [keyword componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]) {
        [args addObject:[NSString stringWithFormat:@"*%@*", [term stringByReplacingOccurrencesOfString:@"+" withString:@" "]]];
    }
    
    if (args.count == 0) {
        [db close];
        return @[];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    NSString *statement = [NSString stringWithFormat:@"SELECT volume,page FROM main WHERE %@ GLOB ?", type == SearchTypeAll ? @"content" : @"buddhawaj"];
    for (int i=0; i < args.count-1; ++i) {
        statement = [statement stringByAppendingString:[NSString stringWithFormat:@" AND %@ GLOB ?", type == SearchTypeAll ? @"content" : @"buddhawaj"]];
    }
    FMResultSet *s = [db executeQuery:statement withArgumentsInArray:args];
    while ([s next]) {
        [results addObject:@{@"volume": [NSNumber numberWithInteger:[s intForColumn:@"volume"]], @"page": [NSNumber numberWithInteger:[s intForColumn:@"page"]]}];
    }
    [db close];
    return results;
}

@end
