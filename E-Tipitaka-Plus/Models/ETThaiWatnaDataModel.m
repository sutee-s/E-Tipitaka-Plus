//
//  ETThaiWatnaDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 18/8/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "ETThaiWatnaDataModel.h"
#import "BookDatabaseHelper.h"
#import <FMDB/FMDatabase.h>

@interface ETThaiWatnaDataModel ()

@end

@implementation ETThaiWatnaDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiWatna;
}

- (NSString *)codeString
{
    return @"thaiwn";
}

- (NSString *)title
{
    return NSLocalizedString(@"Buddhawajana Tipitaka", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Buddhawajana", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (BOOL)isBuddhawajana
{
    return YES;
}

- (BOOL)isTipitaka
{
    return NO;
}

- (NSInteger)firstVolume
{
    return 1;
}

- (BOOL)hasHtmlContent
{
    return YES;
}

- (BOOL)hasIndexNumber
{
    return YES;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 33;
}


- (NSString *)volumeTitle:(NSInteger)volume
{
    return [self.titles objectForKey:[NSString stringWithFormat:@"thai_%d", [self comparingVolume:volume page:1]]];
}

- (NSInteger)totalSections
{
    return 1;
}

- (NSInteger)totalVolumesInSection:(NSInteger)section
{
    return 33;
}

- (NSInteger)convertVolume:(NSInteger)volume item:(NSInteger *)item subItem:(NSInteger)subItem
{
    if (volume <= 8) {
        return volume + 25;
    }
    return volume - 8;
}

- (NSInteger)comparingVolume:(NSInteger)volume page:(NSInteger)page
{
    if (volume <= 25) {
        return volume + 8;
    }
    return volume - 25;
}

- (NSArray *)search:(NSString *)keyword type:(SearchType)type
{
    FMDatabase *db = [BookDatabaseHelper database:LanguageCodeThaiWatna];
    
    if (![db open]) { return nil; }
    
    NSMutableArray *args = [[NSMutableArray alloc] init];
    for (NSString *term in [keyword componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]) {
        [args addObject:[NSString stringWithFormat:@"*%@*", [term stringByReplacingOccurrencesOfString:@"+" withString:@" "]]];
    }

    if (args.count == 0) {
        [db close];
        return @[];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];

    NSString *statement = [NSString stringWithFormat:@"SELECT volume,page FROM main WHERE %@ GLOB ?", type == SearchTypeAll ? @"content" : @"buddhawaj"];
    for (int i=0; i < args.count-1; ++i) {
        statement = [statement stringByAppendingString:[NSString stringWithFormat:@" AND %@ GLOB ?", type == SearchTypeAll ? @"content" : @"buddhawaj"]];
    }
    FMResultSet *s = [db executeQuery:statement withArgumentsInArray:args];
    while ([s next]) {
        [results addObject:@{@"volume": [NSNumber numberWithInteger:[s intForColumn:@"volume"]], @"page": [NSNumber numberWithInteger:[s intForColumn:@"page"]]}];
    }
    [db close];
    return results;
}

@end
