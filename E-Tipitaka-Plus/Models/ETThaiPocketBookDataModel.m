//
//  ETThaiPocketBookDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/10/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "ETThaiPocketBookDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETThaiPocketBookDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiPocketBook;
}

- (NSString *)codeString
{
    return @"thaipb";
}

- (NSString *)title
{
    return NSLocalizedString(@"Buddhawajana Pocket Book", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Pocket Book", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (BOOL)isBuddhawajana
{
    return YES;
}

- (BOOL)isTipitaka
{
    return NO;
}

- (BOOL)hasIndexNumber
{
    return NO;
}

- (NSInteger)firstVolume
{
    return 1;
}

- (BOOL)hasHtmlContent
{
    return YES;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return YES;
}

- (NSString *)volumeTitle:(NSInteger)volume
{
    return [self.titles objectForKey:[NSString stringWithFormat:@"thaipb_%@", [NSNumber numberWithInteger:volume]]];
}

- (NSInteger)totalSections
{
    return 1;
}

- (NSInteger)totalVolumesInSection:(NSInteger)section
{
    return 20;
}

- (NSInteger) getSubItemInVolume:(NSInteger)volume page:(NSInteger)page item:(NSInteger *)item
{
    return 1;
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return 0;
}

- (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page
{
    return nil;
}

@end
