//
//  ETPaliNewDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/2/19.
//  Copyright © 2019 Watnapahpong. All rights reserved.
//

#import "ETPaliDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ETPaliNewDataModel : ETPaliDataModel

@end

NS_ASSUME_NONNULL_END
