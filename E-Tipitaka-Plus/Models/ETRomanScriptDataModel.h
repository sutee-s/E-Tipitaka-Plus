//
//  ETRomanScriptDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 11/12/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "ETBasicDataModel.h"

@interface ETRomanScriptDataModel : ETBasicDataModel

@end
