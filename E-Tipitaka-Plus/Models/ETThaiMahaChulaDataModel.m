//
//  ETThaiMahaChulaDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 25/12/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETThaiMahaChulaDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETThaiMahaChulaDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiMahaChula;
}

- (NSString *)codeString
{
    return @"thaimc";
}

- (NSString *)codeStringForResource
{
    return self.codeString;
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Thai Maha Chula", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Thai Maha Chula", nil);
}

- (NSString *)abbrTitle
{
    return NSLocalizedString(@"thaimc", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (BOOL)hasDifferentItemIndex
{
    return YES;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 45;
}

- (NSString *)itemIndexingSystemName
{
    return NSLocalizedString(@"Maha Chula", nil);
}

- (NSInteger)totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return defaultItemIndexing ? [BookDatabaseHelper totalItemsOfVolume:volume withCode:LanguageCodeThaiSiam] : [BookDatabaseHelper totalItemsOfVolume:volume withCodeString:self.codeStringForResource];
}

- (NSDictionary *)itemMappingTable
{
    return [BookDatabaseHelper itemMappingTableMc];
}

@end
