//
//  MyDocument.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 26/3/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDocument : UIDocument

@property (strong) NSString *jsonString;

- (void)importData;
- (NSArray *)items;

@end
