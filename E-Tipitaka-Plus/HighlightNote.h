//
//  HighlightNote.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/8/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Highlight.h"

@interface HighlightNote : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, strong) Highlight *highlight;
@property (nonatomic, strong) NSString *selection;
@property (nonatomic, assign) NSRange range;

@end
