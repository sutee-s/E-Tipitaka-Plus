//
//  PageInfo.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 11/12/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageInfo : NSObject

@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSString *keywords;

- (id)initWithCode:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page;

@end
