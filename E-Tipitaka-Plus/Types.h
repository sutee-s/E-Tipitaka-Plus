//
//  Types.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 30/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#ifndef E_Tipitaka_Plus_Types_h
#define E_Tipitaka_Plus_Types_h

typedef enum {
    LanguageCodeThaiSiam        = 1,
    LanguageCodePaliSiam        = 2,
    LanguageCodeThaiMahaMakut   = 3,
    LanguageCodeThaiMahaChula   = 4,
    LanguageCodeThaiFiveBooks   = 5,
    LanguageCodeThaiWatna       = 6,
    LanguageCodeThaiPocketBook  = 7,
    LanguageCodeRomanScript     = 8,
    LanguageCodePaliMahaChula   = 9,
    LanguageCodeThaiSupreme     = 10,
    LanguageCodeThaiVinaya      = 11,
    LanguageCodePaliSiamNew     = 12,
    LanguageCodeThaiMahaChula2  = 13
} LanguageCode;

typedef enum {
    ComparingSideLeft   = 1,
    ComparingSideRight  = 2,
} ComparingSide;

typedef enum {
    DictionaryTypePali  = 1,
    DictionaryTypeThai  = 2,
    DictionaryTypeEnglish = 3,
} DictionaryType;

typedef enum {
    HighlightTypeDescription  = 1,
    HighlightTypeFootnote     = 2,
} HighlightType;

typedef enum {
    SearchTypeAll = 1,
    SearchTypeBuddhawaj = 2,
} SearchType;

typedef enum {
    ReadingStateTypeSkimmed = 1,
    ReadingStateTypeRead = 2,
    ReadingStateTypeUnread = 3,
} ReadingStateType;


#endif
