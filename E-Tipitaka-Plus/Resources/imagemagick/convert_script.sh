#!/bin/bash

set -x
cd iphone-ipad
for i in *.png; do cp $i `echo ../2x/$i | sed s/.png/@2x.png/` ;done

cd ../ipad
for i in *.png; do cp $i `echo ../2x/$i | sed s/.png/@2x~ipad.png/` ;done

cd ../2x
for i in *.png; do convert $i -resize 50% `echo ../x/$i | sed s/@2x//` ;done
mv *.png ../../images/

cd ../x
mv *.png ../../images/
