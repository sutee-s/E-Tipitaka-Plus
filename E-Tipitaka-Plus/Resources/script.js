var mySheet = document.styleSheets[0];

function addCSSRule(selector, newRule) {
    if (mySheet.addRule) {
        mySheet.addRule(selector, newRule);
    } else {
        ruleIndex = mySheet.cssRules.length;
        mySheet.insertRule(selector + "{" + newRule + ";}", ruleIndex);
    }
}

function scrollToElement(theElement) {
    var selectedPosX = 0;
    var selectedPosY = 0;
    while(theElement != null){
        selectedPosX += theElement.offsetLeft;
        selectedPosY += theElement.offsetTop;
        theElement = theElement.offsetParent;
    }
    window.scrollTo(selectedPosX,selectedPosY);
}

function scrollToKeywords() {
    var keywords = document.getElementsByClassName("keywords");
    if (keywords.length > 0) {
        scrollToElement(keywords[0]);
    }
}

function scrollToItem(itemId) {
    var item = document.getElementById(itemId);
    if (item) {
        scrollToElement(item);
    }
}

function getLinePositionOfSelection() {
    var selection = window.getSelection();
    var range = selection.getRangeAt(0);
    var node = selection.anchorNode.previousSibling;
    alert(selection.anchorNode.parentNode);
    var linePosition = 0;
    while (node != null) {
        if (node.nodeType == Node.ELEMENT_NODE && node.nodeName == "BR") {
            linePosition++;
        }
        node = node.previousSibling;
    }
    return linePosition;
}

function findTextAtLinePosition(row) {
    var body = document.getElementById("body");
    var j = 0;
    for (var i=0; i< body.childNodes.length; i++) {
        var child = body.childNodes[i];
        
        if (child.nodeType == Node.TEXT_NODE && row == j) {
            
        }
        
        if (child.nodeType == Node.ELEMENT_NODE && child.nodeName == "BR") {
            j++;
        }
    }
}

function getSelectedTextPosition() {
    var sel = window.getSelection();
    var oRange = sel.getRangeAt(0);
    return JSON.stringify([oRange.startOffset, oRange.endOffset]);
}

function getSelectionCharOffsets() {
    var element = document.getElementById("main");
    var start = 0, end = 0;
    var sel, range, priorRange;
    if (typeof window.getSelection != "undefined") {
        range = window.getSelection().getRangeAt(0);
        priorRange = range.cloneRange();
        priorRange.selectNodeContents(element);
        priorRange.setEnd(range.startContainer, range.startOffset);
        start = priorRange.toString().length;
        end = start + range.toString().length;
    } else if (typeof document.selection != "undefined" &&
               (sel = document.selection).type != "Control") {
        range = sel.createRange();
        priorRange = document.body.createTextRange();
        priorRange.moveToElementText(element);
        priorRange.setEndPoint("EndToStart", range);
        start = priorRange.text.length;
        end = start + range.text.length;
    }
    return JSON.stringify([start, end]);
}

function updateHtml(html) {
    var element = document.getElementById("html");
    element.innerHTML = html.substring(1, html.length-1);
}

function decorateHighlight(rowid, text) {
    $(text).insertAfter("#hi"+rowid);
}

function removeHighlightDecorations() {
    $(".decoration").remove();
}

function removeHighlight(rowid, type) {
    var range = rangy.createRange();
    range.selectNodeContents(document.body);
    var highlightApplier = rangy.createClassApplier("highlight"+type,
        {
            elementTagName: "a",
            elementProperties: {
            href: "js-call://highlight_click/" + rowid,
            id: "hi"+rowid
            }
        });
    highlightApplier.undoToRange(range);
}

function addHighlight(selection, position, rowid, type, color, length) {
    var range = rangy.createRange();
    var searchScopeRange = rangy.createRange();
    searchScopeRange.selectNodeContents(document.body);
    
    var options = {
        caseSensitive: true,
        wholeWordsOnly: false,
        withinRange: searchScopeRange,
        direction: "forward"
    };
    
    range.selectNodeContents(document.body);
    var prefix = length > 0 ? "note-" : "";
    var highlightApplier = rangy.createClassApplier(prefix+"highlight"+type+"-"+color,
        {
            elementTagName: "a",
            elementProperties: {
                href: "js-call://highlight_click/" + rowid,
                id: "hi"+rowid
            }
        });
    
    // highlightApplier.undoToRange(range);
    
    var count = 1;
    while (range.findText(new RegExp(selection), options)) {
        if (count == position) {
            highlightApplier.applyToRange(range);
            break;
        }
        range.collapse(false);
        count += 1;
    }
}

function addHighlightDict(selection, position, rowid) {
    var range = rangy.createRange();
    var searchScopeRange = rangy.createRange();
    searchScopeRange.selectNodeContents(document.body);
    
    var options = {
        caseSensitive: true,
        wholeWordsOnly: false,
        withinRange: searchScopeRange,
        direction: "forward"
    };
    
    range.selectNodeContents(document.body);
    var highlightApplier = rangy.createClassApplier("highlight",
        {
            elementTagName: "a",
            elementProperties: {
                href: "js-call://highlight_click/" + rowid,
                id: "hi"+rowid
            }
        });
    
    var count = 1;
    while (range.findText(new RegExp(selection), options)) {
        if (count == position) {
            highlightApplier.applyToRange(range);
            break;
        }
        range.collapse(false);
        count += 1;
    }
}

function search(term, searchType) {
    var searchResultApplier = rangy.createClassApplier("keywords");
    var range = rangy.createRange();
    var searchScopeRange = rangy.createRange();
    searchScopeRange.selectNodeContents(document.body);
    
    var options = {
        caseSensitive: true,
        wholeWordsOnly: false,
        withinRange: searchScopeRange,
        direction: "forward"
    };
    
    range.selectNodeContents(document.body);
    searchResultApplier.undoToRange(range);
    
    term = new RegExp(term, "g");
    
    while (range.findText(term, options)) {
        var parentNode = range.startContainer.parentNode;
        if (searchType == 1 || (searchType == 2 && parentNode.hasAttributes() && parentNode.attributes["class"] !== undefined && (parentNode.attributes["class"].value == "level-1" || parentNode.attributes["class"].value == "level-2"))) {
            searchResultApplier.applyToRange(range);
        }
        range.collapse(false);
    }
}

function getSelectionCountPosition() {
    var sel = rangy.getSelection();

    var searchScopeRange = rangy.createRange();
    searchScopeRange.selectNodeContents(document.body);
    var options = {
        caseSensitive: true,
        wholeWordsOnly: false,
        withinRange: searchScopeRange,
        direction: "forward"
    };

    var range = rangy.createRange();
    range.selectNodeContents(document.body);
    
    var position = 1;
    while (range.findText(sel.toString(), options)) {
        if (rangy.serializeRange(range, true) == rangy.serializeRange(sel.getRangeAt(0), true)) {
            break;
        }
        range.collapse(false);
        position += 1;
    }
    return position;
}
