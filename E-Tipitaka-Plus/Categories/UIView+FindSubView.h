//
//  UIView+FindSubView.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/6/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (FindSubView)

- (UIView *)findSubview:(NSString *)name resursion:(BOOL)resursion;

@end

NS_ASSUME_NONNULL_END
