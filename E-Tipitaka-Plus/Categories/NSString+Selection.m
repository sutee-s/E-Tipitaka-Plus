//
//  NSString+Selection.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 3/2/23.
//  Copyright © 2023 Watnapahpong. All rights reserved.
//

#import "NSString+Selection.h"

@implementation NSString (Selection)

- (NSString *)formatSelection
{
  NSString *selection = self;
  selection = [selection stringByReplacingOccurrencesOfString:@" \n" withString:@"\\\\s+"];
  selection = [selection stringByReplacingOccurrencesOfString:@"\t" withString:@"\\\\s+"];
  selection = [selection stringByReplacingOccurrencesOfString:@"[" withString:@"\\\\["];
  selection = [selection stringByReplacingOccurrencesOfString:@"]" withString:@"\\\\]"];
  selection = [selection stringByReplacingOccurrencesOfString:@"." withString:@"[.]"];
  selection = [selection stringByReplacingOccurrencesOfString:@"*" withString:@"[*]"];
  selection = [selection stringByReplacingOccurrencesOfString:@"(" withString:@"[(]"];
  selection = [selection stringByReplacingOccurrencesOfString:@")" withString:@"[)]"];
  selection = [selection stringByReplacingOccurrencesOfString:@" " withString:@"\\\\s"];
  selection = [selection stringByReplacingOccurrencesOfString:@"\n" withString:@"\\\\s"];
  selection = [selection stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
  return selection;
}

@end
