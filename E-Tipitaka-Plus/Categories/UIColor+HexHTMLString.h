//
//  UIColor+HexHTMLString.h
//  EpubReaderNoCurl
//
//  Created by Sutee Sudprasert on 3/23/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexHTMLString)

- (NSString *)hexHTMLString;
+ (UIColor*)pxColorWithHexValue:(NSString*)hexValue;

@end
