//
//  NSString+Thai.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Thai)

- (NSString *)stringByReplacingThaiNumeric;
- (NSInteger)integerValueFromThaiNumeric;
- (NSNumber *)numberValueFromThaiNumberic;

@end
