//
//  NSString+Selection.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 3/2/23.
//  Copyright © 2023 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Selection)
 
- (NSString *)formatSelection;

@end

NS_ASSUME_NONNULL_END
