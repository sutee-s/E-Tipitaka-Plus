//
//  UIView+Snapshot.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 21/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Snapshot)

- (UIView *)customSnapshot;

@end
