//
//  NSString+PaseHTML.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/12/22.
//  Copyright © 2022 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (PaseHTML)

- (NSString *)parseHTML;

@end

NS_ASSUME_NONNULL_END
