//
//  UIColor+HexHTMLString.m
//  EpubReaderNoCurl
//
//  Created by Sutee Sudprasert on 3/23/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "UIColor+HexHTMLString.h"

@implementation UIColor (HexHTMLString)

- (NSString *)hexHTMLString
{
    CGFloat red, green, blue, alpha;        
    [self getRed:&red green:&green blue:&blue alpha:&alpha];
    int iRed = red * 255;
    int iGreen = green * 255;
    int iBlue = blue * 255;        
    return [NSString stringWithFormat:@"#%02x%02x%02x", iRed, iGreen, iBlue];
}

+ (UIColor*)pxColorWithHexValue:(NSString*)hexValue 
{
    //Default
    UIColor *defaultResult = [UIColor blackColor];
    
    //Strip prefixed # hash
    if ([hexValue hasPrefix:@"#"] && [hexValue length] > 1) {
        hexValue = [hexValue substringFromIndex:1];
    }
    
    //Determine if 3 or 6 digits
    NSUInteger componentLength = 0;
    if ([hexValue length] == 3) {
        componentLength = 1;
    }
    else if ([hexValue length] == 6) {
        componentLength = 2;
    }
    else {
        return defaultResult;
    }
    
    BOOL isValid = YES;
    CGFloat components[3];
    
    //Seperate the R,G,B values
    for (NSUInteger i = 0; i < 3; i++) {
        NSString *component = [hexValue substringWithRange:NSMakeRange(componentLength * i, componentLength)];
        if (componentLength == 1) {
            component = [component stringByAppendingString:component];
        }
        NSScanner *scanner = [NSScanner scannerWithString:component];
        unsigned int value;
        isValid &= [scanner scanHexInt:&value];
        components[i] = (CGFloat)value / 256.0f;
    }
    
    if (!isValid) {
        return defaultResult;
    }
    
    return [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:1.0];
}

@end
