//
//  NSString+PaseHTML.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/12/22.
//  Copyright © 2022 Watnapahpong. All rights reserved.
//

#import "NSString+PaseHTML.h"

@implementation NSString (PaseHTML)

- (NSString *)parseHTML
{
  return [[NSAttributedString alloc] initWithData:[self dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil].string;
}

@end
