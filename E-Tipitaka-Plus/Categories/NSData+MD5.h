//
//  NSData+MD5.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 24/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MD5)

- (NSString *)MD5;

@end
