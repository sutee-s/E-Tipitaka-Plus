//
//  UIWebView+Javascript.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/8/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (Javascript)

- (void)loadJavascriptFileFromResourceBundle:(NSString *)fileName;

@end
