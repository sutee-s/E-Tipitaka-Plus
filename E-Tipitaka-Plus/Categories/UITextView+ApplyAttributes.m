//
//  UITextView+ApplyAttributes.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/12/22.
//  Copyright © 2022 Watnapahpong. All rights reserved.
//

#import "UITextView+ApplyAttributes.h"

@implementation UITextView (ApplyAttributes)

- (void)applyAttrubutesToSelectedRange:(id)attribute forKey:(NSString *)key
{
  [self applyAttributes:attribute forKey:key atRange:self.selectedRange];
}

- (void)applyAttributeToTypingAttribute:(id)attribute forKey:(NSString *)key
{
  NSMutableDictionary *dictionary = [self.typingAttributes mutableCopy];
  [dictionary setObject:attribute forKey:key];
  [self setTypingAttributes:dictionary];
}

- (void)applyAttributes:(id)attribute forKey:(NSString *)key atRange:(NSRange)range
{
  // If any text selected apply attributes to text
  if (range.length > 0)
  {
    NSMutableAttributedString *attributedString = [self.attributedText mutableCopy];
    
        // Workaround for when there is only one paragraph,
    // sometimes the attributedString is actually longer by one then the displayed text,
    // and this results in not being able to set to lef align anymore.
        if (range.length == attributedString.length-1 && range.length == self.text.length)
            ++range.length;
        
    [attributedString addAttributes:[NSDictionary dictionaryWithObject:attribute forKey:key] range:range];
    
    [self setAttributedText:attributedString];
    [self setSelectedRange:range];
    [self scrollRangeToVisible:range];
  }
  // If no text is selected apply attributes to typingAttribute
  else
  {
    [self applyAttributeToTypingAttribute:attribute forKey:key];
  }
  
}

@end
