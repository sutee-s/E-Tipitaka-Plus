//
//  String+StripHtmlTags.swift
//  E-Tipitaka-Plus
//
//  Created by SUTEE SUDPRASERT on 30/11/2567 BE.
//  Copyright © 2567 BE Watnapahpong. All rights reserved.
//

import Foundation
import SwiftSoup

extension String {
    var attributedHtmlString: NSAttributedString? {
        try? NSAttributedString(
            data: Data(utf8),
            options: [
                .documentType: NSAttributedString.DocumentType.html,
                .characterEncoding: String.Encoding.utf8.rawValue
            ],
            documentAttributes: nil
        )
    }
}

@objc class MySoup : NSObject {
  @objc class func stripTags(html: String) -> String {
    return html
      .replacingOccurrences(of: "<span style=\"background-color: rgb\\(255, 255, 0\\);\">(.+?)</span>", with: "<strong>$1</strong>", options: .regularExpression)
      .replacingOccurrences(of: "<a [^>]+>(.|\n+?)</a>", with: "$1", options: .regularExpression)
      .replacingOccurrences(of: "<span [^>]+>(.|\n+?)</span>", with: "$1", options: .regularExpression)
      .replacingOccurrences(of: "<br />", with: " ")
      .replacingOccurrences(of: "<div>(.|\n+?)</div>", with: "$1", options: .regularExpression)
      .replacingOccurrences(of: "\n", with: "")
  }
  
  @objc class func decode(encodedString:String) -> String {
    if let attributedText = encodedString.attributedHtmlString {
      return attributedText.string
    }
    return encodedString
  }
      
}

  

