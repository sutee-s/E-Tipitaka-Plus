//
//  UITextView+ApplyAttributes.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/12/22.
//  Copyright © 2022 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (ApplyAttributes)

- (void)applyAttrubutesToSelectedRange:(id)attribute forKey:(NSString *)key;
- (void)applyAttributes:(id)attribute forKey:(NSString *)key atRange:(NSRange)range;
- (void)applyAttributeToTypingAttribute:(id)attribute forKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
